#!/bin/bash

#
# Copyright (c) 2024 Austin Lehman
#

# Built on Windows 10 Home edition with Maven 3.9.6 and
# OpenJDK (Adoptium) Java 23.01. It requires Wix 3.14 and is
# built using Gitbash environment. (MinGW I believe)

VERSION="1.0.2"
PACKAGE_DIR=package

echo "Running the packager for aussom."
echo "Copyright 2024 Austin Lehman"
echo "Licensed under the GNU GPL Version 3"
echo ""
echo "Generating version: $VERSION"
echo "If all goes well resulting .msi will be in the target/ directory."
echo ""

if [ -z "$JAVA_HOME" ]
then
  echo "Error, \$JAVA_HOME is not set. Please set it in your environment or at the top of this script."
  exit 1
else
  echo "\$JAVA_HOME found, continuing."
fi

echo "Making package directory ..."
mkdir package

echo "Packaging with maven ...";
mvn clean package
if [ "$?" -ne 0 ]
then
  echo "Error, build failure. Cannot package without a successful build. Exiting."
  exit 1
else
  echo "Build successful, generating docs ..."
  # Generating the docs
  java -jar package/aussom-*.jar gendocs.aus

  # Copy docs into package directory
  rm -r $PACKAGE_DIR/docs
  mkdir $PACKAGE_DIR/docs
  cp -R docs/* $PACKAGE_DIR/docs

  # Copy over modules and add them to the package dir.
  #aussom copy-modules.aus
  #cp -R modules/* $PACKAGE_DIR/modules

  # The packaging idea I got from here to set the path variable automatically.
  # https://stackoverflow.com/questions/67784565/jpackage-update-path-environment-variable

  echo "Running jpackage ..."
  jpackage \
    -t msi \
    -d target \
    -i package \
    -n "aussom" \
    --java-options "-javaagent:\$APPDIR/libs/jar-loader.jar" \
    --app-version "$VERSION" \
    --copyright "Copyright © 2024 Austin Lehman" \
    --description "The Aussom Programming Language" \
    --vendor "Austin Lehman" \
    --license-file "LICENSE.txt" \
    --main-class com.lehman.aussom.Main \
    --main-jar "aussom-$VERSION.jar" \
    --win-console \
    --resource-dir "packaging-files/msi" \
    --verbose
fi

echo "Cleaning up package directory ..."
rm -r package
echo "Done!"
