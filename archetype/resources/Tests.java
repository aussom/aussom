/*
 * Your license here.
 */

import com.aussom.Engine;
import com.aussom.Util;
import com.aussom.stdlib.Lang;
import org.junit.Assert;
import org.junit.Test;

public class Tests {

    public Tests() throws Exception {
    }

    @Test
    public void runAussomTest() throws Exception {
        Engine eng = new Engine();
        // Add the Aussom module
        Lang.get().langIncludes.put("${moduleName}.aus", Util.read("src/main/aus/${moduleName}.aus"));

        eng.loadUniverseClasses();
        eng.addIncludePath(".");
        eng.parseFile("${moduleName}-test.aus");
        int ret = eng.run();

        if (ret == 0) {
            Assert.assertTrue(true);
        } else {
            Assert.assertTrue(false);
        }
    }
}
