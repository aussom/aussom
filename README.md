# All this code is Aussom!

What is it? It's an object-oriented interpreted programming language that runs on the JVM. Read on to get started!

**Note:** Currenly, there's only an installer for Linux. If you run Windows or MacOS, you can download the source and install it manually. ([Use the Source, Luke!](https://www.urbandictionary.com/define.php?term=Use%20the%20Source%2C%20Luke)) I do hope to create Windows and MacOS installers in the future though. 

### Getting Started

First up, [download the current .deb release](https://www.dropbox.com/scl/fi/7czlurzbycsrczzc40c9k/aussom-0.5-amd64.deb?rlkey=pymulf0j0ekjlpfzwkkrtv7by&st=630i7xui&dl=0) and install it. 

```
$ sudo dkpg -i aussom-0.5-amd64.deb
```

Once that completes you should be able to invoke Aussom from the command line.

```
$ aussom -h
usage: aussom [options] <aussom-file>
 -d,--doc            generate aussomdoc for file
 -h,--help           print this message
 -o,--outdir <arg>   the output directory to place the generated aussomdoc
                     file
 -od,--opendoc       opens the Aussom documentation in the system browser
 -v,--version        print the version information

```

### Get the Docs!

Aussom cli comes with a cool feature where it will launch the docs in your local browser. Just run the following command.

```
$ aussom -od
```
 
That command should've launched the doc page in your browser and provide a bunch more information about the language and how to get started writing your first program. If for some reason it didn't launch the page in your browser, you can manually open it from `/opt/aussom/lib/app/docs/html/index.html`.


### Uninstalling

To remove Aussom you can just use apt.

```
$ sudo apt-get purge aussom
```