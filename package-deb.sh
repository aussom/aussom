#!/bin/bash

#
# Copyright (c) 2024 Austin Lehman
#

VERSION="1.0.2"
PACKAGE_DIR=package

echo "Running the packager for aussom."
echo "Copyright 2024 Austin Lehman"
echo "Licensed under the GNU GPL Version 3"
echo ""
echo "Generating version: $VERSION"
echo "If all goes well resulting .deb will be in the target/ directory."
echo ""

if [ -z "$JAVA_HOME" ]
then
  echo "Error, \$JAVA_HOME is not set. Please set it in your environment or at the top of this script."
  exit 1
else
  echo "\$JAVA_HOME found, continuing."
fi

echo "Making package directory ..."
mkdir package

echo "Packaging with maven ...";
mvn clean package
if [ "$?" -ne 0 ]
then
  echo "Error, build failure. Cannot package without a successful build. Exiting."
  exit 1
else
  echo "Build successful, generating docs ..."
  # Generating the docs
  java -jar package/aussom-*.jar gendocs.aus

  # Copy docs into package directory
  rm -r $PACKAGE_DIR/docs
  mkdir $PACKAGE_DIR/docs
  cp -R docs/* $PACKAGE_DIR/docs

  # Copy over modules and add them to the package dir.
  #aussom copy-modules.aus
  #cp -R modules/* $PACKAGE_DIR/modules

  echo "Running jpackage ..."
  jpackage \
    -t deb \
    -d target \
    -i package \
    -n "aussom" \
    --java-options "-javaagent:\$APPDIR/libs/jar-loader.jar" \
    --app-version "$VERSION" \
    --copyright "Copyright © 2024 Austin Lehman" \
    --description "The Aussom Programming Language" \
    --vendor "Austin Lehman" \
    --linux-deb-maintainer "austin@rosevillecode.com" \
    --linux-shortcut \
    --license-file "LICENSE.txt" \
    --main-class com.lehman.aussom.Main \
    --main-jar "aussom-$VERSION.jar" \
    --verbose

    # Explode the deb for repackaging.
    cd target
    mkdir deb
    dpkg-deb -x aussom_*_amd64.deb deb
    dpkg-deb -e aussom_*_amd64.deb deb/DEBIAN

    # Copy deb control files here.
    cp ../packaging-files/deb/postinst deb/DEBIAN/postinst
    cp ../packaging-files/deb/prerm deb/DEBIAN/prerm

    # Repackage deb.
    dpkg-deb -b deb "aussom-$VERSION-amd64.deb"

    # Remove deb directory
    rm -r -f deb
    rm aussom_*_amd64.deb
    cd ..
fi

echo "Cleaning up package directory ..."
rm -r package
echo "Done!"