package com.lehman.aussom.stdlib;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class FxApplication  extends Application
{
	private static FxApplication instance = null;

	private static Stage stage = null;
	private static boolean initialized = false;

	public Stage getStage() { return stage; }
	public void setStage(Stage stg) { stage = stg; }

	public FxApplication() {
		Platform.setImplicitExit(false);
	}

	public boolean isInitialized() { return initialized; }

	public static boolean isInstantiated() {
		if(instance == null) return false;
		return true;
	}

	public static void stopInstance() {
		Platform.exit();
	}

	public static FxApplication getInstance() {
	      if(instance == null) {
	         instance = new FxApplication();
	         instance.initApp();
	      }
	      return instance;
	}

	private class FxAppLauncher extends Thread {
		private FxApplication app = null;

		public FxAppLauncher(FxApplication App) { this.app = App; }
		public void run() {
			this.app.launchApp();
		}
	}

	private void initApp() {
		initialized = false;

		FxAppLauncher al = new FxAppLauncher(this);
		al.start();

		while(!initialized) {
			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void launchApp() {
		String[] args = new String[0];
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		initialized = true;
	}

	public List<String> getStyleSheets() {
		List<String> styleSheets = new ArrayList<String>();
		if(stage instanceof AussomFxApp) {
			styleSheets = ((AussomFxApp)stage).getStyleSheets();
		}
		return styleSheets;
	}
}