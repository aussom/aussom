package com.lehman.aussom.stdlib;

public enum XmlNodeType {
    ELEMENT(1),
    ATTRIBUTE(2),
    TEXT(3),
    CDATA_SECTION(4),
    ENTITY_REFERENCE(5),
    ENTITY(6),
    PROCESSING_INSTRUCTION(7),
    COMMENT(8),
    DOCUMENT(9),
    DOCUMENT_TYPE(10),
    DOCUMENT_FRAGMENT(11),
    NOTATION(12);
    private int code;
    XmlNodeType(int Code) {
        this.code = Code;
    }
    public int getCode() {
        return code;
    }
    public static XmlNodeType get(int val) {
        switch(val) {
            case 1: {
                return ELEMENT;
            } case 2: {
                return ATTRIBUTE;
            } case 3: {
                return TEXT;
            } case 4: {
                return CDATA_SECTION;
            } case 5: {
                return ENTITY_REFERENCE;
            } case 6: {
                return ENTITY;
            } case 7: {
                return PROCESSING_INSTRUCTION;
            } case 8: {
                return COMMENT;
            } case 9: {
                return DOCUMENT;
            } case 10: {
                return DOCUMENT_TYPE;
            } case 11: {
                return DOCUMENT_FRAGMENT;
            } case 12: {
                return NOTATION;
            } default: {
                return ELEMENT;
            }
        }
    }
}
