package com.lehman.aussom.stdlib;

import com.aussom.Environment;
import com.aussom.Util;
import com.aussom.ast.aussomException;
import com.aussom.types.*;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.io.FileTemplateLoader;
import com.github.jknack.handlebars.io.TemplateLoader;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

public class AussomHandlebars {
    private Handlebars hbars;
    private Template t;

    public AussomHandlebars() throws aussomException {
        TemplateLoader loader = new FileTemplateLoader(".", ".html");
        this.hbars = new Handlebars(loader);
        try {
            this.t = hbars.compileInline("");
        } catch (Exception e) {
            throw new aussomException("Handlebars(): IOException in constructor: " + e.getMessage());
        }
    }

    public AussomType compileString(Environment env, ArrayList<AussomType> args) throws aussomException {
        AussomString cstr = (AussomString)args.get(0);
        try {
            this.t = this.hbars.compileInline(cstr.getValue());
        } catch (Exception e) {
            throw new aussomException("Handlebars.compileString() IOException: " + e.getMessage());
        }
        return env.getCurObj() != null ? env.getCurObj() : new AussomNull();
    }

    public AussomType compile(Environment env, ArrayList<AussomType> args) throws aussomException {
        String fstr = ((AussomString)args.get(0)).getValue();
        File f = new File(fstr + ".html");
        if (f.exists()) {
            try {
                this.t = this.hbars.compileInline(Util.read(fstr + ".html"));
            } catch (Exception e) {
                throw new aussomException("Handlebars.compile() IOException: " + e.getMessage());
            }
            return env.getCurObj() != null ? env.getCurObj() : new AussomNull();
        } else {
            return new AussomException("Handlebars.compile(): cannot find specified file '" + fstr + ".html'.");
        }
    }

    public AussomType apply(Environment env, ArrayList<AussomType> args) throws aussomException {
        String json = "";
        if (args.get(0).getType() == cType.cString) {
            AussomString cstr = (AussomString) args.get(0);
            json = cstr.getValue();
        } else if(args.get(0) instanceof AussomObject) {
            json = ((AussomObject)args.get(0)).toJson(env, new ArrayList<AussomType>()).getValueString();
        } else {
            json = args.get(0).getValueString();
        }

        Gson gson = new Gson();

        Type type = new TypeToken<Map<String, Object>>(){}.getType();
        Map<String, Object> map = gson.fromJson(json, type);
        Context context = Context.newBuilder(map).build();

        String ret = null;
        try {
            ret = this.t.apply(context);
        } catch (Exception e) {
            throw new aussomException("handlebars.apply(): IOException: " + e.getMessage());
        }

        return new AussomString(ret);
    }
}
