package com.lehman.aussom.stdlib;

import com.aussom.Environment;
import com.aussom.ast.astClass;
import com.aussom.ast.aussomException;
import com.aussom.stdlib.ABuffer;
import com.aussom.stdlib.console;
import com.aussom.types.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;

public class AussomAJI extends AussomJavaObject {

    public class GenericInvocationHandler implements InvocationHandler {
        protected Environment env;
        protected AussomAJI aji;
        protected AussomCallback callback;
        public GenericInvocationHandler(Environment Env, AussomAJI Aji, AussomCallback Callback) {
            this.env = Env;
            this.aji = Aji;
            this.callback = Callback;
        }

        @Override
        public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
            AussomCallback ac = this.callback;
            AussomList args = new AussomList();

            for (Object obj : objects) {
                args.add(this.aji.mapResultObject(this.env, obj));
            }

            AussomType ret = ac.call(this.env, args);
            if (ret.isEx()) {
                console.get().err(ret.toString());
            }
            return this.aji.getClassAussomTypeValue(ret);
        }
    }

    public class ConstructorMatch {
        public Constructor<?> constructor;
        public List<Object> args = new ArrayList<>();
    }

    public AussomType invokeStatic(Environment env, ArrayList<AussomType> args) throws aussomException {
        String javaObjName = (((AussomString) args.get(0)).str());
        String javaMethodName = (((AussomString) args.get(1)).str());

        AussomList etcArgs = (AussomList) args.get(2);
        List<AussomType> constArgs = etcArgs.getValue();

        if (!(Boolean)env.getEngine().getSecurityManager().getProperty("aussom.aji.static.invoke"))
            throw new aussomException("aji.invokeStatic(): Security manager property 'aussom.aji.static.invoke' set to false.");

        try {
            Class<?> cl = Class.forName(javaObjName);
            MethodMatch mm = this.getMatch(null, cl, javaMethodName, constArgs);

            if (mm.method != null) {
                // Match found, attempt to invoke.
                try {
                    mm.method.setAccessible(true);
                    Object retObj = mm.method.invoke(null, mm.args.toArray());

                    // It is alread an aussom type, no wrapping needed.
                    if (retObj instanceof AussomType)
                        return (AussomType) retObj;

                    // Not an Aussom type, map the result.
                    return this.mapResultObject(env, retObj);
                } catch (Exception e) {
                    throw new aussomException("AussomJavaObject.invoke(): In Java class '" + javaMethodName + "' method invoke exception: " + e.getMessage());
                }
            } else {
                throw new aussomException("AussomJavaObject.newObj(): In Java class '" + javaMethodName + "' could not find a matching method '" + javaMethodName + "' for the provided arguments.");
            }
        } catch (ClassNotFoundException e) {
            throw new aussomException("aji.invokeStatic(): Java class '" + javaObjName + "' not found. Is the .jar file in the classpath?");
        }
    }

    public AussomType invokeStaticRaw(Environment env, ArrayList<AussomType> args) throws aussomException {
        String javaObjName = (((AussomString) args.get(0)).str());
        String javaMethodName = (((AussomString) args.get(1)).str());

        AussomList etcArgs = (AussomList) args.get(2);
        List<AussomType> constArgs = etcArgs.getValue();

        if (!(Boolean)env.getEngine().getSecurityManager().getProperty("aussom.aji.static.invoke"))
            throw new aussomException("aji.invokeStatic(): Security manager property 'aussom.aji.static.invoke' set to false.");

        try {
            Class<?> cl = Class.forName(javaObjName);
            MethodMatch mm = this.getMatch(null, cl, javaMethodName, constArgs);

            if (mm.method != null) {
                // Match found, attempt to invoke.
                try {
                    mm.method.setAccessible(true);
                    Object retObj = mm.method.invoke(null, mm.args.toArray());

                    // It is alread an aussom type, no wrapping needed.
                    if (retObj instanceof AussomType)
                        return (AussomType) retObj;

                    // Not an Aussom type, wrap result object.
                    return this.wrapRawObject(env, retObj);
                } catch (Exception e) {
                    throw new aussomException("AussomJavaObject.invokeStaticRaw(): In Java class '" + javaMethodName + "' method invoke exception: " + e.getMessage());
                }
            } else {
                throw new aussomException("AussomJavaObject.invokeStaticRaw(): In Java class '" + javaMethodName + "' could not find a matching method '" + javaMethodName + "' for the provided arguments.");
            }
        } catch (ClassNotFoundException e) {
            throw new aussomException("aji.invokeStaticRaw(): Java class '" + javaObjName + "' not found. Is the .jar file in the classpath?");
        }
    }

    public AussomType getStaticMember(Environment env, ArrayList<AussomType> args) throws aussomException {
        String javaObjName = (((AussomString) args.get(0)).str());
        String javaMemberName = (((AussomString) args.get(1)).str());

        if (!(Boolean)env.getEngine().getSecurityManager().getProperty("aussom.aji.static.getmember"))
            throw new aussomException("aji.getStaticMember(): Security manager property 'aussom.aji.static.getmember' set to false.");

        try {
            Class<?> cl = Class.forName(javaObjName);
            Field field = cl.getDeclaredField(javaMemberName);
            field.setAccessible(true);
            Object robj = field.get(null);
            return this.mapResultObject(env, robj);
        } catch (ClassNotFoundException e) {
            throw new aussomException("aji.getStaticMember(): Java class '" + javaObjName + "' not found. Is the .jar file in the classpath?");
        } catch (NoSuchFieldException e) {
            throw new aussomException("aji.getStaticMember(): Java class '" + javaObjName + "' static field '" + javaMemberName + "' not found.");
        } catch (IllegalAccessException e) {
            throw new aussomException("aji.getStaticMember(): Java class '" + javaObjName + "' static field '" + javaMemberName + "' illegal access exception.");
        }
    }

    public AussomType setStaticMember(Environment env, ArrayList<AussomType> args) throws aussomException {
        String javaObjName = (((AussomString) args.get(0)).str());
        String javaMemberName = (((AussomString) args.get(1)).str());
        AussomType aval = (AussomType)args.get(2);

        if (!(Boolean)env.getEngine().getSecurityManager().getProperty("aussom.aji.static.setmember"))
            throw new aussomException("aji.getStaticMember(): Security manager property 'aussom.aji.static.setmember' set to false.");

        try {
            Class<?> cl = Class.forName(javaObjName);
            Field field = cl.getDeclaredField(javaMemberName);
            field.setAccessible(true);
            Object jobj = this.getClassAussomTypeValue(aval);
            if (jobj != null || !(aval instanceof AussomNull)) {
                field.set(null, jobj);
            } else {
                // Attempt to coerce.
                CoercedValue cv = new CoercedValue();
                this.attemptCoercion(field.getType(), aval, cv);
                if (cv.val != null) {
                    field.set(null, cv.val);
                } else {
                    throw new aussomException("aji.getStaticMember(): Java class '" + javaObjName + "' field '" + javaMemberName + "' can't find suitable conversion from Aussom type '" + aval.getType().name() + "' to Java type '" + field.getType().getName() + "'.");
                }
            }
        } catch (ClassNotFoundException e) {
            throw new aussomException("aji.getStaticMember(): Java class '" + javaObjName + "' not found. Is the .jar file in the classpath?");
        } catch (NoSuchFieldException e) {
            throw new aussomException("aji.getStaticMember(): Java class '" + javaObjName + "' field '" + javaMemberName + "' not found.");
        } catch (IllegalAccessException e) {
            throw new aussomException("aji.getStaticMember(): Java class '" + javaObjName + "' field '" + javaMemberName + "' illegal access exception.");
        }
        return env.getClassInstance();
    }

    public AussomType newObj(Environment env, ArrayList<AussomType> args) throws aussomException {
        String javaObjName = (((AussomString)args.get(0)).str());

        AussomList etcArgs = (AussomList) args.get(1);
        List<AussomType> constArgs = etcArgs.getValue();

        if (!(Boolean)env.getEngine().getSecurityManager().getProperty("aussom.aji.object.create"))
            throw new aussomException("aji.newObj(): Security manager property 'aussom.aji.object.create' set to false.");

        try {
            Class<?> cl = Class.forName(javaObjName);
            ConstructorMatch cm = this.getConstructorMatch(cl, constArgs);
            cm.constructor.setAccessible(true);
            if (cm.constructor != null) {
                // Match found, attempt to invoke.
                try {
                    Object newObj = cm.constructor.newInstance(cm.args.toArray());
                    AussomObject ret = env.getEngine().instantiateObject("AussomJavaObject");
                    AussomJavaObject ajo = (AussomJavaObject) ret.getExternObject();
                    ajo.setName(javaObjName);
                    ajo.setObj(newObj);
                    return ret;
                } catch (Exception e) {
                    throw new aussomException("aji.newObj(): In Java class '" + javaObjName + "' constructor instantiation exception: " + e.getMessage());
                }
            } else {
                throw new aussomException("aji.newObj(): In Java class '" + javaObjName + "' could not find a matching constructor for the provided arguments.");
            }
        } catch (ClassNotFoundException e) {
            throw new aussomException("aji.newObj(): Java class '" + javaObjName + "' not found. Is the .jar file in the classpath?");
        }
    }

    public AussomType bufferToInputStream(Environment env, ArrayList<AussomType> args) throws aussomException {
        AussomObject ao = (AussomObject) args.get(0);
        if (ao.getExternObject() instanceof ABuffer) {
            ABuffer ab = (ABuffer) ao.getExternObject();
            AussomObject newObj = env.getEngine().instantiateObject("AussomJavaObject");
            AussomJavaObject ajo = (AussomJavaObject) newObj.getExternObject();
            ajo.setObj(new ByteArrayInputStream(ab.getBuffer()));
            return newObj;
        } else {
            throw new aussomException("aji.bufferToInputStream(): Expected object of type 'buffer' but found '" + ao.getClassDef().getName() + "'.");
        }
    }

    public AussomType inputStreamToBuffer(Environment env, ArrayList<AussomType> args) throws aussomException {
        AussomObject ao = (AussomObject) args.get(0);
        if (ao.getExternObject() instanceof AussomJavaObject) {
            AussomJavaObject ajo = (AussomJavaObject) ao.getExternObject();
            if (ajo.getObj() instanceof InputStream) {
                InputStream is = (InputStream) ajo.getObj();
                astClass ac = env.getClassByName("buffer");
                AussomObject ret = (AussomObject) ac.instantiate(env, false, new AussomList());
                ABuffer ab = (ABuffer) ret.getExternObject();
                try {
                    ab.setBuffer(is.readAllBytes());
                } catch (IOException e) {
                    throw new aussomException("aji.bufferToInputStream(): IOException while reading InputStream bytes: " + e.getMessage());
                }
                return ret;
            } else {
                throw new aussomException("aji.bufferToInputStream(): Expected wrapped AussomJavaObject to be of type 'InputStream' but found '" + ajo.getObj().getClass().getName() + "'.");
            }
        } else {
            throw new aussomException("aji.bufferToInputStream(): Expected object of type 'AussomJavaObject' but found '" + ao.getClassDef().getName() + "'.");
        }
    }

    public AussomType closure(Environment env, ArrayList<AussomType> args) throws aussomException, ClassNotFoundException {
        String className = ((AussomString)args.get(0)).getValue();
        AussomCallback ac = (AussomCallback)args.get(1);

        AussomObject ret = env.getEngine().instantiateObject("AussomJavaObject");

        Class<?> cl = Class.forName(className);
        Method methods[] = cl.getMethods();
        if (methods.length > 0) {
            methods[0].setAccessible(true);
            String name = methods[0].getName();
            Type types[] = methods[0].getGenericParameterTypes();
            Class<?> retType = methods[0].getReturnType();
            Parameter parameters[] = methods[0].getParameters();

            Object handler = (Object) Proxy.newProxyInstance(AussomFx.class.getClassLoader(),
              new Class[] { cl },
              new GenericInvocationHandler(env,this, ac));

            AussomJavaObject ajo = (AussomJavaObject) ret.getExternObject();
            ajo.setName(handler.getClass().getCanonicalName());
            ajo.setObj(handler);
        }

        return ret;
    }

    private ConstructorMatch getConstructorMatch(Class<?> cl, List<AussomType> constArgs) {
        ConstructorMatch cm = this.getConstructorExactMatch(cl, constArgs);
        if (cm.constructor != null) return cm;
        else {
            Constructor<?> constructors[] = cl.getConstructors();
            cm = this.findConstructorFromList(constructors, constArgs);
            if (cm.constructor != null) return cm;
        }
        return new ConstructorMatch();
    }

    private ConstructorMatch getConstructorExactMatch(Class<?> cl, List<AussomType> constArgs) {
        ConstructorMatch cm = new ConstructorMatch();
        Class<?> typeArgs[] = new Class<?>[constArgs.size()];

        for (int i = 0; i < constArgs.size(); i++) {
            AussomType at = constArgs.get(i);
            typeArgs[i] = this.getClassType(at);
            cm.args.add(this.getClassAussomTypeValue(at));
        }

        try {
            cm.constructor = cl.getConstructor(typeArgs);
        } catch (NoSuchMethodException e) {
            // Do nothing, we expect this
        }

        return cm;
    }

    private ConstructorMatch findConstructorFromList(Constructor<?> constructors[], List<AussomType> constArgs) {
        ConstructorMatch match = new ConstructorMatch();

        boolean found = true;
        for (Constructor constructor : constructors) {
            found = true;
            if (constructor.getParameterCount() == constArgs.size()) {
                // This may be the one!
                if (constArgs.size() == 0) {
                    match.constructor = constructor;
                    break;
                } else {
                    // Check the args, if they can be coerced.
                    List<Object> targs = new ArrayList<>();
                    int num = 0;
                    for (Parameter param : constructor.getParameters()) {
                        AussomType arg = constArgs.get(num);
                        Class<?> tmpType = this.getClassType(arg);
                        if (tmpType != null && (tmpType == param.getType() || param.getType() == Object.class)) {
                            // Look for the direct convert
                            targs.add(this.getClassAussomTypeValue(arg));
                        } else {
                            // No direct conversion, attempt first level coercion.
                            CoercedValue tobj = new CoercedValue();
                            if (!this.attemptCoercion(param.getType(), arg, tobj)) {
                                found = false;
                                break;
                            }
                            targs.add(tobj.val);
                        }
                        num++;
                    }
                    if (found) {
                        // Hurray, we made it here!
                        match.constructor = constructor;
                        match.args = targs;
                        break;
                    }
                }
            }
        }

        return match;
    }
}
