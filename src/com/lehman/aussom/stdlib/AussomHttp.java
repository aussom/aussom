package com.lehman.aussom.stdlib;

import com.aussom.Environment;
import com.aussom.types.*;
import okhttp3.*;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class AussomHttp {
    protected HttpCookieJar cookies = new HttpCookieJar();
    protected OkHttpClient client;

    protected boolean trustAllCerts = false;

    public AussomHttp() {
        this.createClient();
    }

    public AussomType setTrustAllCerts(Environment env, ArrayList<AussomType> args) throws IOException {
        this.trustAllCerts = ((AussomBool)args.get(0)).getValue();
        this.createClient();
        return env.getClassInstance();
    }

    public AussomType get(Environment env, ArrayList<AussomType> args) throws IOException {
        String url = ((AussomString)args.get(0)).getValue();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = this.client.newCall(request).execute();
        return this.processResponse(response);
    }

    public AussomType post(Environment env, ArrayList<AussomType> args) throws IOException {
        String url = ((AussomString)args.get(0)).getValue();
        String content = ((AussomString)args.get(1)).getValue();
        String mediaType = ((AussomString)args.get(2)).getValue();

        MediaType mtype = MediaType.get(mediaType);
        RequestBody body = RequestBody.create(content, mtype);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = this.client.newCall(request).execute();
        return this.processResponse(response);
    }

    public AussomType put(Environment env, ArrayList<AussomType> args) throws IOException {
        String url = ((AussomString)args.get(0)).getValue();
        String content = ((AussomString)args.get(1)).getValue();
        String mediaType = ((AussomString)args.get(2)).getValue();

        MediaType mtype = MediaType.get(mediaType);
        RequestBody body = RequestBody.create(content, mtype);
        Request request = new Request.Builder()
                .url(url)
                .put(body)
                .build();
        Response response = this.client.newCall(request).execute();
        return this.processResponse(response);
    }

    public AussomType patch(Environment env, ArrayList<AussomType> args) throws IOException {
        String url = ((AussomString)args.get(0)).getValue();
        String content = ((AussomString)args.get(1)).getValue();
        String mediaType = ((AussomString)args.get(2)).getValue();

        MediaType mtype = MediaType.get(mediaType);
        RequestBody body = RequestBody.create(content, mtype);
        Request request = new Request.Builder()
                .url(url)
                .patch(body)
                .build();
        Response response = this.client.newCall(request).execute();
        return this.processResponse(response);
    }

    public AussomType delete(Environment env, ArrayList<AussomType> args) throws IOException {
        String url = ((AussomString)args.get(0)).getValue();
        Request request = new Request.Builder()
                .url(url)
                .delete()
                .build();
        Response response = this.client.newCall(request).execute();
        return this.processResponse(response);
    }

    private AussomMap processResponse(Response response) throws IOException {
        AussomMap ret = new AussomMap();
        ret.put("body", new AussomString(response.body().string()));

        AussomMap info = new AussomMap();
        ret.put("info", info);
        info.put("headers", this.getHeaders(response));
        info.put("protocol", new AussomString(response.protocol().toString()));
        info.put("isRedirect", new AussomBool(response.isRedirect()));
        info.put("isSuccessful", new AussomBool(response.isSuccessful()));
        info.put("responseCode", new AussomInt(response.code()));
        info.put("cookies", this.getCookies());

        return ret;
    }

    private AussomMap getHeaders(Response response) {
        AussomMap headers = new AussomMap();
        for (String str : response.headers().names()) {
            List<String> lst = response.headers().toMultimap().get(str);
            if (lst.size() == 1) {
                headers.put(str, new AussomString(lst.get(0).replaceAll("\"","\\\\\"")));
            } else {
                AussomList cl = new AussomList();
                for (int i = 0; i < lst.size(); i++) {
                    cl.add(new AussomString(lst.get(i).replaceAll("\"","\\\\\"")));
                }
                headers.put(str, cl);
            }
        }
        return headers;
    }

    private AussomList getCookies() {
        AussomList cookieList = new AussomList();
        for (Cookie cookie : this.cookies.getCookies()) {
            AussomMap mc = new AussomMap();
            mc.put("domain", new AussomString(cookie.domain()));
            mc.put("name", new AussomString(cookie.name()));
            mc.put("value", new AussomString(cookie.value()));
            mc.put("path", new AussomString(cookie.path()));
            mc.put("secure", new AussomBool(cookie.secure()));
            mc.put("expiresAt", new AussomInt(cookie.expiresAt()));
            mc.put("domain", new AussomBool(cookie.persistent()));
            mc.put("hostOnly", new AussomBool(cookie.hostOnly()));
            mc.put("httpOnly", new AussomBool(cookie.httpOnly()));
            cookieList.add(mc);
        }
        return cookieList;
    }

    private void createClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.cookieJar(cookies);
        if (this.trustAllCerts) {
            // Get the trust manager
            TrustManager[] tm = this.getTrustManager();
            // Get the ssl context.
            SSLContext sslc = this.getSslContext(tm);
            // Set the trust manager and socket factory.
            builder.sslSocketFactory(sslc.getSocketFactory(), (X509TrustManager) tm[0]);
            builder.hostnameVerifier((hostname, session) -> true);
        }
        this.client = builder.build();
    }

    private TrustManager[] getTrustManager() {
        return new TrustManager[] {
            new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }
            }
        };
    }

    private SSLContext getSslContext(TrustManager[] tm) {
        SSLContext sslc;
        try {
            sslc = SSLContext.getInstance("SSL");
            sslc.init(null, tm, new java.security.SecureRandom());
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return sslc;
    }
}
