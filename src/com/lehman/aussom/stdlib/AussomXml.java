package com.lehman.aussom.stdlib;

import com.aussom.Environment;
import com.aussom.ast.aussomException;
import com.aussom.types.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;

public class AussomXml {
    public AussomXml() {}

    /**
     * Parses an xml file and returns an Aussom data structure.
     */
    public AussomType parse(Environment env, ArrayList<AussomType> args) throws aussomException {
        String xmlText = (((AussomString) args.get(0)).str());
        try {
            Document doc = this.getDocFromString(xmlText);
            return this.converXml(doc);
        } catch (Exception e) {
            throw new aussomException("xml.parse(): " + e.getMessage());
        }
    }

    /**
     * Converts the Aussom structure to XML.
     */
    public AussomType toXml(Environment env, ArrayList<AussomType> args) throws aussomException, ParserConfigurationException, TransformerException {
        AussomMap adoc = ((AussomMap) args.get(0));
        boolean pretty = ((AussomBool)args.get(1)).getValue();
        boolean strict = ((AussomBool)args.get(2)).getValue();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbf.newDocumentBuilder();
        Document doc = builder.newDocument();
        this.getAttributes(doc, adoc, strict);
        this.getChildren(doc, doc, adoc, strict);

        if (pretty) {
            Transformer tf = TransformerFactory.newInstance().newTransformer();
            tf.setOutputProperty(OutputKeys.INDENT, "yes");
            tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult res = new StreamResult(new StringWriter());
            DOMSource src = new DOMSource(doc);
            tf.transform(src, res);
            return new AussomString(res.getWriter().toString());
        } else {
            Transformer tf = TransformerFactory.newInstance().newTransformer();
            //tf.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StreamResult res = new StreamResult(new StringWriter());
            DOMSource src = new DOMSource(doc);
            tf.transform(src, res);
            return new AussomString(res.getWriter().toString());
        }
    }

    private void getAttributes(Node node, AussomMap cobj, boolean strict) throws aussomException {
        if (cobj.getValue().containsKey("attributes")) {
            if (cobj.getValue().get("attributes") instanceof AussomMap) {
                AussomMap attributes = (AussomMap) cobj.getValue().get("attributes");
                for (String atstr : attributes.getValue().keySet()) {
                    String name = atstr;
                    String value = attributes.getValue().get(atstr).getValueString();
                    ((Element)node).setAttribute(name, value);
                }
            } else {
                if (strict)
                    throw new aussomException("xml.toXml(): Doc root node has 'attributes' defined but it's not of type map.");
            }
        }
    }

    private void getChildren(Document doc, Node node, AussomMap cobj, boolean strict) throws aussomException {
        if (cobj.getValue().containsKey("children")) {
            if (cobj.getValue().get("children") instanceof AussomList) {
                AussomList children = (AussomList) cobj.getValue().get("children");
                for (AussomType at : children.getValue()) {
                    if (at instanceof AussomMap) {
                        node.appendChild(this.getNode(doc, (AussomMap) at, strict));
                    } else {
                        if (strict)
                            throw new aussomException("xml.toXml(): Node is expecting type 'map' but found '" + at.getType().name() + "' insetad: " + at.toString());
                    }
                }
            } else {
                if (strict)
                    throw new aussomException("xml.toXml(): Node has 'children' defined but not of type list.");
            }
        }
    }

    private Node getNode(Document doc, AussomMap cobj, boolean strict) throws aussomException {
        String name = "element";
        if (cobj.getValue().containsKey("name") && !cobj.getValue().get("name").getValueString().trim().equals("")) {
            name = cobj.getValue().get("name").getValueString().trim();
        } else {
            if (strict)
                throw new aussomException("xml.toXml(): Node is missing name: " + cobj.toString());
        }

        String typestr = "ELEMENT";
        if (cobj.getValue().containsKey("type") && !cobj.getValue().get("type").getValueString().trim().equals("")) {
            typestr = cobj.getValue().get("type").getValueString().trim();
        }
        XmlNodeType type = this.getType(typestr, strict);

        Node node = this.getNodeByType(doc, name, type, strict);

        // For text nodes.
        if (cobj.getValue().containsKey("value") && !cobj.getValue().get("value").getValueString().trim().equals("")) {
            node.setTextContent(cobj.getValue().get("value").getValueString());
        }

        // Get attributes.
        this.getAttributes(node, cobj, strict);

        // Get children.
        this.getChildren(doc, node, cobj, strict);

        return node;
    }

    private XmlNodeType getType(String type, boolean strict) throws aussomException {
        switch (type) {
            case "ELEMENT": {
                return XmlNodeType.ELEMENT;
            } case "ATTRIBUTE": {
                return XmlNodeType.ATTRIBUTE;
            } case "TEXT": {
                return XmlNodeType.TEXT;
            }
             case "CDATA_SECTION": {
                return XmlNodeType.CDATA_SECTION;
            }
             case "ENTITY_REFERENCE": {
                return XmlNodeType.ENTITY_REFERENCE;
            }
             case "ENTITY": {
                return XmlNodeType.ENTITY;
            }
             case "PROCESSING_INSTRUCTION": {
                return XmlNodeType.PROCESSING_INSTRUCTION;
            }
             case "COMMENT": {
                return XmlNodeType.COMMENT;
            }
             case "DOCUMENT": {
                return XmlNodeType.DOCUMENT;
            }
             case "DOCUMENT_TYPE": {
                return XmlNodeType.DOCUMENT_TYPE;
            }
             case "DOCUMENT_FRAGMENT": {
                return XmlNodeType.DOCUMENT_FRAGMENT;
            }
             case "NOTATION": {
                return XmlNodeType.NOTATION;
            } default: {
                 if (strict)
                     throw new aussomException("xml.toXml(): Unrecognized node type '" + type + "' provided.");
                 return XmlNodeType.ELEMENT;
            }
        }
    }

    private Node getNodeByType(Document doc, String Name, XmlNodeType Type, boolean strict) throws aussomException {
        switch (Type) {
            case ELEMENT: {
                return doc.createElement(Name);
            } case ATTRIBUTE: {
                return doc.createAttribute(Name);
            } case TEXT: {
                return doc.createTextNode("");
            } case CDATA_SECTION: {
                return doc.createCDATASection("");
            } case ENTITY_REFERENCE: {
                return doc.createEntityReference(Name);
            } case ENTITY: {
                return doc.createEntityReference(Name);
            } case PROCESSING_INSTRUCTION: {
                return doc.createProcessingInstruction(Name, "");
            } case COMMENT: {
                return doc.createComment("");
            } case DOCUMENT: {
                if (strict)
                    throw new aussomException("xml.toXml(): Attempt to create a node of type 'DOCUMENT' within the document.");
                return doc.createElement("name");
            } case DOCUMENT_TYPE: {
                if (strict)
                    throw new aussomException("xml.toXml(): Attempt to create a node of type 'DOCUMENT_TYPE' within the document.");
                return doc.createElement("name");
            } case DOCUMENT_FRAGMENT: {
                return doc.createDocumentFragment();
            } case NOTATION: {
                return doc.createElement(Name);
            } default: {
                return doc.createElement(Name);
            }
        }
    }

    private AussomType converXml(Node node) {
        AussomMap am = new AussomMap();
        am.put("type", new AussomString(XmlNodeType.get(node.getNodeType()).name()));
        am.put("name", new AussomString(node.getNodeName()));
        if (node.hasAttributes()) {
            AussomMap attrs = new AussomMap();
            for (int i = 0; i < node.getAttributes().getLength(); i++) {
                Node an = node.getAttributes().item(i);
                attrs.put(an.getNodeName(), new AussomString(an.getNodeValue()));
            }
            am.put("attributes", attrs);
        }
        if (node.hasChildNodes()) {
            AussomList children = new AussomList();
            for (int i = 0; i < node.getChildNodes().getLength(); i++) {
                children.add(this.converXml(node.getChildNodes().item(i)));
            }
            am.put("children", children);
        }
        if (node.getNodeValue() != null && node.getNodeValue().trim().length() != 0) {
            am.put("value", new AussomString(node.getNodeValue()));
        }
        return am;
    }

    private Document getDocFromString(String str) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder bd = dbf.newDocumentBuilder();
        return bd.parse(new InputSource(new StringReader(str)));
    }
}
