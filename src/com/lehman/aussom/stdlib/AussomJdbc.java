/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib;

import com.aussom.Environment;
import com.aussom.ast.aussomException;
import com.aussom.stdlib.ABuffer;
import com.aussom.stdlib.ADate;
import com.aussom.types.*;

import java.sql.*;
import java.util.ArrayList;

public class AussomJdbc {
    protected Connection con;

    protected String driver = "";
    protected String url = "";
    protected String userName = "";
    protected String password = "";

    public AussomJdbc() { }

    public AussomType setDriver(Environment env, ArrayList<AussomType> args) {
        this.driver = ((AussomString)args.get(0)).getValue();
        return env.getCurObj() != null ? env.getCurObj() : new AussomNull();
    }

    public AussomType setUrl(Environment env, ArrayList<AussomType> args) {
        this.url = ((AussomString)args.get(0)).getValue();
        return env.getCurObj() != null ? env.getCurObj() : new AussomNull();
    }

    public AussomType setUserName(Environment env, ArrayList<AussomType> args) {
        this.userName = ((AussomString)args.get(0)).getValue();
        return env.getCurObj() != null ? env.getCurObj() : new AussomNull();
    }

    public AussomType setPassword(Environment env, ArrayList<AussomType> args) {
        this.password = ((AussomString)args.get(0)).getValue();
        return env.getCurObj() != null ? env.getCurObj() : new AussomNull();
    }

    public AussomType getDriver(Environment env, ArrayList<AussomType> args) {
        return new AussomString(this.driver);
    }

    public AussomType getUrl(Environment env, ArrayList<AussomType> args) {
        return new AussomString(this.url);
    }

    public AussomType getUserName(Environment env, ArrayList<AussomType> args) {
        return new AussomString(this.userName);
    }

    public AussomType getPassword(Environment env, ArrayList<AussomType> args) {
        return new AussomString(this.password);
    }

    public AussomType setConnectionInfo(Environment env, ArrayList<AussomType> args) {
        this.driver = ((AussomString)args.get(0)).getValue();
        this.url = ((AussomString)args.get(1)).getValue();
        this.userName = ((AussomString)args.get(2)).getValue();
        this.password = ((AussomString)args.get(3)).getValue();
        return env.getCurObj() != null ? env.getCurObj() : new AussomNull();
    }

    public AussomType select(Environment env, ArrayList<AussomType> args) throws SQLException, aussomException {
        if (this.con != null) {
            String query = ((AussomString) args.get(0)).getValue();
            AussomList params = (AussomList) args.get(1);

            PreparedStatement ps = this.con.prepareStatement(query);
            this.addParamsToPreparedStatement(ps, params);
            // execute the query.
            ResultSet rs = ps.executeQuery();

            // returned object
            AussomMap ret = new AussomMap();

            // First find col info
            AussomList cols = new AussomList();
            ret.put("cols", cols);
            ResultSetMetaData rsmd = rs.getMetaData();
            for (int i = 0; i < rsmd.getColumnCount(); i++) {
                AussomMap colInfo = new AussomMap();
                colInfo.put("name", new AussomString(rsmd.getColumnName(i + 1)));
                colInfo.put("tableName", new AussomString(rsmd.getTableName(i + 1)));
                colInfo.put("label", new AussomString(rsmd.getColumnLabel(i + 1)));
                colInfo.put("type", new AussomString((rsmd.getColumnTypeName(i + 1))));
                colInfo.put("precision", new AussomInt(rsmd.getPrecision(i + 1)));
                colInfo.put("autoIncrement", new AussomBool(rsmd.isAutoIncrement(i + 1)));
                colInfo.put("caseSensitive", new AussomBool(rsmd.isCaseSensitive(i + 1)));
                colInfo.put("currency", new AussomBool(rsmd.isCurrency(i + 1)));
                colInfo.put("nullable", new AussomInt(rsmd.isNullable(i + 1)));
                cols.add(colInfo);
            }

            AussomList rows = new AussomList();
            ret.put("rows", rows);
            while (rs.next()) {
                AussomList row = new AussomList();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                /*
-7	BIT
-6	TINYINT
-5	BIGINT
-4	LONGVARBINARY
-3	VARBINARY
-2	BINARY
-1	LONGVARCHAR
0	NULL
1	CHAR
2	NUMERIC
3	DECIMAL
4	INTEGER
5	SMALLINT
6	FLOAT
7	REAL
8	DOUBLE
12	VARCHAR
91	DATE
92	TIME
93	TIMESTAMP
1111 	OTHER
                 */
                    int colType = rsmd.getColumnType(i + 1);
                    if (colType == 1 || colType == 12 || colType == -1) {
                        row.add(new AussomString(rs.getString(i + 1)));
                    } else if (colType == 2 || colType == 3) {
                        row.add(new AussomString(rs.getBigDecimal(i + 1).toString()));
                    } else if (colType == -7) {
                        row.add(new AussomBool(rs.getBoolean(i + 1)));
                    } else if (colType == -6 || colType == 5 || colType == 4) {
                        row.add(new AussomInt(rs.getLong(i + 1)));
                    } else if (colType == 7 || colType == 6 || colType == 8) {
                        row.add(new AussomDouble(rs.getDouble(i + 1)));
                    } else if (colType == 91 || colType == 92 || colType == 93) {
                        AussomObject co = (AussomObject)env.getClassByName("date").instantiate(env, false, new AussomList());
                        ADate dt = (ADate) co.getExternObject();
                        dt.setTime(rs.getDate(i + 1).getTime());
                        row.add(co);
                    } else if (colType == 0) {
                        row.add(new AussomNull());
                    } else if (colType == -4 || colType == -3 || colType == -2) {
                        AussomObject co = (AussomObject)env.getClassByName("buffer").instantiate(env, false, new AussomList());
                        ABuffer buff = (ABuffer)co.getExternObject();
                        buff.setBuffer(rs.getBytes(i + 1));
                        row.add(co);
                    } else {
                        throw new aussomException("jdbc.select(): Unknown column type " + colType + " '" + rsmd.getColumnTypeName(i + 1) + "' found.");
                    }
                }
                rows.add(row);
            }
            rs.close();
            ps.close();

            return ret;
        } else {
            throw new aussomException("jdbc.select(): Connection is closed.");
        }
    }

    public AussomType update(Environment env, ArrayList<AussomType> args) throws SQLException, aussomException {
        if (this.con != null) {
            String query = ((AussomString) args.get(0)).getValue();
            AussomList params = (AussomList) args.get(1);

            PreparedStatement ps = this.con.prepareStatement(query);
            this.addParamsToPreparedStatement(ps, params);
            // execute the query.
            int rowsAffected = ps.executeUpdate();
            return new AussomInt(rowsAffected);
        } else {
            throw new aussomException("jdbc.update(): Connection is closed.");
        }
    }

    public AussomType connect(Environment env, ArrayList<AussomType> args) {
        try {
            Class.forName(this.driver);
            this.con = DriverManager.getConnection(this.url, this.userName, this.password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return env.getCurObj() != null ? env.getCurObj() : new AussomNull();
    }

    public AussomType disconnect(Environment env, ArrayList<AussomType> args) {
        if (this.con != null) {
            try {
                this.con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                this.con = null;
            }
        }
        return env.getCurObj() != null ? env.getCurObj() : new AussomNull();
    }

    private void addParamsToPreparedStatement(PreparedStatement ps, AussomList params) throws SQLException {
        for (int i = 0; i < params.size(); i++) {
            AussomType param = params.getValue().get(i);
            if (param.getType() == cType.cString)
                ps.setString(i + 1, ((AussomString)param).getValue());
            else if (param.getType() == cType.cBool)
                ps.setBoolean(i + 1, ((AussomBool)param).getValue());
            else if (param.getType() == cType.cInt)
                ps.setLong(i + 1, ((AussomInt)param).getValue());
            else if (param.getType() == cType.cDouble)
                ps.setDouble(i + 1, ((AussomDouble)param).getValue());
            else if (param.getType() == cType.cObject && ((AussomObject)param).getClassDef().instanceOf("date")) {
                ADate dt = (ADate)((AussomObject)param).getExternObject();
                ps.setDate(i + 1, new Date(dt.getTime()));
            } else if (param.getType() == cType.cNull) {
                ps.setNull(i + 1, 0);
            } else {
                // No good, not sure what to do here ... perhaps I should throw an exception instead.
                ps.setString(i + 1, param.getValueString());
            }
        }
    }
}

