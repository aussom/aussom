/*
 * Copyright 2024 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib;

import ca.cgjennings.jvm.JarLoader;
import com.aussom.Environment;
import com.aussom.ast.aussomException;
import com.aussom.types.*;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class AussomApp {
    static AussomApp instance = null;

    public AussomApp() {
        this.init();
    }

    public void init() {
        // Do any initialization here!
    }

    public static AussomApp get() {
        if(instance == null) instance = new AussomApp();
        return instance;
    }

    public AussomType loadJar(Environment env, ArrayList<AussomType> args) throws aussomException {
        if (!(Boolean)env.getEngine().getSecurityManager().getProperty("aussom.app.loadjar"))
            throw new aussomException("app.loadJar(): Security manager property 'aussom.app.loadjar' set to false.");

        String jarFile = (((AussomString)args.get(0)).str());

        // Find the file. First just check path as is.
        File jf = new File(jarFile);
        boolean found = false;
        if (!jf.exists()) {
            for (String path : env.getEngine().getIncludePaths()) {
                jf = new File(path + jarFile);
                if (jf.exists()) {
                    found = true;
                    break;
                }
            }
        } else {
            found = true;
        }

        if (!found) {
            return new AussomException("app.loadJar(): Provided jar file '" + jarFile + "' couldn't be found.");
        }

        try {
            JarLoader.addToClassPath(jf);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return new AussomBool(true);
    }

    public AussomType openWebPage(Environment env, ArrayList<AussomType> args) {
        String url = (((AussomString) args.get(0)).str());
        try {
            this.openWebPage(url);
        } catch (IOException e) {
            return new AussomException(e.getMessage());
        }
        return new AussomNull();
    }

    public void openWebPage(String url) throws IOException {
        File htmlFile = new File(url);
        Desktop.getDesktop().browse(htmlFile.toURI());
    }
}
