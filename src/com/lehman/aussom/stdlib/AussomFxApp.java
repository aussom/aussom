package com.lehman.aussom.stdlib;

import com.aussom.Environment;
import com.aussom.ast.aussomException;
import com.aussom.stdlib.console;
import com.aussom.types.*;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class AussomFxApp extends Stage {
	private Scene scene = null;


	private String title = "";
	private int width = 0;
	private int height = 0;

	private MenuBar mBar = null;
	private Parent layout = null;
	private boolean maximized = false;
	private boolean fullScreen = false;

	/* Block main execution thread */
	private boolean block = true;

	/* Event handlers */
	private AussomCallback onShown = null;
	private AussomCallback onClosing = null;

	/* Style sheet files */
	private List<String> styleSheets = new ArrayList<String>();
	private String style = null;
	private String id = null;

	/**
	 * Empty default constructor
	 */
	public AussomFxApp() {
		super();
	}

	public void set(String title, Integer width, Integer height) {
		this.title = title;
		if ((width != null) && (height != null))
			this.setBounds(width, height);
	}


	//public void setTitle(String Title) { this.title = Title; }

	//public Scene getScene() { return this.scene; }

	//public void setScene(Scene theScene) { this.scene = theScene; }

	public void setBounds(int Width, int Height) {
		this.width = Width;
		this.height = Height;
	}

	public Parent getLayout() {
		return this.layout;
	}

	public void setLayout(Parent Layout) {
		this.layout = Layout;
	}

	public List<String> getStyleSheets() {
		return this.styleSheets;
	}

	public void setStyleSheets(List<String> StyleSheets) {
		this.styleSheets = StyleSheets;
	}

	public void show(Environment env, boolean block) {
		this.block = block;

		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				setTitle(title);
				setMaximized(maximized);

				if (onShown != null) registerOnShown(env);
				registerOnClosing(env);

				if (mBar != null) {
					if ((width != 0) && (height != 0))
						scene = new Scene(new VBox(), width, height);
					else
						scene = new Scene(new VBox());
					((VBox) scene.getRoot()).getChildren().addAll(mBar, layout);
					VBox.setVgrow(layout, Priority.ALWAYS);
				} else {
					if (layout == null) layout = new HBox();

					if ((width != 0) && (height != 0))
						scene = new Scene(layout, width, height);
					else
						scene = new Scene(layout);
				}

				for (String ss : styleSheets) {
					scene.getStylesheets().add(ss);
				}
				if (style != null) {
					scene.getRoot().setStyle(style);
				}
				if (id != null) {
					scene.getRoot().setId(id);
				}

				setScene(scene);
				show();
			}
		});

		/* If blocking is set. */
		this.handleBlock();
	}

	private void handleBlock() {
		if (this.block) {
			while (this.block) {
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void addStyleSheet(String SSFileName) throws aussomException {
		// If default stage not set, set it now.
		final FxApplication app = FxApplication.getInstance();
		app.setStage(this);

		final File f = new File(SSFileName);
		try {
			this.styleSheets.add(f.toURI().toURL().toExternalForm());
		} catch (MalformedURLException e) {
			throw new aussomException("fxApp.addStyleSheet(): Malformed URL exception. " + e.getMessage());
		}

		if (this.scene != null) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					try {
						scene.getStylesheets().add(f.toURI().toURL().toExternalForm());
					} catch (MalformedURLException e) {
						console.get().err("fxApp.addStyleSheet(): Malformed URL exception. " + e.getMessage());
					}
				}
			});
		}
	}

	public void registerOnShown(Environment env) {
		if (this.onShown == null) this.setOnShown(null);
		else {
			setOnShown(new EventHandler<WindowEvent>() {
				@Override
				public void handle(WindowEvent event) {
					if (onShown != null) {
						onShown.call(env, new AussomList());
					}
				}
			});
		}
	}

	public void registerOnClosing(Environment env) {
		setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				if (onClosing != null) {
					onClosing.call(env, new AussomList());
				}
				block = false;
			}
		});
	}

	public AussomType getAjo(Environment env, ArrayList<AussomType> args) throws aussomException {
		AussomObject ao = env.getEngine().instantiateObject("AussomJavaObject");
		AussomJavaObject ajo = (AussomJavaObject) ao.getExternObject();
		ajo.setObj(this);
		ajo.setName(this.getClass().getName());
		return ao;
	}

	public AussomType _show(Environment env, ArrayList<AussomType> args) {
		if(!args.get(0).isNull()) this.block = ((AussomBool)args.get(0)).getValue();
		this.show(env, this.block);
		return env.getClassInstance();
	}

	public AussomType _close(Environment env, ArrayList<AussomType> args) {
		this.shutdown();
		return env.getClassInstance();
	}

	private void shutdown() {
		this.close();
		fireEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSE_REQUEST));
	}

	public AussomType add(Environment env, ArrayList<AussomType> args) throws aussomException {
        Node node = null;
        if (args.get(0).getType() == cType.cObject) {
            AussomObject ao = this.getAussomJavaObject((AussomObject) args.get(0));
            if (ao.getExternObject() instanceof AussomJavaObject) {
                AussomJavaObject ajo = (AussomJavaObject) ao.getExternObject();
                if (ajo.getObj() instanceof Node) {
                    node = (Node) ajo.getObj();
                } else {
                    throw new aussomException("fxApp.add(): Provided AussomJavaObject internal object is expected to be of type JavaFX Node but found  '" + ajo.getObj().getClass().getName() + "' instead.");
                }
            } else {
                throw new aussomException("fxApp.add(): Expecting provided object to be of type AussomJavaObject but found '" + ao.getExternObject().getClass().getName() + "' instead.");
            }
        } else {
            throw new aussomException("fxApp.add(): Expecting type object but found '" + args.get(0).getType().name() + "' instead.");
        }
        if(layout == null) layout = new HBox();
        if(this.layout instanceof Pane)
            ((Pane)this.layout).getChildren().add(node);
        else if(this.layout instanceof ScrollPane)
            ((ScrollPane)this.layout).setContent(node);
        return env.getClassInstance();
	}

	public String getStyleString() {
		if(this.scene != null) return this.scene.getRoot().getStyle();
		else if(this.style != null) return this.style;
		else return "";
	}

	public AussomType newFxApp(Environment env, ArrayList<AussomType> args) {
		this.title = ((AussomString)args.get(0)).getValue();
		if((!args.get(1).isNull())&&(!args.get(2).isNull())) {
			this.width = (int) ((AussomInt)args.get(1)).getValue();
			this.height = (int)((AussomInt)args.get(2)).getValue();
		}
		//FxApplication.getInstance().initApp();
		// Default layout
		FlowPane fp = new FlowPane();
		fp.setVgap(8);
	    fp.setHgap(4);
		this.layout = fp;
		return env.getClassInstance();
	}

	public static AussomType openInBrowser(Environment env, ArrayList<AussomType> args) {
		FxApplication inst = FxApplication.getInstance();
		inst.getHostServices().showDocument(((AussomString)args.get(0)).getValue());
		return env.getClassInstance();
	}

	/* Event handlers */
	public AussomType setOnShow(Environment env, ArrayList<AussomType> args) {
		if(args.get(0).isNull()) { this.onShown = null; }
		else { this.onShown = (AussomCallback) args.get(0); }
		this.registerOnShown(env);
		return env.getClassInstance();
	}

	public AussomType setOnClosing(Environment env, ArrayList<AussomType> args) {
		if(args.get(0).isNull()) { this.onClosing = null; }
		else { this.onClosing = (AussomCallback)args.get(0); }
		this.registerOnClosing(env);
		return env.getClassInstance();
	}

	/* Getters */
	public AussomType getMaximized(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(isMaximized());
	}

	public AussomType getFullScreen(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(this.isFullScreen());
	}

	/* Setters */
	public AussomType setLayout(Environment env, ArrayList<AussomType> args) throws aussomException {
        if (args.get(0).getType() == cType.cObject) {
            AussomObject ao = this.getAussomJavaObject((AussomObject) args.get(0));
            if (ao.getExternObject() instanceof AussomJavaObject) {
                AussomJavaObject ajo = (AussomJavaObject) ao.getExternObject();
                if (ajo.getObj() instanceof Parent) {
                    this.layout = (Parent) ajo.getObj();
                } else {
                    throw new aussomException("fxApp.setLayout(): Provided AussomJavaObject internal object is expected to be of type JavaFX Parent but found  '" + ajo.getObj().getClass().getName() + "' instead.");
                }
            } else {
                throw new aussomException("fxApp.setLayout(): Expecting provided object to be of type AussomJavaObject but found '" + ao.getExternObject().getClass().getName() + "' instead.");
            }
        } else {
            throw new aussomException("fxApp.setLayout(): Expecting type object but found '" + args.get(0).getType().name() + "' instead.");
        }
        return env.getClassInstance();
	}

	public AussomType setMenuBar(Environment env, ArrayList<AussomType> args) throws aussomException {
		if (args.get(0).getType() == cType.cObject) {
            AussomObject ao = this.getAussomJavaObject((AussomObject) args.get(0));
            if (ao.getExternObject() instanceof AussomJavaObject) {
                AussomJavaObject ajo = (AussomJavaObject) ao.getExternObject();
                if (ajo.getObj() instanceof MenuBar) {
                    this.mBar = (MenuBar) ajo.getObj();
                } else {
                    throw new aussomException("fxApp.setMenuBar(): Provided AussomJavaObject internal object is expected to be of type JavaFX MenuBar but found  '" + ajo.getObj().getClass().getName() + "' instead.");
                }
            } else {
                throw new aussomException("fxApp.setMenuBar(): Expecting provided object to be of type AussomJavaObject but found '" + ao.getExternObject().getClass().getName() + "' instead.");
            }
        } else {
            throw new aussomException("fxApp.setMenuBar(): Expecting type object but found '" + args.get(0).getType().name() + "' instead.");
        }
        return env.getClassInstance();
	}

	public AussomType setMaximized(Environment env, ArrayList<AussomType> args) {
		boolean maxed = ((AussomBool)args.get(0)).getValue();
		this.maximized = maxed;
		setMaximized(this.maximized);
		return env.getClassInstance();
	}

	public AussomType setFullScreen(Environment env, ArrayList<AussomType> args) {
		this.fullScreen = ((AussomBool)args.get(0)).getValue();
		Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                	setFullScreen(fullScreen);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return env.getClassInstance();
	}

	public AussomType getStyle(Environment env, ArrayList<AussomType> args) {
		return new AussomString(this.getStyleString());
	}

	public AussomType getId(Environment env, ArrayList<AussomType> args) {
		if(this.scene != null) return new AussomString(this.scene.getRoot().getId());
		else if(this.id != null) return new AussomString(this.id);
		else return new AussomString();
	}

	public AussomType setStyle(Environment env, ArrayList<AussomType> args) {
		this.style = ((AussomString)args.get(0)).getValue();
		if(this.scene != null) this.scene.getRoot().setStyle(((AussomString)args.get(0)).getValue());
		return env.getClassInstance();
	}

	public AussomType setId(Environment env, ArrayList<AussomType> args) {
		this.id = ((AussomString)args.get(0)).getValue();
		if(this.scene != null) this.scene.getRoot().setId(((AussomString)args.get(0)).getValue());
		return env.getClassInstance();
	}

	public AussomType setToolTip(Environment env, ArrayList<AussomType> args) throws aussomException {
        if (args.size() > 0) {
            if (args.get(0).getType() == cType.cObject) {
                AussomObject ao = this.getAussomJavaObject((AussomObject) args.get(0));
                if (ao.getExternObject() instanceof AussomJavaObject) {
                    AussomJavaObject ajo = (AussomJavaObject) ao.getExternObject();
                    if (ajo.getObj() instanceof Tooltip) {
                        // We need to invoke later in order for the tooltip to be constructed.
                        // Otherwise the tooltip is just set to null.
                        final Node n = this.layout;
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                Tooltip.install(n, ((Tooltip)ajo.getObj()));
                            }
                        });
                    } else {
                        throw new aussomException("fxApp.setToolTip(): Provided AussomJavaObject internal object is expected to be of type JavaFX Tooltip but found  '" + ajo.getObj().getClass().getName() + "' instead.");
                    }
                } else {
                    throw new aussomException("fxApp.setToolTip(): Expecting provided object to be of type AussomJavaObject but found '" + ao.getExternObject().getClass().getName() + "' instead.");
                }
            } else {
                throw new aussomException("fxApp.setToolTip(): Expecting type object but found '" + args.get(0).getType().name() + "' instead.");
            }
        } else {
            Tooltip.install(this.layout, null);
        }
		return env.getClassInstance();
	}

	/**
	 * Gets the AussomJavaObject with the provided AussomObject. If the current
	 * object is of type AussomJavaObject, it is just returned. Otherwise, it
	 * looks for the member (this.obj) with the type AussomJavaObject.
	 * @param ao is an AussomObject to check.
	 * @return An AussomObject with the AussomJavaObject or the original
	 * object.
	 */
	private AussomObject getAussomJavaObject(AussomObject ao) {
		AussomObject ret = ao;

		if (!(ao.getExternObject() instanceof AussomJavaObject)) {
			if (ao.getMembers().contains("obj")) {
				AussomType at = ao.getMembers().get("obj");
				if (at instanceof AussomObject) {
					AussomObject innerao = (AussomObject) at;
					if (innerao.getExternObject() instanceof AussomJavaObject)
						ret = innerao;
				}
			}
		}

		return ret;
	}
}