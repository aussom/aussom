/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib;

import com.aussom.Environment;
import com.aussom.Util;
import com.aussom.ast.aussomException;
import com.aussom.stdlib.ABuffer;
import com.aussom.types.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

/**
 * Class with functions that support the aussom file functions.
 */
public class AussomFile {
    /**
     * Reads a text file with the provided file name.
     * @param env
     * @param args
     * @return A AussomString with the file contents or a AussomExcpetion.
     */
    public AussomType read(Environment env, ArrayList<AussomType> args) {
        String fileName = (((AussomString)args.get(0)).str());
        String data = "";
        try {
            data = Util.read(fileName);
        } catch (IOException e) {
            return new AussomException("file.read(): Failed to read file '" + fileName + "': " + e.getMessage());
        }
        return new AussomString(data);
    }

    public AussomType write(Environment env, ArrayList<AussomType> args) {
        String fileName = (((AussomString)args.get(0)).str());
        String data = (((AussomString)args.get(1)).str());
        boolean append = false;
        if (args.size() == 3) {
            append = (((AussomBool)args.get(2)).getValue());
        }

        try {
            Util.write(fileName, data, append);
        } catch (IOException e) {
            return new AussomException("file.write(): Failed to write file '" + fileName + "': " + e.getMessage());
        }
        return new AussomBool(true);
    }

    public AussomType writeBinary(Environment env, ArrayList<AussomType> args) {
        String fileName = (((AussomString)args.get(0)).str());
        AussomObject cobj = (AussomObject)args.get(1);
        ABuffer data = (ABuffer) cobj.getExternObject();

        try {
            OutputStream os = new FileOutputStream(new File(fileName));
            os.write(data.getBuffer());
            os.close();
        } catch (IOException e) {
            return new AussomException("file.write(): Failed to write file '" + fileName + "': " + e.getMessage());
        }
        return new AussomBool(true);
    }

    public AussomType _readBinary(Environment env, ArrayList<AussomType> args) throws aussomException {
        String fileName = (((AussomString)args.get(0)).str());
        AussomObject cobj = (AussomObject)args.get(1);
        ABuffer data = (ABuffer)cobj.getExternObject();

        try {
            InputStream is = new FileInputStream(new File(fileName));

            byte[] buff = new byte[10000];
            int read = 0;
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            while((read = is.read(buff)) != -1) {
                bao.write(buff, 0, read);
            }
            data.setBuffer(bao.toByteArray());

            is.close();
        } catch (IOException e) {
            return new AussomException("file.write(): Failed to write file '" + fileName + "': " + e.getMessage());
        }
        return cobj;
    }

    public AussomType ls(Environment env, ArrayList<AussomType> args) throws IOException, aussomException {
        AussomList ret = new AussomList();
        String dirName = (((AussomString)args.get(0)).str());

        File dir = new File(dirName);
        File[] files = dir.listFiles();

        if (files != null) {
            // Get the names of the files by using the .getName() method
            for (File f : files) {
                AussomMap fdata = new AussomMap();
                fdata.put("name", new AussomString(f.getName()));
                fdata.put("path", new AussomString(f.getPath()));
                fdata.put("absolutePath", new AussomString(f.getAbsolutePath()));
                fdata.put("canonicalPath", new AussomString(f.getCanonicalPath()));
                fdata.put("parent", new AussomString(f.getParent()));
                fdata.put("size", new AussomInt(f.length()));
                fdata.put("canRead", new AussomBool(f.canRead()));
                fdata.put("canWrite", new AussomBool(f.canWrite()));
                fdata.put("canExecute", new AussomBool(f.canExecute()));
                fdata.put("isDirectory", new AussomBool(f.isDirectory()));
                fdata.put("isFile", new AussomBool(f.isFile()));
                fdata.put("isHidden", new AussomBool(f.isHidden()));
                fdata.put("uri", new AussomString(f.toURI().toString()));
                ret.add(fdata);
            }
        } else {
            throw new aussomException("file.ls(): Provided directory '" + dirName + "' not found.");
        }

        return ret;
    }

    public AussomType rm(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomBool((new File(fileName)).delete());
    }

    public AussomType rmr(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        File f = new File(fileName);
        if (f.exists() && f.isDirectory()) {
            return new AussomBool(this.removeRecursivly(f));
        } else {
            return new AussomBool((new File(fileName)).delete());
        }
    }

    private boolean removeRecursivly(File Dir) {
        for (File f : Dir.listFiles()) {
            if (f.isDirectory()) {
                boolean res = this.removeRecursivly(f);
                if (!res) return res;
            } else {
                boolean res = f.delete();
                if (!res) return res;
            }
        }
        return Dir.delete();
    }

    public AussomType exists(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomBool((new File(fileName)).exists());
    }

    public AussomType isFile(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomBool((new File(fileName)).isFile());
    }

    public AussomType isDir(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomBool((new File(fileName)).isDirectory());
    }

    public AussomType canExecute(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomBool((new File(fileName)).canExecute());
    }

    public AussomType canRead(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomBool((new File(fileName)).canRead());
    }

    public AussomType canWrite(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomBool((new File(fileName)).canWrite());
    }

    public AussomType getAbsPath(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomString((new File(fileName)).getAbsolutePath());
    }

    public AussomType getCanonicalPath(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomString((new File(fileName)).getCanonicalPath());
    }

    public AussomType getName(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomString((new File(fileName)).getName());
    }

    public AussomType getParent(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomString((new File(fileName)).getParent());
    }

    public AussomType getPath(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomString((new File(fileName)).getPath());
    }

    public AussomType isAbsolute(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomBool((new File(fileName)).isAbsolute());
    }

    public AussomType isHidden(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomBool((new File(fileName)).isHidden());
    }

    public AussomType lastModified(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomInt((new File(fileName)).lastModified());
    }

    public AussomType length(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        return new AussomInt((new File(fileName)).length());
    }

    public AussomType mkdir(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        File f = new File(fileName);
        f.mkdir();
        return new AussomBool(true);
    }

    public AussomType mkdirs(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        File f = new File(fileName);
        f.mkdirs();
        return new AussomBool(true);
    }

    public AussomType rename(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        String newFileName = (((AussomString)args.get(0)).str());
        File sf = new File(fileName);
        File df = new File(newFileName);
        sf.renameTo(df);
        return new AussomBool(true);
    }

    public AussomType setExecutable(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        File f = new File(fileName);
        f.setExecutable(true);
        return new AussomBool(true);
    }

    public AussomType setReadable(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        File f = new File(fileName);
        f.setReadable(true);
        return new AussomBool(true);
    }

    public AussomType setWritable(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = (((AussomString)args.get(0)).str());
        File f = new File(fileName);
        f.setWritable(true);
        return new AussomBool(true);
    }

    public AussomType cp(Environment env, ArrayList<AussomType> args) throws IOException {
        String src = (((AussomString)args.get(0)).str());
        String dst = (((AussomString)args.get(1)).str());
        boolean replace = (((AussomBool)args.get(2)).getValue());
        Path sp = Path.of(src);
        Path dp = Path.of(dst);
        if (replace)
            Files.copy(sp, dp, StandardCopyOption.REPLACE_EXISTING);
        else
            Files.copy(sp, dp, StandardCopyOption.ATOMIC_MOVE);
        return new AussomBool(true);
    }

    public AussomType cpr(Environment env, ArrayList<AussomType> args) throws IOException {
        String src = (((AussomString)args.get(0)).str());
        String dst = (((AussomString)args.get(1)).str());
        boolean replace = (((AussomBool)args.get(2)).getValue());
        this.copyRecursive(src, dst, replace);
        return new AussomBool(true);
    }

    private void copyRecursive(String src, String dst, boolean replace) throws IOException {
        Path sp = Path.of(src);
        Path dp = Path.of(dst);
        if (replace)
            Files.copy(sp, dp, StandardCopyOption.REPLACE_EXISTING);
        else
            Files.copy(sp, dp, StandardCopyOption.ATOMIC_MOVE);

        // Descendants
        File sf = new File(src);
        if (sf.isDirectory()) {
            for (File f : sf.listFiles()) {
                this.copyRecursive(f.getPath(), dst + "/" + f.getName(), replace);
            }
        }
    }
}
