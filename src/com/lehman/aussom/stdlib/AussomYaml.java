/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib;

import com.aussom.Environment;
import com.aussom.Util;
import com.aussom.types.*;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AussomYaml {
    public static AussomType parse(Environment env, ArrayList<AussomType> args) {
        String data = ((AussomString)args.get(0)).getValue();
        AussomMap ret = new AussomMap();

        Yaml yaml = new Yaml();
        Map<String, Object> obj = yaml.load(data);
        AussomYaml.mapYamlMap(obj, ret);

        return ret;
    }

    public AussomType parseFile(Environment env, ArrayList<AussomType> args) throws IOException {
        String fileName = ((AussomString)args.get(0)).getValue();
        try {
            ArrayList<AussomType> fargs = new ArrayList<AussomType>();
            fargs.add(new AussomString(Util.read(fileName)));
            return AussomYaml.parse(env, fargs);
        } catch (IOException e) {
            return new AussomException("file.read(): Failed to read file '" + fileName + "': " + e.getMessage());
        }
    }

    private static void mapYamlMap(Map<String, Object> obj, AussomMap ret) {
        for (String key : obj.keySet()) {
            Object tobj = obj.get(key);
            ret.put(key, AussomYaml.mapAussomType(tobj));
        }
    }

    private static void mapYamlList(ArrayList<Object> lst, AussomList ret) {
        for (Object tobj : lst) {
            ret.add(AussomYaml.mapAussomType(tobj));
        }
    }

    private static AussomType mapAussomType(Object tobj) {
        AussomType t = new AussomNull();
        if(tobj instanceof Boolean) {
            t = new AussomBool((Boolean)tobj);
        } else if (tobj instanceof Integer) {
            t = new AussomInt((Integer)tobj);
        } else if (tobj instanceof String) {
            t = new AussomString((String)tobj);
        } else if (tobj instanceof ArrayList) {
            t = new AussomList();
            AussomYaml.mapYamlList((ArrayList<Object>)tobj, (AussomList) t);
        } else if (tobj instanceof HashMap) {
            t = new AussomMap();
            AussomYaml.mapYamlMap((Map<String, Object>) tobj, (AussomMap) t);
        } else {
            System.out.println("Unmapped value found: " + tobj.getClass().getName());
        }
        return t;
    }
}
