/*
 * Copyright 2024 Austin Lehman
 */

include fx.Image;

/**
 * Represents a JavaFX ImageView, a component for displaying images.
 * The ImageView class supports setting an image via an Image object, URL string, or null.
 */
class ImageView : Node {

    /**
     * Creates a new ImageView object with either an Image object, a string URL, or null for no image.
     * @p Image An Image object, URL string, or null.
     * @r this object
     */
    public ImageView(Image = null) {
        if (Image instanceof 'string') {
            this.obj = aji.newObj("javafx.scene.image.ImageView", Image);
        } else if (Image instanceof 'Image') {
            this.obj = aji.newObj("javafx.scene.image.ImageView", Image.obj);
        } else {
            if (!this.copy(Val)) {
                this.obj = aji.newObj("javafx.scene.image.ImageView");
            }
        }
    }

    /**
     * Sets the image for the ImageView.
     * @p Image An Image object to display in the ImageView.
     * @r this object for chaining
     */
    public setImage(object Image) {
        this.obj.invoke("setImage", Image.obj);
        return this;
    }
}