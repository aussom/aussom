/*
 * Copyright 2024 Austin Lehman
 */

/**
 * Represents a JavaFX ListView, a control for displaying a list of items.
 * The ListView class allows for item selection, change listeners, and retrieval
 * of the selected item.
 */
class ListView : Node, Control, Region {

    /**
     * Initializes a new ListView instance with optional items.
     * If an item list is provided, it is set as the initial content of the ListView.
     * @p Items An array of items to populate the ListView, or null for an empty list.
     * @r this object
     */
    public ListView(Items = null) {
        if (!this.copy(Items)) {
            this.obj = aji.newObj("javafx.scene.control.ListView");
            if (Items != null) {
                olist = this.observableArrayList();
                for (item : Items) {
                    olist.invoke("add", item);
                }
                this.obj.invoke("setItems", olist);
            }
        }
    }

    /**
     * Sets a callback function that triggers when the selected item changes.
     * @p OnChange The callback function to execute on item selection change.
     * @r this object for chaining
     */
    public onChange(callback OnChange) {
        smodel = this.getSelectionModel();
        siprop = smodel.invoke("selectedItemProperty");
        siprop.invoke("addListener", aji.closure("javafx.beans.value.ChangeListener", OnChange));
        return this;
    }

    /**
     * Retrieves the currently selected item in the ListView.
     * @r The selected item object, or null if no item is selected.
     */
    public getSelected() {
        return this.getSelectionModel().invoke("getSelectedItem");
    }

    /**
     * Internal method to get the selection model for the ListView.
     * @r The selection model for managing item selection.
     */
    private getSelectionModel() {
        return this.obj.invoke("getSelectionModel");
    }
}
