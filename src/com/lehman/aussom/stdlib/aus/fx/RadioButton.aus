/*
 * Copyright 2024 Austin Lehman
 */

include fx.Labeled;
include fx.ToggleButton;

/**
 * Represents a JavaFX RadioButton, a selectable button in a group where only one can be selected.
 * The RadioButton class extends Node, Control, Labeled, and ToggleButton.
 */
class RadioButton : Node, Control, Labeled, ToggleButton {

    /**
     * Initializes a new RadioButton instance with the specified label text.
     * If no text is provided, the label defaults to an empty string.
     * @p Val The text label for the RadioButton, or null for a blank label.
     * @r this object
     */
    public RadioButton(Val) {
        if (!this.copy(Val)) {
            str = Val;
            if (Val == null) { str = ""; }
            this.obj = aji.newObj("javafx.scene.control.RadioButton", str);
        }
    }
}
