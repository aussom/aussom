/*
 * Copyright 2024 Austin Lehman
 */

/**
 * Color object stores a JavaFX Color object and provides
 * functions to set it.
 */
class Color : FxObj {
    /**
     * Creates a new Color object with the provied optional
     * hex string and alpha double values.
     * @p Val is an optional string with the web hex
     * value to set. (ex. #ffffff)
     * @p Alpha is an optional double between 0 and 1 with the alpha value.
     * @r this object
     */
    public Color(string Val = null, double Alpha = 1.0) {
        color = Val;
        if (Val == null) {
            color = "#000000";
        }
        this.obj = aji.invokeStatic("javafx.scene.paint.Color", "web", color, Alpha);
    }

    /**
     * Set the color value ith the provied
     * hex string and optional alpha double values.
     * @p Val is an optional string with the web hex
     * value to set. (ex. #ffffff)
     * @p Alpha is an optional double between 0 and 1 with the alpha value.
     * @r this object
     */
    public web(string Val, double Alpha = 1.0) {
        this.obj = aji.invokeStatic("javafx.scene.paint.Color", "web", Val, Alpha);
        return this;
    }

    /**
     * Set the color value ith the provied
     * red blue green values.
     * @p Red is a double with the red part. (0.0-255.0)
     * @p Blue is a double with the blue part. (0.0-255.0)
     * @p Green is a double with the green part. (0.0-255.0)
     * @r this object
     */
    public rgb(double Red, double Blue, double Green) {
        this.obj = aji.invokeStatic("javafx.scene.paint.Color", "rgb", Red, Blue, Green);
        return this;
    }
}