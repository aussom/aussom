/*
 * Copyright 2024 Austin Lehman
 */

/**
 * Jdbc class implements functionality for communicating with
 * RDBMS systems using Java JDBC.
 */
extern class jdbc : com.lehman.aussom.stdlib.AussomJdbc {
    /**
     * Sets the JDBC driver string.
     * @p Driver is a string with the JDBC driver.
     * @r This object.
     */
    public extern setDriver(string Driver);

    /**
     * Sets the DB URL string.
     * @p Url is a string with the DB URL.
     * @r This object.
     */
    public extern setUrl(string Url);

    /**
     * Sets the DB user name.
     * @p UserName is the DB user name.
     * @r This object.
     */
    public extern setUserName(string UserName);

    /**
     * Sets the DB password.
     * @p Password is the DB password.
     * @r This object.
     */
    public extern setPassword(string Password);

    /**
     * Gets the JDBC driver string.
     * @r A string with the JDBC driver.
     */
    public extern getDriver();

    /**
     * Gets the DB URL.
     * @r A string with the DB URL.
     */
    public extern getUrl();

    /**
     * Gets the DB user name.
     * @r A string with the DB user name.
     */
    public extern getUserName();

    /**
     * Gets the DB password.
     * @r A string with the DB password.
     */
    public extern getPassword();

    /**
     * Sets all of the JDBC connection info in a single function call.
     * @p Driver is the JDBC driver string.
     * @p Url is the JDBC Url.
     * @p UserName is a string with the DB user name.
     * @p Password is a string with the DB password.
     */
    public extern setConnectionInfo(string Driver, string Url, string UserName, string Password);

    /**
     * Performs a select query with the provided arguments and returns
     * the results. It returns a map with 'cols' and 'rows'.
     * @p Query is a string with the JDBC query.
     * @p Params is an optional list with prepared statement arguments.
     * @r A map with the query result in 'cols' and 'rows'.
     */
    public extern select(string Query, list Params = []);

    /**
     * Performs an update query with the provided arguments.
     * @p Query is a string with the JDBC query.
     * @p Params is an optional list with prepared statement arguments.
     * @r An int with the number of rows affected.
     */
    public extern update(string Query, list Params = []);

    /**
     * Attempts to connect to the database.
     * @r This object.
     */
    public extern connect();

    /**
     * Attempts to disconnect from the database.
     * @r This object.
     */
    public extern disconnect();
}