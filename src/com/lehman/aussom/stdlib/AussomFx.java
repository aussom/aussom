package com.lehman.aussom.stdlib;

import com.aussom.Engine;
import com.aussom.Environment;
import com.aussom.ast.astClass;
import com.aussom.ast.aussomException;
import com.aussom.types.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.stage.Modality;

import java.lang.reflect.*;
import java.util.ArrayList;

public class AussomFx {
    public AussomType fxApp(Environment env, ArrayList<AussomType> args) throws aussomException {
        String title = ((AussomString)args.get(0)).str();
		Integer width = null;
		Integer height = null;
		if((!args.get(1).isNull())&&(!args.get(2).isNull())) {
			width = (int)((AussomInt)args.get(1)).getValue();
			height = (int)((AussomInt)args.get(2)).getValue();
		}
		return fxApplication(env, title, width, height, false);
	}

	public AussomType fxDialog(Environment env, ArrayList<AussomType> args) throws aussomException {
		String title = ((AussomString)args.get(0)).str();
		Integer width = null;
		Integer height = null;
		if((!args.get(1).isNull())&&(!args.get(2).isNull())) {
			width = (int)((AussomInt)args.get(1)).getValue();
			height = (int)((AussomInt)args.get(2)).getValue();
		}
		return fxApplication(env, title, width, height, true);
	}

	public AussomType runLater(Environment env, ArrayList<AussomType> args) {
		final AussomCallback toCall = (AussomCallback)args.get(0);
		final AussomList runArgs = (AussomList)args.get(1);
		Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                	toCall.call(env, runArgs);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });
		return env.getClassInstance();
	}

	public AussomType isUiThread(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(Platform.isFxApplicationThread());
	}

    public AussomType shutdown(Environment env, ArrayList<AussomType> args) {
        FxApplication.getInstance().stopInstance();
        return env.getClassInstance();
    }

    public AussomType  toHexString(Environment env, ArrayList<AussomType> args) throws aussomException {
        AussomObject ao = (AussomObject) args.get(0);
        if (ao.getExternObject() instanceof AussomJavaObject) {
            AussomJavaObject ajo = (AussomJavaObject) ao.getExternObject();
            Color color = (Color)ajo.getObj();
            if (ajo.getObj() instanceof Color) {
                return new AussomString(String.format("#%02X%02X%02X",
                    (int) (color.getRed() * 255),
                    (int) (color.getGreen() * 255),
                    (int) (color.getBlue() * 255)));
            } else {
                throw new aussomException("fx.toHexString(): AussomJavaObject expecting type Color but found '" + ajo.getObj().getClass().getName() + "' instead.");
            }
        } else {
            throw new aussomException("fx.toHexString(): Expecting AussomJavaObject with java Color object but found '" + ao.getExternObject().getClass().getName() + "' instead.");
        }
    }

    private AussomType fxApplication(Environment env, final String title, final Integer width, final Integer height, final boolean modal) throws aussomException {
        Engine eng = env.getEngine();
		if(eng.getClasses().containsKey("fxApp")) {
            // Hack here, switch to not extern, then back.
            astClass ac = env.getClassByName("fxApp");
            ac.setExtern(false);
            AussomObject ao  = (AussomObject) ac.instantiate(env, false, new AussomList());
            ao.setExternObject(null); // Needed because we wait for this to become != false later.
            ac.setExtern(true);
            final FxApplication app = FxApplication.getInstance();
            if(!Platform.isFxApplicationThread()) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        AussomFxApp newStage = new AussomFxApp();
                        newStage.set(title, width, height);
                        newStage.setStyleSheets(app.getStyleSheets());
                        if(modal)
                            newStage.initModality(Modality.APPLICATION_MODAL);
                        ao.setExternObject(newStage);
                    }
                });
                // Wait for extern object to be set in UI thread.
                while(ao.getExternObject() == null) { try { Thread.sleep(200); } catch (InterruptedException e) { e.printStackTrace(); } }
            } else {
                AussomFxApp newStage = new AussomFxApp();
                newStage.set(title, width, height);
                newStage.setStyleSheets(app.getStyleSheets());
                if(modal)
                    newStage.initModality(Modality.APPLICATION_MODAL);
                ao.setExternObject(newStage);
            }

            return ao;
        }
		else
			throw new aussomException("fx.fxApp(): Class 'fxApp' not found.");
	}
}
