package com.lehman.aussom.stdlib;

import com.aussom.Environment;
import com.aussom.types.AussomString;
import com.aussom.types.AussomType;
import org.commonmark.Extension;
import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import java.util.ArrayList;
import java.util.List;

public class AussomMarkdown {

    static AussomMarkdown instance = null;

    public AussomMarkdown() {
        this.init();
    }

    public void init() {
        // Do any initialization here!
    }

    public static AussomMarkdown get() {
        if(instance == null) instance = new AussomMarkdown();
        return instance;
    }

    /**
     * Converts the provided markdown string to HTML.
     * @param env
     * @param args
     * @return A AussomString with the converted HTML text.
     */
    public AussomType toHtml(Environment env, ArrayList<AussomType> args) {
        String markdownStr = (((AussomString)args.get(0)).str());

        List<Extension> extensions = List.of(TablesExtension.create());
        Parser mp = Parser
                .builder()
                .extensions(extensions)
                .build();
        Node doc = mp.parse(markdownStr);
        HtmlRenderer renderer = HtmlRenderer
                .builder()
                .extensions(extensions)
                .build();
        String htmlStr = renderer.render(doc);

        return new AussomString(htmlStr);
    }
}
