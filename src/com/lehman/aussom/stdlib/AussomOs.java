package com.lehman.aussom.stdlib;

import com.aussom.Environment;
import com.aussom.stdlib.console;
import com.aussom.types.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class AussomOs {
    private static class StreamLogger extends Thread {
        private InputStream is;
        private boolean logToStdOut = true;
        private StringBuffer sb = new StringBuffer();

        public StreamLogger(InputStream inputStream, boolean LogToStdOut) {
            this.is = inputStream;
            this.logToStdOut = LogToStdOut;
        }

        public String getResult() { return this.sb.toString(); }

        @Override
        public void run() {
            BufferedReader br = new BufferedReader(new InputStreamReader(this.is));
            String line = null;
            try {
                line = br.readLine();
                while(line != null) {
                    sb.append(line + System.lineSeparator());
                    if (logToStdOut)
                        console.get().log(line);
                    line = br.readLine();
                }
            } catch (IOException e) {
                sb.append(e.getMessage() + System.lineSeparator());
                if (logToStdOut)
                    console.get().err(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public AussomType exec(Environment env, ArrayList<AussomType> args) {
        AussomMap ret = new AussomMap();
        ret.getValue().put("success", new AussomBool(true));
        ret.getValue().put("result", new AussomString(""));


        String cmd = ((AussomString) args.get(0)).getValue();
        boolean logToStdOut = ((AussomBool) args.get(1)).getValue();

        boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");

        Process process = null;
        try {
            String cmdStr[] = new String[] { "/bin/sh", "-c", cmd };
            if (isWindows) {
                cmdStr = new String[] { "cmd.exe", "-c", cmd };
            }
            process = Runtime.getRuntime().exec(cmdStr);
        } catch (IOException e) {
            console.get().err("IOException: " + e.getMessage());
        } finally {
            StreamLogger sl = new StreamLogger(process.getInputStream(), logToStdOut);
            sl.start();

            try {
                int exitCode = process.waitFor();
                ret.getValue().put("exitCode", new AussomInt(exitCode));
                if (exitCode != 0)
                    ret.getValue().put("success", new AussomBool(false));
            } catch (InterruptedException e) {
                console.get().err(e.getMessage());
                ret.getValue().put("success", new AussomBool(false));
            }

            ret.getValue().put("result", new AussomString(sl.getResult()));
        }

        return ret;
    }

    public AussomType execRaw(Environment env, ArrayList<AussomType> args) {
        AussomMap ret = new AussomMap();
        ret.getValue().put("success", new AussomBool(true));
        ret.getValue().put("result", new AussomString(""));


        AussomList al = (AussomList) args.get(0);
        List<String> cmdParts = new ArrayList<>();
        for (AussomType at : al.getValue()) {
            cmdParts.add(at.getValueString());
        }
        boolean logToStdOut = ((AussomBool) args.get(1)).getValue();

        Process process = null;
        try {
            process = Runtime.getRuntime().exec(cmdParts.toArray(new String[cmdParts.size()]));
        } catch (IOException e) {
            console.get().err("IOException: " + e.getMessage());
        } finally {
            StreamLogger sl = new StreamLogger(process.getInputStream(), logToStdOut);
            sl.start();

            try {
                int exitCode = process.waitFor();
                ret.getValue().put("exitCode", new AussomInt(exitCode));
                if (exitCode != 0)
                    ret.getValue().put("success", new AussomBool(false));
            } catch (InterruptedException e) {
                console.get().err(e.getMessage());
                ret.getValue().put("success", new AussomBool(false));
            }

            ret.getValue().put("result", new AussomString(sl.getResult()));
        }

        return ret;
    }
}
