/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;

import com.aussom.Environment;
import com.aussom.ast.aussomException;
import com.aussom.types.*;
import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;

public class Screen extends TerminalScreen {
	private TextGraphics text;
	
	public Screen () throws IOException {
		super(new DefaultTerminalFactory(System.out, System.in, Charset.forName("UTF8")).createTerminal());
		this.text = this.newTextGraphics();
	}
	
	public AussomType start(Environment env, ArrayList<AussomType> args) throws IOException {
		this.startScreen();
		return new AussomNull();
	}
	
	public AussomType stop(Environment env, ArrayList<AussomType> args) throws IOException {
		this.stopScreen();
		return new AussomNull();
	}
	
	public AussomType setColor(Environment env, ArrayList<AussomType> args) throws IOException {
		String str = ((AussomString)args.get(0)).getValue();
		TextColor tc = Curses.get().getTextColor(str);
		this.text.setForegroundColor(tc);
		return new AussomNull();
	}
	
	public AussomType setBgColor(Environment env, ArrayList<AussomType> args) throws IOException {
		String str = ((AussomString)args.get(0)).getValue();
		TextColor tc = Curses.get().getTextColor(str);
		this.text.setBackgroundColor(tc);
		return new AussomNull();
	}
	
	public AussomType putString(Environment env, ArrayList<AussomType> args) throws IOException {
		int col = (int) ((AussomInt)args.get(0)).getValue();
		int row = (int) ((AussomInt)args.get(1)).getValue();
		String str = ((AussomString)args.get(2)).getValue();
		this.text.putString(col, row, str);
		return new AussomNull();
	}
	
	public AussomType clear(Environment env, ArrayList<AussomType> args) throws IOException {
		this.clear();
		return new AussomNull();
	}
	
	public AussomType refresh(Environment env, ArrayList<AussomType> args) throws IOException {
		this.refresh();
		return new AussomNull();
	}

	public AussomType getKey(Environment env, ArrayList<AussomType> args) throws IOException, aussomException {
		KeyStroke keyStroke = this.readInput();
		AussomObject cobj = env.getEngine().instantiateObject("keyStroke");
		CursesKeyStroke keyStrokeObj = (CursesKeyStroke) cobj.getExternObject();
		keyStrokeObj.init(keyStroke);
		return cobj;
	}
	
	public AussomType pollKey(Environment env, ArrayList<AussomType> args) throws IOException, aussomException {
		KeyStroke keyStroke = this.pollInput();
		AussomObject cobj = env.getEngine().instantiateObject("keyStroke");
		CursesKeyStroke keyStrokeObj = (CursesKeyStroke) cobj.getExternObject();
		keyStrokeObj.init(keyStroke);
		return cobj;
	}
	
	public AussomType doResizeIfNecessary(Environment env, ArrayList<AussomType> args) throws IOException {
		TerminalSize newSize = this.doResizeIfNecessary();
		if (newSize != null) {
			AussomMap cm = new AussomMap();
			cm.addMember("rows", new AussomInt(newSize.getRows()));
			cm.addMember("cols", new AussomInt(newSize.getColumns()));
			return cm;
		} else {
			return new AussomNull();
		}
	}
	
	public AussomType setPos(Environment env, ArrayList<AussomType> args) throws IOException {
		int col = (int) ((AussomInt)args.get(0)).getValue();
		int row = (int) ((AussomInt)args.get(1)).getValue();
		TerminalPosition tp = new TerminalPosition(col, row);
		this.setCursorPosition(tp);
		return new AussomNull();
	}
}
