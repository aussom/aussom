/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;

import com.aussom.Environment;
import com.aussom.ast.aussomException;
import com.aussom.types.*;
import com.googlecode.lanterna.SGR;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;

public class Curses {
	/**
	 * The single Lang instance.
	 */
	private static Curses instance = null;
	
	private Terminal term;
	private TextGraphics text;
	
	/**
	 * Default constructor set to private to defeat instantiation. See get to get an 
	 * instance of the object.
	 * @throws IOException 
	 */
	public Curses() throws IOException {
		this.init();
	}
	
	public void init() throws IOException {
		
	}
	
	public static Curses get() throws IOException {
		if(instance == null) instance = new Curses();
		return instance;
	}
	
	public AussomType start(Environment env, ArrayList<AussomType> args) throws IOException {
		this.term = new DefaultTerminalFactory(System.out, System.in, Charset.forName("UTF8")).createTerminal();
		this.text = this.term.newTextGraphics();
		return new AussomNull();
	}

	public AussomType enterPrivateMode(Environment env, ArrayList<AussomType> args) throws IOException {
		this.term.enterPrivateMode();
		return new AussomNull();
	}

	public AussomType exitPrivateMode(Environment env, ArrayList<AussomType> args) throws IOException {
		this.term.exitPrivateMode();
		return new AussomNull();
	}
	
	public AussomType stop(Environment env, ArrayList<AussomType> args) throws IOException {
		this.term.exitPrivateMode();
		return new AussomNull();
	}
	
	public AussomType setPos(Environment env, ArrayList<AussomType> args) throws IOException {
		int col = (int) ((AussomInt)args.get(0)).getValue();
		int row = (int) ((AussomInt)args.get(1)).getValue();
		this.term.setCursorPosition(col, row);
		return new AussomNull();
	}
	
	public AussomType putChar(Environment env, ArrayList<AussomType> args) throws IOException {
		String str = ((AussomString)args.get(0)).getValue();
		if (str.length() > 0) {
			char ch = str.charAt(0);
			this.term.putCharacter(ch);
		}
		return new AussomNull();
	}
	
	public AussomType getCols(Environment env, ArrayList<AussomType> args) throws IOException {
		TerminalSize screenSize = this.term.getTerminalSize();
		return new AussomInt(screenSize.getColumns());
	}
	
	public AussomType getRows(Environment env, ArrayList<AussomType> args) throws IOException {
		TerminalSize screenSize = this.term.getTerminalSize();
		return new AussomInt(screenSize.getRows());
	}
	
	public AussomType flush(Environment env, ArrayList<AussomType> args) throws IOException {
		this.term.flush();
		return new AussomNull();
	}
	
	public AussomType putString(Environment env, ArrayList<AussomType> args) throws IOException {
		int col = (int) ((AussomInt)args.get(0)).getValue();
		int row = (int) ((AussomInt)args.get(1)).getValue();
		String str = ((AussomString)args.get(2)).getValue();
		this.text.putString(col, row, str);
		return new AussomNull();
	}
	
	public AussomType putLine(Environment env, ArrayList<AussomType> args) throws IOException {
		int x1 = (int) ((AussomInt)args.get(0)).getValue();
		int y1 = (int) ((AussomInt)args.get(1)).getValue();
		int x2 = (int) ((AussomInt)args.get(2)).getValue();
		int y2 = (int) ((AussomInt)args.get(3)).getValue();
		String str = ((AussomString)args.get(4)).getValue();
		if (str.length() > 0) {
			char ch = str.charAt(0);
			this.text.drawLine(x1, y1, x2, y2, ch);
		}
		return new AussomNull();
	}
	
	public AussomType setColor(Environment env, ArrayList<AussomType> args) throws IOException {
		String str = ((AussomString)args.get(0)).getValue();
		TextColor tc = this.getTextColor(str);
		this.text.setForegroundColor(tc);
		return new AussomNull();
	}
	
	public AussomType setBgColor(Environment env, ArrayList<AussomType> args) throws IOException {
		String str = ((AussomString)args.get(0)).getValue();
		TextColor tc = this.getTextColor(str);
		this.text.setBackgroundColor(tc);
		return new AussomNull();
	}
	
	public AussomType enableStyle(Environment env, ArrayList<AussomType> args) throws IOException {
		String str = ((AussomString)args.get(0)).getValue();
		SGR sgr = this.getStyle(str);
		this.term.enableSGR(sgr);
		return new AussomNull();
	}
	
	public AussomType disableStyle(Environment env, ArrayList<AussomType> args) throws IOException {
		String str = ((AussomString)args.get(0)).getValue();
		SGR sgr = this.getStyle(str);
		this.term.enableSGR(sgr);
		return new AussomNull();
	}

	public AussomType resetStyle(Environment env, ArrayList<AussomType> args) throws IOException {
		this.term.resetColorAndSGR();
		return new AussomNull();
	}
	
	public AussomType clearScreen(Environment env, ArrayList<AussomType> args) throws IOException {
		this.term.clearScreen();
		return new AussomNull();
	}

	public AussomType getKey(Environment env, ArrayList<AussomType> args) throws IOException, aussomException {
		KeyStroke keyStroke = this.term.readInput();
		AussomObject cobj = env.getEngine().instantiateObject("keyStroke");
		CursesKeyStroke keyStrokeObj = (CursesKeyStroke) cobj.getExternObject();
		keyStrokeObj.init(keyStroke);
		return cobj;
	}
	
	public AussomType pollKey(Environment env, ArrayList<AussomType> args) throws IOException, aussomException {
		KeyStroke keyStroke = this.term.pollInput();
		AussomObject cobj = env.getEngine().instantiateObject("keyStroke");
		CursesKeyStroke keyStrokeObj = (CursesKeyStroke) cobj.getExternObject();
		keyStrokeObj.init(keyStroke);
		return cobj;
	}
	
	public TextColor getTextColor(String str) {
		TextColor tc = TextColor.ANSI.DEFAULT;
		switch(str.trim().toLowerCase()) {
		case "black":
			tc = TextColor.ANSI.BLACK;
			break;
		case "red":
			tc = TextColor.ANSI.RED;
			break;
		case "green":
			tc = TextColor.ANSI.GREEN;
			break;
		case "yellow":
			tc = TextColor.ANSI.YELLOW;
			break;
		case "blue":
			tc = TextColor.ANSI.BLUE;
			break;
		case "magenta":
			tc = TextColor.ANSI.MAGENTA;
			break;
		case "cyan":
			tc = TextColor.ANSI.CYAN;
			break;
		case "white":
			tc = TextColor.ANSI.WHITE;
			break;
		case "def":
			tc = TextColor.ANSI.DEFAULT;
			break;
		}
		return tc;
	}
	
	public SGR getStyle(String str) {
		SGR sgr = null;
		switch(str.trim().toLowerCase()) {
		case "bold":
			sgr = SGR.BOLD;
			break;
		case "reverse":
			sgr = SGR.REVERSE;
			break;
		case "underline":
			sgr = SGR.UNDERLINE;
			break;
		case "blink":
			sgr = SGR.BLINK;
			break;
		case "bordered":
			sgr = SGR.BORDERED;
			break;
		case "franktur":
			sgr = SGR.FRAKTUR;
			break;
		case "crossed_out":
			sgr = SGR.CROSSED_OUT;
			break;
		case "circled":
			sgr = SGR.CIRCLED;
			break;
		case "italic":
			sgr = SGR.ITALIC;
			break;
		}
		return sgr;
	}
}
