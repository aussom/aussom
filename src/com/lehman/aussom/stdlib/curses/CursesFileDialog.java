/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.types.*;
import com.googlecode.lanterna.gui2.dialogs.FileDialogBuilder;

import java.io.File;
import java.util.ArrayList;

public class CursesFileDialog {
    public static AussomType show(Environment env, ArrayList<AussomType> args) {
        Gui guiObj = (Gui)((AussomObject)args.get(0)).getExternObject();
        String title = ((AussomString)args.get(1)).getValue();
        String description = ((AussomString)args.get(2)).getValue();
        String actionLabel = ((AussomString)args.get(3)).getValue();

		File ret = new FileDialogBuilder()
            .setTitle(title)
            .setDescription(description)
            .setActionLabel(actionLabel)
            .build()
            .showDialog(guiObj.getDwm());

		return new AussomString(ret.getAbsolutePath());
	}
}
