/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.types.*;
import com.googlecode.lanterna.gui2.Button;

import java.util.ArrayList;

public class CursesButton extends CursesComponent implements CursesComponentInt {
    private Button button = new Button("");

    public CursesButton() {
        this.component = button;
    }

    public AussomType newButton(Environment env, ArrayList<AussomType> args) {
        String text = ((AussomString)args.get(0)).getValue();
        AussomCallback cb = (AussomCallback) args.get(1);
		this.button = new Button(text, new Runnable() {
            @Override
            public void run() {
                cb.call(env, new AussomList());
            }
        });
		this.component = this.button;
		return new AussomNull();
	}

    public AussomType setText(Environment env, ArrayList<AussomType> args) {
        String text = ((AussomString)args.get(0)).getValue();
		this.button.setLabel(text);
		return new AussomNull();
	}

	public AussomType getText(Environment env, ArrayList<AussomType> args) {
        return new AussomString(this.button.getLabel());
    }
}
