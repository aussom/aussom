/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.types.*;
import com.googlecode.lanterna.gui2.ActionListBox;
import java.util.ArrayList;

public class CursesActionBox extends CursesComponent implements CursesComponentInt {
    private ActionListBox actionBox = new ActionListBox();

    public CursesActionBox() {
        this.component = this.actionBox;
    }

    public AussomType setItems(Environment env, ArrayList<AussomType> args) {
        AussomList items = (AussomList) args.get(0);
        this.actionBox.clearItems();
		for (AussomType ct : items.getValue()) {
		    AussomList cl = (AussomList)ct;
		    String val = cl.getValue().get(0).getValueString();
		    AussomCallback cb = (AussomCallback) cl.getValue().get(1);
		    this.actionBox.addItem(val, new Runnable() {
                @Override
                public void run() {
                    AussomList args = new AussomList();
                    args.add(new AussomString(val));
                    cb.call(env, args);
                }
            });
        }
		return new AussomNull();
	}

    public AussomType addItem(Environment env, ArrayList<AussomType> args) {
        AussomList cl = (AussomList)args.get(0);
        String val = cl.getValue().get(0).getValueString();
        AussomCallback cb = (AussomCallback) cl.getValue().get(1);
        this.actionBox.addItem(val, new Runnable() {
            @Override
            public void run() {
                AussomList args = new AussomList();
                args.add(new AussomString(val));
                cb.call(env, args);
            }
        });
		return new AussomNull();
	}
}
