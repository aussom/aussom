/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.types.*;
import com.googlecode.lanterna.gui2.CheckBoxList;

import java.util.ArrayList;
import java.util.List;

public class CursesCheckBox extends CursesComponent implements CursesComponentInt {
    private CheckBoxList checkBox = new CheckBoxList();

    public CursesCheckBox() {
        this.component = this.checkBox;
    }

    public AussomType setItems(Environment env, ArrayList<AussomType> args) {
        AussomList items = (AussomList) args.get(0);
        this.checkBox.clearItems();
		for (AussomType ct : items.getValue()) {
		    this.checkBox.addItem(ct.getValueString());
        }
		return new AussomNull();
	}

    public AussomType addItem(Environment env, ArrayList<AussomType> args) {
        String item = ((AussomString)args.get(0)).getValue();
		this.checkBox.addItem(item);
		return new AussomNull();
	}

	public AussomType clear(Environment env, ArrayList<AussomType> args) {
        this.checkBox.clearItems();
		return new AussomNull();
	}

	public AussomType getCheckedItems(Environment env, ArrayList<AussomType> args) {
        List<String> checkedItems = this.checkBox.getCheckedItems();
        AussomList items = new AussomList();
        for(String item: checkedItems) {
            items.add(new AussomString(item));
        }
		return items;
	}

	public AussomType size(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.checkBox.getItemCount());
	}

	public AussomType isChecked(Environment env, ArrayList<AussomType> args) {
        int index = (int) ((AussomInt)args.get(0)).getValue();
		return new AussomBool(this.checkBox.isChecked(index));
	}

	public AussomType remove(Environment env, ArrayList<AussomType> args) {
        int index = (int) ((AussomInt)args.get(0)).getValue();
        this.checkBox.removeItem(index);
		return new AussomNull();
	}
}
