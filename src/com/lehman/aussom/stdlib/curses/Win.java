/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.aussom.Environment;
import com.aussom.types.*;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Window;

public class Win extends BasicWindow {
	public AussomType setHints(Environment env, ArrayList<AussomType> args) throws IOException {
		List<Window.Hint> hints = new ArrayList<Hint>();
		AussomList hintList = (AussomList)args.get(0);
		for (AussomType item : hintList.getValue()) {
			String val = item.toString().toLowerCase().trim();
			if (val.equals("fixed_size"))
				hints.add(Hint.FIXED_SIZE);
			else if (val.equals("centered"))
				hints.add(Hint.CENTERED);
			else if (val.equals("expanded"))
				hints.add(Hint.EXPANDED);
			else if (val.equals("fit_terminal_window"))
				hints.add(Hint.FIT_TERMINAL_WINDOW);
			else if (val.equals("fixed_position"))
				hints.add(Hint.FIXED_POSITION);
			else if (val.equals("full_screen"))
				hints.add(Hint.FULL_SCREEN);
			else if (val.equals("menu_popup"))
				hints.add(Hint.MENU_POPUP);
			else if (val.equals("modal"))
				hints.add(Hint.MODAL);
			else if (val.equals("no_decorations"))
				hints.add(Hint.NO_DECORATIONS);
			else if (val.equals("no_focus"))
				hints.add(Hint.NO_FOCUS);
			else if (val.equals("no_post_rendering"))
				hints.add(Hint.NO_POST_RENDERING);
			else
				return new AussomException("Error: Win.setHints(), unknown value '" + val + "' provided.");
		}
		this.setHints(hints);
		return new AussomNull();
	}

	public AussomType set(Environment env, ArrayList<AussomType> args) throws IOException {
		AussomObject co = (AussomObject)args.get(0);
		this.setComponent(((CursesComponentInt)co.getExternObject()).getComponent());
		return new AussomNull();
	}

	public AussomType setMenuBar(Environment env, ArrayList<AussomType> args) throws IOException {
        AussomObject mbarObj = (AussomObject) args.get(0);
        if (mbarObj.getExternObject() instanceof CursesMenuBar) {
            CursesMenuBar menuBar = (CursesMenuBar)mbarObj.getExternObject();
            this.setMenuBar(menuBar.getMenuBar());
            return new AussomNull();
        } else {
            return new AussomException("win expecting object of type 'menubar'.");
        }
	}
}
