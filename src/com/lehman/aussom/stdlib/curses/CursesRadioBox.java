/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.types.*;
import com.googlecode.lanterna.gui2.RadioBoxList;

import java.util.ArrayList;

public class CursesRadioBox extends CursesComponent implements CursesComponentInt {
    private RadioBoxList radioBox = new RadioBoxList();

    public CursesRadioBox() {
        this.component = this.radioBox;
    }

    public AussomType setItems(Environment env, ArrayList<AussomType> args) {
        AussomList items = (AussomList) args.get(0);
        this.radioBox.clearItems();
		for (AussomType ct : items.getValue()) {
		    this.radioBox.addItem(ct.getValueString());
        }
		return new AussomNull();
	}

    public AussomType addItem(Environment env, ArrayList<AussomType> args) {
        String item = ((AussomString)args.get(0)).getValue();
		this.radioBox.addItem(item);
		return new AussomNull();
	}

	public AussomType clear(Environment env, ArrayList<AussomType> args) {
		this.radioBox.clearItems();
		return new AussomNull();
	}

	public AussomType clearSelected(Environment env, ArrayList<AussomType> args) {
		this.radioBox.clearSelection();
		return new AussomNull();
	}

	public AussomType getSelected(Environment env, ArrayList<AussomType> args) {
		String selected = (String) this.radioBox.getSelectedItem();
		return new AussomString(selected);
	}

	public AussomType getSelectedIndex(Environment env, ArrayList<AussomType> args) {
		int index = this.radioBox.getSelectedIndex();
		return new AussomInt(index);
	}

	public AussomType isChecked(Environment env, ArrayList<AussomType> args) {
        int index = (int) ((AussomInt)args.get(0)).getValue();
		return new AussomBool(this.radioBox.isChecked(index));
	}

	public AussomType remove(Environment env, ArrayList<AussomType> args) {
        int index = (int) ((AussomInt)args.get(0)).getValue();
        this.radioBox.removeItem(index);
		return new AussomNull();
	}

	public AussomType size(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.radioBox.getItemCount());
	}
}
