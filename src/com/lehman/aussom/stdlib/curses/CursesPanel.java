/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.types.AussomNull;
import com.aussom.types.AussomObject;
import com.aussom.types.AussomType;
import com.googlecode.lanterna.gui2.AbsoluteLayout;
import com.googlecode.lanterna.gui2.Panel;

import java.util.ArrayList;

public class CursesPanel extends CursesComponent implements CursesComponentInt {
	private Panel panel = new Panel();

	public CursesPanel() {
		this.component = panel;
		this.panel.setLayoutManager(new AbsoluteLayout());
	}

	public AussomType setLayout(Environment env, ArrayList<AussomType> args) {
		AussomObject co = (AussomObject)args.get(0);
		this.panel.setLayoutManager(((CursesLayoutInt)co.getExternObject()).getLayout());
		return new AussomNull();
	}

    public AussomType add(Environment env, ArrayList<AussomType> args) {
		AussomObject co = (AussomObject)args.get(0);
		this.panel.addComponent(((CursesComponentInt)co.getExternObject()).getComponent());
		return new AussomNull();
	}
}
