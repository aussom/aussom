/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import java.io.IOException;
import java.util.ArrayList;

import com.aussom.Environment;
import com.aussom.types.AussomNull;
import com.aussom.types.AussomObject;
import com.aussom.types.AussomString;
import com.aussom.types.AussomType;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.gui2.DefaultWindowManager;
import com.googlecode.lanterna.gui2.EmptySpace;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.Window;

public class Gui {
	private MultiWindowTextGUI dwm;
	public Gui () { }
	
	public AussomType newGui(Environment env, ArrayList<AussomType> args) throws IOException {
		AussomObject scrn = (AussomObject)args.get(0);
		String bgstr = ((AussomString)args.get(1)).getValue();
		TextColor tc = Curses.get().getTextColor(bgstr);
		this.dwm = new MultiWindowTextGUI((Screen)scrn.getExternObject(), new DefaultWindowManager(), new EmptySpace(tc));
		return new AussomNull();
	}
	
	public AussomType start(Environment env, ArrayList<AussomType> args) throws IOException {
		this.dwm.getScreen().startScreen();
		return new AussomNull();
	}
	
	public AussomType stop(Environment env, ArrayList<AussomType> args) throws IOException {
		for (Window w : this.dwm.getWindows()) {
			w.close();
		}
		this.dwm.getScreen().stopScreen();
		return new AussomNull();
	}
	
	public AussomType addWindowAndWait(Environment env, ArrayList<AussomType> args) throws IOException {
		AussomObject co = (AussomObject)args.get(0);
		this.dwm.addWindowAndWait((Window)co.getExternObject());
		return new AussomNull();
	}

	public MultiWindowTextGUI getDwm() {
		return dwm;
	}
}
