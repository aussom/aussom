/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.types.*;
import com.googlecode.lanterna.gui2.table.Table;

import java.util.ArrayList;
import java.util.List;

public class CursesTable extends CursesComponent implements CursesComponentInt {
    private Table<String> table = new Table<String>("");

    public CursesTable() {
        this.component = this.table;
    }

    public AussomType setCols(Environment env, ArrayList<AussomType> args) {
        AussomList colList = (AussomList)args.get(0);
        String[] cols = new String[colList.size()];
        for(int i = 0; i < colList.size(); i++) {
            AussomString col = (AussomString) colList.getValue().get(i);
            cols[i] = col.getValueString();
        }
		this.table = new Table<String>(cols);
        this.component = this.table;
		return new AussomNull();
	}

    public AussomType addCol(Environment env, ArrayList<AussomType> args) {
        String colName = ((AussomString)args.get(0)).getValue();
		this.table.getTableModel().addColumn(colName, new String[0]);
		return new AussomNull();
	}

	public AussomType addRow(Environment env, ArrayList<AussomType> args) {
        AussomList valList = (AussomList)args.get(0);
        String[] rows = new String[valList.size()];
        for (int i = 0; i < valList.size(); i++) {
            String val = valList.getValue().get(i).getValueString();
            rows[i] = val;
        }
		this.table.getTableModel().addRow(rows);
		return new AussomNull();
	}

	public AussomType setAction(Environment env, ArrayList<AussomType> args) {
        if (args.get(0) != null) {
            AussomCallback cb = (AussomCallback) args.get(0);
            this.table.setSelectAction(new Runnable() {
                @Override
                public void run() {
                    AussomList args = new AussomList();
                    List<String> data = table.getTableModel().getRow(table.getSelectedRow());
                    AussomList row = new AussomList();
                    for (String val : data) {
                        row.add(new AussomString(val));
                    }
                    args.add(row);
                    cb.call(env, args);
                }
            });
        } else {
            this.table.setSelectAction(null);
        }
		return new AussomNull();
	}

	public AussomType setVisibleCols(Environment env, ArrayList<AussomType> args) {
        int cols = (int) ((AussomInt)args.get(0)).getValue();
		this.table.setVisibleColumns(cols);
		return new AussomNull();
	}

	public AussomType setVisibleRows(Environment env, ArrayList<AussomType> args) {
        int rows = (int) ((AussomInt)args.get(0)).getValue();
		this.table.setVisibleRows(rows);
		return new AussomNull();
	}

	public AussomType setViewTopRow(Environment env, ArrayList<AussomType> args) {
        int row = (int) ((AussomInt)args.get(0)).getValue();
		this.table.setViewTopRow(row);
		return new AussomNull();
	}
}
