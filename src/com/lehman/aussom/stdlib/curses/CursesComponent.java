/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.ast.aussomException;
import com.aussom.types.AussomInt;
import com.aussom.types.AussomNull;
import com.aussom.types.AussomString;
import com.aussom.types.AussomType;
import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.AbstractComponent;
import com.googlecode.lanterna.gui2.BorderLayout;
import com.googlecode.lanterna.gui2.LayoutData;

import java.util.ArrayList;

public class CursesComponent {
    protected AbstractComponent component;

    public AussomType setPos(Environment env, ArrayList<AussomType> args) throws aussomException {
        Long col = ((AussomInt)args.get(0)).getNumericInt();
        Long row = ((AussomInt)args.get(1)).getNumericInt();
		this.component.setPosition(new TerminalPosition(col.intValue(), row.intValue()));
		return new AussomNull();
	}

	public AussomType setSize(Environment env, ArrayList<AussomType> args) throws aussomException {
        Long cols = ((AussomInt)args.get(0)).getNumericInt();
        Long rows = ((AussomInt)args.get(1)).getNumericInt();
		this.component.setSize(new TerminalSize(cols.intValue(), rows.intValue()));
		return new AussomNull();
	}

	public AussomType setPrefSize(Environment env, ArrayList<AussomType> args) throws aussomException {
        Long cols = ((AussomInt)args.get(0)).getNumericInt();
        Long rows = ((AussomInt)args.get(1)).getNumericInt();
		this.component.setPreferredSize(new TerminalSize(cols.intValue(), rows.intValue()));
		return new AussomNull();
	}

	public AussomType setLayoutData(Environment env, ArrayList<AussomType> args) {
        String val = ((AussomString)args.get(0)).getValueString();

		LayoutData data = null;
		if (val.toLowerCase().equals("center")) {
		    data = BorderLayout.Location.CENTER;
        } else if (val.toLowerCase().equals("top")) {
		    data = BorderLayout.Location.TOP;
        } else if (val.toLowerCase().equals("bottom")) {
		    data = BorderLayout.Location.BOTTOM;
        } else if (val.toLowerCase().equals("left")) {
		    data = BorderLayout.Location.LEFT;
        } else if (val.toLowerCase().equals("right")) {
		    data = BorderLayout.Location.RIGHT;
        }

		if (data != null) {
		    this.component.setLayoutData(data);
        }

		return new AussomNull();
	}

    public AbstractComponent getComponent() {
        return this.component;
    }
}
