/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.types.*;
import com.googlecode.lanterna.gui2.menu.Menu;
import com.googlecode.lanterna.gui2.menu.MenuItem;

import java.util.ArrayList;

public class CursesMenu {
    private Menu menu = new Menu("xxxxxx");

    public AussomType newMenu(Environment env, ArrayList<AussomType> args) {
        String label = ((AussomString) args.get(0)).getValueString();
        this.menu = new Menu(label);
        return new AussomNull();
	}

    public AussomType addItem(Environment env, ArrayList<AussomType> args) {
        String text = ((AussomString)args.get(0)).getValue();
        AussomCallback cb = (AussomCallback) args.get(1);
		MenuItem item = new MenuItem(text, new Runnable() {
            @Override
            public void run() {
                AussomList listArgs = new AussomList();
                listArgs.add(new AussomString(text));
                cb.call(env, listArgs);
            }
        });
		this.menu.add(item);
		return new AussomNull();
	}

    public Menu getMenu() {
        return menu;
    }
}
