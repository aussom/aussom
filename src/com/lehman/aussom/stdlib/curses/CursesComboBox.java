/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.types.*;
import com.googlecode.lanterna.gui2.ComboBox;

import java.util.ArrayList;

public class CursesComboBox extends CursesComponent implements CursesComponentInt {
    private ComboBox comboBox = new ComboBox();

    public CursesComboBox() {
        this.component = this.comboBox;
    }

    public AussomType setItems(Environment env, ArrayList<AussomType> args) {
        AussomList items = (AussomList) args.get(0);
        this.comboBox.clearItems();
		for (AussomType ct : items.getValue()) {
		    this.comboBox.addItem(ct.getValueString());
        }
		return new AussomNull();
	}

	public AussomType addItem(Environment env, ArrayList<AussomType> args) {
        String item = ((AussomString)args.get(0)).getValue();
		this.comboBox.addItem(item);
		return new AussomNull();
	}

	public AussomType clear(Environment env, ArrayList<AussomType> args) {
        this.comboBox.clearItems();
		return new AussomNull();
	}

	public AussomType size(Environment env, ArrayList<AussomType> args) {
        return new AussomInt(this.comboBox.getItemCount());
	}

    public AussomType getItemAt(Environment env, ArrayList<AussomType> args) {
        Long index = ((AussomInt)args.get(0)).getValue();
        String item = (String) this.comboBox.getItem(index.intValue());
		return new AussomString(item);
	}

	public AussomType getSelectedIndex(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.comboBox.getSelectedIndex());
	}

	public AussomType getSelectedItem(Environment env, ArrayList<AussomType> args) {
        return new AussomString((String) this.comboBox.getSelectedItem());
	}

	public AussomType getText(Environment env, ArrayList<AussomType> args) {
        return new AussomString(this.comboBox.getText());
	}
}
