/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.types.*;
import com.googlecode.lanterna.gui2.menu.MenuBar;

import java.util.ArrayList;

public class CursesMenuBar {
    private MenuBar menuBar = new MenuBar();

    public AussomType add(Environment env, ArrayList<AussomType> args) {
        AussomObject menuObj = (AussomObject) args.get(0);
        if (menuObj.getExternObject() instanceof CursesMenu) {
            CursesMenu menu = (CursesMenu)menuObj.getExternObject();
            this.menuBar.add(menu.getMenu());
            return new AussomNull();
        } else {
            return new AussomException("menubar expecting object of type 'menu'.");
        }
	}

    public MenuBar getMenuBar() {
        return menuBar;
    }
}
