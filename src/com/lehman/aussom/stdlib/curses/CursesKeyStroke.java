/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.types.*;
import com.googlecode.lanterna.input.KeyStroke;

import java.util.ArrayList;

public class CursesKeyStroke {
    private KeyStroke key = null;

    public CursesKeyStroke() { }

    public void init(KeyStroke Key) {
        this.key = Key;
    }

    public AussomType getChar(Environment env, ArrayList<AussomType> args) {
		return new AussomString(this.key.getCharacter().toString());
	}

	public AussomType getEventTime(Environment env, ArrayList<AussomType> args) {
		return new AussomInt(this.key.getEventTime());
	}

	public AussomType getKeyType(Environment env, ArrayList<AussomType> args) {
		return new AussomString(this.key.getKeyType().toString().toLowerCase());
	}

	public AussomType isAltDown(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(this.key.isAltDown());
	}

	public AussomType isCtrlDown(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(this.key.isCtrlDown());
	}

	public AussomType isShiftDown(Environment env, ArrayList<AussomType> args) {
		return new AussomBool(this.key.isShiftDown());
	}

	public AussomType toString(Environment env, ArrayList<AussomType> args) {
		return new AussomString(this.key.toString());
	}
}
