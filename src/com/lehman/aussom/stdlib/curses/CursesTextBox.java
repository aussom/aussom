/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom.stdlib.curses;

import com.aussom.Environment;
import com.aussom.ast.aussomException;
import com.aussom.types.AussomBool;
import com.aussom.types.AussomNull;
import com.aussom.types.AussomString;
import com.aussom.types.AussomType;
import com.googlecode.lanterna.gui2.TextBox;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class CursesTextBox extends CursesComponent  implements CursesComponentInt {
    private TextBox textBox = new TextBox("");

    public CursesTextBox() {
        this.component = this.textBox;
    }

    public AussomType newTextBox(Environment env, ArrayList<AussomType> args) throws aussomException {
        Boolean multiline = ((AussomBool)args.get(0)).getNumericBool();
		if (multiline) {
		    this.textBox = new TextBox("", TextBox.Style.MULTI_LINE);
        } else {
		    this.textBox = new TextBox("", TextBox.Style.SINGLE_LINE);
        }
		this.component = this.textBox;
		return new AussomNull();
	}

    public AussomType setText(Environment env, ArrayList<AussomType> args) {
        String text = ((AussomString)args.get(0)).getValue();
		this.textBox.setText(text);
		return new AussomNull();
	}

	public AussomType setValidation(Environment env, ArrayList<AussomType> args) {
        String validationPattern = ((AussomString)args.get(0)).getValue();
		this.textBox.setValidationPattern(Pattern.compile(validationPattern));
		return new AussomNull();
	}

    public AussomType setReadOnly(Environment env, ArrayList<AussomType> args) {
        Boolean readOnly = ((AussomBool)args.get(0)).getValue();
		this.textBox.setReadOnly(readOnly);
		return new AussomNull();
	}

	public AussomType getText(Environment env, ArrayList<AussomType> args) {
		return new AussomString(this.textBox.getText());
	}
}
