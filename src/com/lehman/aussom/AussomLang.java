/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom;

import java.net.URISyntaxException;
import com.aussom.Engine;
import com.aussom.LoggingInt;
import com.aussom.SecurityManagerImpl;
import com.aussom.Util;
import com.aussom.stdlib.Lang;
import com.aussom.stdlib.console;

public class AussomLang {
	/**
	 * The single Lang instance.
	 */
	private static AussomLang instance = null;

	private SecurityManagerImpl secman;
	
	/**
	 * Default constructor set to private to defeat instantiation. See get to get an 
	 * instance of the object.
	 */
	private AussomLang() {
		this.init();
	}
	
	private void init() {
		// Setup security manager here.
		this.secman = new AussomLangSecurityManager();

		Lang.get().getLangIncludes().put("file.aus", Util.loadResource("/com/lehman/aussom/stdlib/aus/file.aus"));
	}
	
	/**
	 * Gets a handle of the Universe object.
	 * @return The instance of the AussomLang object.
	 */
	public static AussomLang get() {
		if(instance == null) instance = new AussomLang();
		return instance;
	}
	
	public void runScript(String ScriptFile) {
		try {
			Engine eng = new Engine(this.secman);
			// Add local modules path to includes.
			eng.addIncludePath("modules");
			eng.loadUniverseClasses();
			if (isRunningJar()) {
				// Add packaged ca files.
				eng.addResourceIncludePath("/com/lehman/aussom/stdlib/aus");
			} else {
				// Running from the IDE.
				eng.addIncludePath("src/com/lehman/aussom/stdlib/aus");
			}
			// Add includes from the current working directory.
			eng.addIncludePath(".");
			eng.parseFile(ScriptFile);
			eng.run();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isRunningJar() {
		String jarPath = null;
		try {
			 jarPath = AussomLang.class
					.getProtectionDomain()
					.getCodeSource()
					.getLocation()
					.toURI()
					.getPath();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		if (jarPath != null && jarPath.endsWith(".jar")) return true;
		return false;
	}
}
