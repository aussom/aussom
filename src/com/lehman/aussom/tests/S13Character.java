package com.lehman.aussom.tests;

import java.util.ArrayList;
import java.util.List;

public class S13Character {
    public String firstName = "";
    private String lastName = "";
    private String rank = "";
    public String side = "";
    private String org = "";

    public S13Character() { }
    public S13Character(String First, String Last) {
        this.firstName = First;
        this.lastName = Last;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getName() {
        return (this.firstName + " " + this.lastName).trim();
    }
}
