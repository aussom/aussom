package com.lehman.aussom.tests;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class S13TestObj {
    // Default constructor
    public S13TestObj() { }

    public static String MY_KEY_FIELD = "Robert Hogan";

    public static String invokeStaticSimpleString(String Str) {
        return Str;
    }

    /**********************************************************************
     * Simple datatype tests with no coercion.
     **********************************************************************/
    public Boolean simpleBoolean(boolean val) {
        List<String> test = new ArrayList<String>();
        test.add("Hogan");
        Class<?> cls = test.getClass();
        return val;
    }

    public String simpleString(String val) {
        return val;
    }

    public Long simpleLong(long val) {
        return val;
    }

    public Double simpleDouble(double val) {
        return val;
    }

    public List<Object> simpleList(List<Object> val) {
        return val;
    }

    public Map<Object, Object> simpleMap(Map<Object, Object> val) {
        return val;
    }

    public Object simpleGenericObject(Object val) {
        return val;
    }

    public S13Character simpleObject(S13Character val) {
        return val;
    }

    public byte[] simpleByteArray(byte[] val) {
        return val;
    }

    public int coerceInt(int val) {
        return val;
    }

    public Integer coerceIntegerGeneric(Integer val) {
        return val;
    }

    public short coerceShort(short val) {
        return val;
    }

    public Short coerceShortGeneric(Short val) {
        return val;
    }

    public byte coerceByte(byte val) {
        return val;
    }

    public Byte coerceByteGeneric(Byte val) {
        return val;
    }

    public float coerceFloat(float val) {
        return val;
    }

    public Float coerceFloatGeneric(Float val) {
        return val;
    }

    public char coerceChar(char val) {
        return val;
    }

    public char[] coerceCharArray(char[] val) {
        return val;
    }

    public InputStream coerceInputStream(InputStream is) throws IOException {
        byte[] bytes =  is.readAllBytes();
        return new ByteArrayInputStream(bytes);
    }
}
