/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom;


import com.aussom.SecurityManagerImpl;

/**
 * This is the security manager implemented for this standard desktop full
 * version of aussom. In other versions of the interpreter permissions could
 * be more or less depending on need.
 */
public class AussomLangSecurityManager extends SecurityManagerImpl {
    public AussomLangSecurityManager() {
        /*
         * Security manager itself.
         */
        // instantiate - can new instances be created from this one? This
        // normally applies to ones instantiated from within aussom and are blocked
        // in CSecurityManager sub-class constructor.
        this.props.put("securitymanager.instantiate", true);

        // getProp
        this.props.put("securitymanager.property.get", true);

        // keySet/getMap
        this.props.put("securitymanager.property.list", true);

        // setProp
        this.props.put("securitymanager.property.set", false);

        /**
         * Aussom app properties
         */
        this.props.put("aussom.app.loadjar", true);

        /*
         *  Aussom AJI actions.
         */
        // Can you invoke a static Java function.
        this.props.put("aussom.aji.static.invoke", true);

        // Get a static field value.
        this.props.put("aussom.aji.static.getmember", true);

        // Set a static field value.
        this.props.put("aussom.aji.static.setmember", true);

        // Can you create a new aji object (with a Java object)
        this.props.put("aussom.aji.object.create", true);

        // Can you invoke a Java function.
        this.props.put("aussom.aji.invoke", true);

        // Get a field value.
        this.props.put("aussom.aji.getmember", true);

        // Set a field value.
        this.props.put("aussom.aji.setmember", true);

        /*
         *  System information view. See com.aussom.stdlib.CSys.java.
         */
        this.props.put("os.info.view", true);
        this.props.put("java.info.view", true);
        this.props.put("java.home.view", true);
        this.props.put("java.classpath.view", true);
        this.props.put("aussom.info.view", true);
        this.props.put("aussom.path.view", true);
        this.props.put("current.path.view", true);
        this.props.put("home.path.view", true);
        this.props.put("user.name.view", true);

        /*
         *  Reflection actions. See com.aussom.stdlib.CReflect.java.
         */
        this.props.put("reflect.eval.string", true);
        this.props.put("reflect.eval.file", true);
        this.props.put("reflect.include.module", true);

        /*
		 * Aussomdoc actions. See com.aussom.stdlib.ALang.java.
		 */
		this.props.put("aussomdoc.file.getJson", true);
		this.props.put("aussomdoc.class.getJson", true);
    }
}
