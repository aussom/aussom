/*
 * Copyright 2021 Austin Lehman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.lehman.aussom;

import java.io.File;
import java.io.OutputStream;
import java.util.List;
import java.io.PrintStream;

import com.aussom.DefaultLoggingImpl;
import com.aussom.stdlib.ASys;
import com.lehman.aussom.docs.DocWriterInt;
import com.lehman.aussom.docs.MarkdownDocWriterImpl;
import com.aussom.DefaultSecurityManagerImpl;
import com.aussom.Engine;
import com.aussom.Util;
import com.lehman.aussom.stdlib.AussomApp;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.aussom.stdlib.console;

public class Main {
	private static PrintStream origPs;
	private static PrintStream origErrPs;

	private static int logLevel = DefaultLoggingImpl.INFO;

	public static String version = "1.0.2";

	public static void main(String[] args) throws Exception {
		// Redirect all the garbage at the beginning to null.
		redirectStdoutToNull();

		// This is cool, thanks to
		// https://github.com/burningwave/core
		setAllToAllModules();

		// Set default logging.
		setDefaultLogger();

		// Redirect back
		redirectStdoutBack();

		// create the command line parser
		CommandLineParser parser = new DefaultParser();
		
		// create the Options
		Options options = new Options();

		// Version option
		Option version = new Option("v", "version", false, "print the version information" );
		options.addOption(version);

		// Help option
		Option help = new Option("h", "help", false, "print this message" );
		options.addOption(help);

		// Aussomdoc options
		Option doc = new Option("d", "doc", false, "generate aussomdoc for file");
		options.addOption(doc);

		// opendoc options
		Option openDoc = new Option("od", "opendoc", false, "opens the Aussom documentation in the system browser");
		options.addOption(openDoc);

		// arcgen options
		Option arcgen = new Option("ag", "arcgen", false, "runs the Aussom archetype generate program");
		options.addOption(arcgen);

		Option outDir = new Option("o", "outdir", true, "the output directory to place the generated aussomdoc file");
		options.addOption(outDir);

		Option loglevel = new Option("l", "loglevel", true, "set the log level (trace, debug, info, warning, error)");
		options.addOption(loglevel);
		
		try {
			// parse the command line arguments
			CommandLine line = parser.parse(options, args);

			// Check for log level
			if (line.hasOption("loglevel")) {
				String levelStr = line.getOptionValue("loglevel");
				int logLevelVal = getLogLevel(levelStr.trim());
				if (logLevelVal != -1) {
					logLevel = logLevelVal;
				} else {
					console.get().warn("unrecognized loglevel provided '" + levelStr + "'.");
				}
			}

			// has the buildfile argument been passed?
			if(line.hasOption("help")) {
				// automatically generate the help statement
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("aussom [options] <aussom-file>", options);
			} else if (line.hasOption("version")) {
				printInfo();
			} else if (line.hasOption("doc")) {
				if (line.getArgList().size() == 1) {
					String aussomFile = line.getArgList().get(0);
					console.get().info("Now to generate doc for file '" + aussomFile + "'.");
					String outFile = aussomFile + ".md";
					if (line.hasOption("outdir")) {
						File inFile = new File(aussomFile);
						outFile = line.getOptionValue("outdir") + "/" + inFile.getName() + ".md";
					}
					Util.write(outFile, getAussomdocMarkdown(aussomFile), false);
					console.get().info("Wrote doc to '" + outFile + "'.");
				} else if (line.getArgList().size() > 1) {
					console.get().err("Too many arguments provided, expecting just one script file or none at all.\n");
					printHelp(options);
				}
			} else if (line.hasOption("opendoc")){
				AussomApp.get().openWebPage(ASys._getAssemblyPath() + "/docs/html/index.html");
			} else if (line.hasOption("arcgen")){
				arcGen();
			} else {
				List<String> pargs = line.getArgList();
				if (pargs.size() == 0) {
					console.get().err("Expecting aussom script file, but no arguments found.");
					console.get().info("Use -h option for help.");
				} else if (pargs.size() > 1) {
					console.get().err("Too many arguments provided, expecting just one script file.");
					console.get().info("Use -h option for help.");
				} else {
					// We nailed it, let's run the file
					String ScriptFile = pargs.get(0);
					File f = new File(ScriptFile);
					if (f.exists() && f.isFile()) {
						runApp(f.getAbsolutePath());
					} else {
						console.get().err("Provided script file '" + ScriptFile + "' not found.");
					}
				}
			}
		}
		catch( ParseException exp ) {
		    System.out.println("CLI parse exception:" + exp.getMessage());
		}
	}
	
	public static void runApp(String ScriptFile) {
		// Setup logging.
		DefaultLoggingImpl logger = new DefaultLoggingImpl();
		logger.setLevel(logLevel);
		console.get().register(logger);

		// run the file
		AussomLang.get().runScript(ScriptFile);
	}

	public static String getAussomdocMarkdown(String ScriptFile) throws Exception {
		// Create a new Aussom engine.
		Engine eng = new Engine(new DefaultSecurityManagerImpl());
		eng.setLoadExternClasses(false);

		// Add parser resource path for generating docs.
		eng.addResourceIncludePath("/com/lehman/aussom/stdlib/aus/");

		// Parse the provided file name.
		eng.parseFile(ScriptFile);

		// Build the doc and return it.
		DocWriterInt dw = new MarkdownDocWriterImpl();
		return dw.writeDoc(eng, ScriptFile);
	}

	public static void printHelp(Options options) {
		// automatically generate the help statement
		HelpFormatter formatter = new HelpFormatter();
		printInfo();
		formatter.printHelp("aussom [options] <aussom-file>", options);
	}

	public static void arcGen() throws Exception {
		AussomLang.get().runScript(ASys._getAssemblyPath() + "/archetype/gen-module.aus");
	}

	/**
	 * Prints the application info to standard output.
	 */
	public static void printInfo() {
		String rstr = "";
		rstr += "Aussom-Lang Version " + Main.version  + "\n";
		rstr += "Copyright 2024 Austin Lehman\n";
		rstr += "GNU General Public License Version 3\n";
		System.out.println(rstr);
	}

	private static void setDefaultLogger() {
		// Set logging
		DefaultLoggingImpl logger = new DefaultLoggingImpl();
		logger.setLevel(DefaultLoggingImpl.INFO);
		console.get().register(logger);
	}

	public static void redirectStdoutToNull() {
		try {
			origPs = System.out;
			origErrPs = System.err;
			System.setOut(new PrintStream(new OutputStream() {
				public void write(int b) {
					// Do nothing here ...
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void redirectStdoutBack() {
		try {
			System.setOut(origPs);
			System.setErr(origErrPs);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets all to all modules allowed, and does it by redirecting
	 * stdout while doing it to eliminiate the billboarding.
	 */
	private static void setAllToAllModules() {
		// Set the all to all.
		org.burningwave.core.assembler.StaticComponentContainer.Modules.exportAllToAll();
	}

	private static int getLogLevel(String logstr) {
		if (logstr.toLowerCase().equals("trace")) {
			return DefaultLoggingImpl.TRACE;
		} else if (logstr.toLowerCase().equals("debug")) {
			return DefaultLoggingImpl.DEBUG;
		} else if (logstr.toLowerCase().equals("info")) {
			return DefaultLoggingImpl.INFO;
		} else if (logstr.toLowerCase().equals("warning")) {
			return DefaultLoggingImpl.WARNING;
		} else if (logstr.toLowerCase().equals("error")) {
			return DefaultLoggingImpl.ERROR;
		}
		return -1;
	}
}
