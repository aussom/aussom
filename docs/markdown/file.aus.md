# file: file.aus

## class: file

[10:21] `static` (extern: com.lehman.aussom.stdlib.AussomFile) **extends: object** 

Aussom file is a static class with a bunch of functions for
manipulating files. It has some basic read/write files, along with
list, rm, rename, delete, and many others.

#### Methods

- **write** (`string name, string data, bool append = false`)

	> Writes a text file with to the provided file name with the provided data.

	- **@p** `name` is a string with the file name to write.
	- **@p** `data` is a string with the text data to write.
	- **@p** `append` is an optional flag with true to append and false to not.
	- **@r** `A` boolean with true if successful.


- **read** (`string name`)

	> Reads a text file with the provided file name.

	- **@p** `name` is a string with the file name to read.
	- **@r** `A` string with the contents of the file.


- **writeBinary** (`string name, object data`)

	> Writes a binary file with the provided file name and buffer object.

	- **@p** `name` is a string with the file name to write.
	- **@p** `data` is a buffer object with the data to write.
	- **@r** `A` boolean with true if successful.


- **readBinary** (`string name`)

	> Reads a binary file with the provided file name.

	- **@p** `name` is a string with the file name to read.
	- **@r** `A` buffer object with the data from the file.


- **\_readBinary** (`string name, object buff`)

	> Wrapped function for passing in a new buffer object to read into.

	- **@p** `name` is a string with the file name to read.
	- **@param** `buff` is a buffer object to read data into.
	- **@r** `A` buffer object with the results.


- **ls** (`string dir`)

	> Produces a list of files and directories in the provided directory name string. This function returns a list of map objects with many details for each file or directory.

	- **@p** `dir` is a string with the directory to list.
	- **@r** `A` list of map objects with the directory contents.


- **rm** (`string name`)

	> Removes the file or directory with the name provided.

	- **@p** `name` is a string with the file or directory to remove.
	- **@r** `A` boolean with true for success.


- **rmr** (`string name`)

	> Removes the file or directory with the name provided recursively.

	- **@p** `name` is a string with the file or directory to remove.
	- **@r** `A` boolean with true for success.


- **exists** (`string name`)

	> Checks to see if the provided file or directory exists.

	- **@p** `name` is a string with the file or directory to check.
	- **@r** `A` boolean with true for exists and false for not.


- **isFile** (`string name`)

	> Checks to see if the provided name is a file.

	- **@p** `name` is a string to check if it's a file.
	- **@r** `A` boolean with true if it's a file.


- **isDir** (`string name`)

	> Checks to see if a provided name is a directory.

	- **@p** `name` is a string to check if it's a directory.
	- **@r** `A` boolean with true if it's a directory.


- **canExecute** (`string name`)

	> Checks to see if the provided name can be executed.

	- **@p** `name` is a string to check if it can be executed.
	- **@r** `A` boolean with true if it can be executed.


- **canRead** (`string name`)

	> Checks to see if the provided name can be read.

	- **@p** `name` is a string to check if it can be read.
	- **@r** `A` boolean with true if it can be read.


- **canWrite** (`string name`)

	> Checks to see if the provided name can be written.

	- **@p** `name` is a string to check if it can be written to.
	- **@r** `A` boolean with true if it can be written to.


- **getAbsPath** (`string name`)

	> Gets the absolute path with the provided file name.

	- **@p** `name` is a string with a file name.
	- **@r** `A` string with the absolute path.


- **getCanonicalPath** (`string name`)

	> Gets the canonical path with the provided file name.

	- **@p** `name` is a string with a file name.
	- **@r** `A` string with canonical path.


- **getName** (`string name`)

	> Gets the name of the file or path with the provide file or path name.

	- **@p** `name` is a string with a file or path name.
	- **@r** `A` string with the file or path name.


- **getParent** (`string name`)

	> Gets the parent directory of the provided file or directory.

	- **@p** `name` is a string with a file or directory.
	- **@r** `A` string with the parent directory.


- **isAbsolute** (`string name`)

	> Checks if the provided path name is absolute.

	- **@p** `name` is a string with the path to check.
	- **@r** `A` boolean with true if it's absolute.


- **isHidden** (`string name`)

	> Checks to see if the provided path is hidden.

	- **@p** `name` is a string with the path to check.
	- **@r** `A` boolean with true if the path is hidden.


- **lastModified** (`string name`)

	> Gets the last modified date for the provided path.

	- **@p** `name` is a string with the path.
	- **@r** `An` int with the seconds since epoch of the last modified date.


- **length** (`string name`)

	> Gets the length of the path in bytes.

	- **@p** `name` is a string of the file name.
	- **@r** `An` int with the number of bytes.


- **mkdir** (`string name`)

	> Makes a directory with the provided name.

	- **@p** `name` is a string with the directory name to create.
	- **@r** `A` boolean with true.


- **mkdirs** (`string name`)

	> Makes nested directories with the provided path.

	- **@p** `name` is a string with the path of directories to create.
	- **@r** `A` boolean with true.


- **rename** (`string name, string newName`)

	> Attempts to rename the provided file to a new file name.

	- **@p** `name` is a string with the source file name.
	- **@p** `newName` is a string with the destination file name.
	- **@r** `A` boolean with true.


- **setExecutable** (`string name`)

	> Sets the provided file executable.

	- **@p** `name` is a string with the file name to set.
	- **@r** `A` boolean with true.


- **setReadable** (`string name`)

	> Sets the provided file as readable.

	- **@p** `name` is a string with the file name to set.
	- **@r** `A` boolean with true.


- **setWritable** (`string name`)

	> Sets the provided file to writable.

	- **@p** `name` is a string with the file name to set.
	- **@r** `A` boolean with true.


- **cp** (`string src, string dst, bool replace = true`)

	> Copies the source file to the destination file.

	- **@p** `src` is a string with the source file.
	- **@p** `dst` is a string with the destination file.
	- **@p** `replace` is a boolean with true to replace if it exists and false if not. (default true)
	- **@r** `A` boolean with true.


- **cpr** (`string src, string dst, bool replace = true`)

	> Copies the source file to the destination file. The cpr function is similar to cp but it coppies recursively.

	- **@p** `src` is a string with the source file.
	- **@p** `dst` is a string with the destination file.
	- **@p** `replace` is a boolean with true to replace if it exists and false if not. (default true)
	- **@r** `A` boolean with true.




