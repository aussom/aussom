# file: app.aus

## class: app

[9:21] `static` (extern: com.lehman.aussom.stdlib.AussomApp) **extends: object** 

Aussom app is a static class which provides various core
functions such as dynamic jar loading and such.

#### Methods

- **loadJar** (`string jarName`)

	> Loads the .jar file with the provided name.

	- **@p** `jarName` is the name of the .jar file to load.
	- **@r** `A` boolean with true if successful.


- **openWebPage** (`string webPage`)

	> Opens a web page with the system browser with the provided URL string.

	- **@p** `webPage` is a string with the web URL or local file.




