# file: Tooltip.aus

## class: Tooltip

[9:7] **extends: FxObj** 

Represents a JavaFX Tooltip, a UI element that displays additional information when hovering over a node.
The Tooltip class allows setting tooltip text and specifying which node to attach it to.

#### Methods

- **Tooltip** (`string Val`)

	> Initializes a new Tooltip instance with the specified text.

	- **@p** `Val` Val is a string to set as the text of the tooltip.
	- **@r** `this` object


- **install** (`InstallTo`)

	> Installs the Tooltip on a specified node, so it displays when the user hovers over that node.

	- **@p** `InstallTo` The node object to which the tooltip should be attached.
	- **@r** `this` object for chaining




