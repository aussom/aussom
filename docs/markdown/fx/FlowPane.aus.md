# file: FlowPane.aus

## class: FlowPane

[8:7] **extends: Node, Pane** 

JavaFX FileChooser class.

#### Methods

- **FlowPane** ()

	> Default constructor that creates the FlowPane.

	- **@r** `this` object




