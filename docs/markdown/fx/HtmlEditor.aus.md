# file: HtmlEditor.aus

## class: HtmlEditor

[10:7] **extends: Node, Control, Region** 

Represents a JavaFX HTML editor component, allowing for rich text editing and formatting.
The HtmlEditor class provides an interface to edit HTML-formatted text. It extends Node,
Control, and Region, and offers methods for setting and retrieving HTML content.

#### Methods

- **HtmlEditor** ()

	> Initializes a new HtmlEditor instance. This constructor creates a JavaFX HTMLEditor object and assigns it to this.obj.

	- **@r** `this` object


- **setHtmlText** (`string HtmlText`)

	> Sets the HTML content of the editor. Allows the user to input or update the current HTML text displayed in the editor.

	- **@p** `HtmlText` The HTML-formatted string to set in the editor.
	- **@r** `this` object for chaining


- **getHtmlText** ()

	> Retrieves the current HTML content of the editor. Returns the HTML-formatted text that is currently in the editor.

	- **@r** `string` The current HTML text displayed in the editor.




