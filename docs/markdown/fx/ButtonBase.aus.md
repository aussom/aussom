# file: ButtonBase.aus

## class: ButtonBase

[9:7] **extends: object** 

JavaFX ButtonBase class implements various
button related functions.

#### Methods

- **onClick** (`callback OnClick`)

	> Sets the on click handler with the provided callback.

	- **@p** `OnClick` is a callback with the function reference to call on click.
	- **@r** `this` object.




