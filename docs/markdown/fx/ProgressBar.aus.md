# file: ProgressBar.aus

## class: ProgressBar

[12:7] **extends: ProgressIndicator** 

Represents a JavaFX ProgressBar, a control to display a progress indicator.
The ProgressBar class extends ProgressIndicator and is defaulted to 0.0 progress
to reduce processor-intensive animations.

#### Methods

- **ProgressBar** ()

	> Initializes a new ProgressBar instance. The progress is defaulted to 0.0 to avoid resource-heavy animations.

	- **@r** `this` object




