# file: FontAwesome.aus

## class: Glyph

[45:7] **extends: Label, Region** 

org.controlsfx.glyphfont.Glyph

#### Methods

- **Glyph** (`Obj`)




## class: FontAwesome

[8:14] `static` **extends: object** 

JavaFX FontAwesome class.

#### Members
- **fa**

#### Methods

- **get** ()


- **names** ()

	> Gets a list of all glyph names. @ A list of strings with all the names.



- **create** (`string Name`)

	> Creates a Glyph with the provided name and returns it.

	- **@p** `Name` is a string with the glyph name. (ie GEAR, SAVE ...)
	- **@r** `A` Glyph to be used as a graphic.




