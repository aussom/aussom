# file: FileChooser.aus

## class: FileChooser

[8:7] **extends: FxObj** 

JavaFX FileChooser class.

#### Methods

- **FileChooser** (`string Title = null`)

	> Constructor takes the title of the FileChooser dialog.

	- **@p** `Title` is an optional string with the title of the dialog.
	- **@r** `this` object


- **setTitle** (`string Title`)

	> Sets the dialog title with the provided string.

	- **@p** `Title` is a string with the title to set.
	- **@r** `this` object


- **setInitDir** (`string Dir`)

	> Sets the initial directory to start the FileChooser dialog in.

	- **@p** `Dir` is a string with the directory to start in.
	- **@r** `this` object


- **setInitFileName** (`string FileName`)

	> Sets the initial file name of the FileChooser dialog.

	- **@p** `FileName` is a string with the FileName to set.
	- **@r** `this` object


- **addFilter** (`string FilterName, list ExtList`)

	> Adds the FileChooser filter with the provided name and list of extensions.

	- **@p** `FilterName` is a string with the name of the filter.
	- **@p** `ExtList` is a list of file extensions for this filter. An example is ['*.jpg', '*.jpeg', '*.png']
	- **@r** `this` object


- **show** (`MainStage`)

	> Shows the standard FileChooser dialog with the provided main Stage object.

	- **@p** `MainStage` is the main JavaFX Stage object.
	- **@r** `A` string with the path of the file that was picked or null if no file was chosen.


- **showMultiple** (`MainStage`)

	> Shows the dialog with the provided main Stage object but allowing multiple file selection.

	- **@p** `MainStage` is the main JavaFX Stage object.
	- **@r** `A` list of strings with the files that were chosen or an empty list of none were picked.


- **showSave** (`MainStage`)

	> Shows the dialog with the provided main Stage object but for save file.

	- **@p** `MainStage` is the main JavaFX Stage object.
	- **@r** `A` string with the path of the file that was picked or null if no file was chosen.




