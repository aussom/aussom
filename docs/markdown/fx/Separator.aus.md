# file: Separator.aus

## class: Separator

[9:7] **extends: Node, Control** 

Represents a JavaFX Separator, a horizontal or vertical line used to visually separate content areas.
The Separator class extends Node and Control, providing a simple way to divide sections within a layout.

#### Methods

- **Separator** ()

	> Initializes a new Separator instance.

	- **@r** `this` object




