# file: MenuBar.aus

## class: MenuBar

[9:7] **extends: Node, Control, Region** 

Represents a JavaFX MenuBar, a UI component that can hold multiple Menu items.
The MenuBar class provides functionality to add menus in a horizontal menu bar.

#### Methods

- **MenuBar** ()

	> Initializes a new MenuBar instance.

	- **@r** `this` object


- **add** (`Items`)

	> Adds one or more Menu objects to the MenuBar.

	- **@p** `Items` Either a single Menu or a list of Menus to add.
	- **@r** `this` object for chaining




