# file: Labeled.aus

## class: Labeled

[9:7] **extends: object** 

This class provides easy to use helpers for setting
JavaFX Labeled values.

#### Methods

- **setFont** (`object FontObj`)

	> Sets the font to use.

	- **@p** `FontObject` is a Font object to set.
	- **@r** `this` object


- **setGraphic** (`object Graphic`)

	> Sets the provided graphic. This is an Image or ImageView object.



- **getText** ()

	> Gets the label text.

	- **@r** `A` string with the current set text.


- **setText** (`string Text`)

	> Sets the provided text.

	- **@p** `Text` is a string with the Text to set.
	- **@r** `this` object




