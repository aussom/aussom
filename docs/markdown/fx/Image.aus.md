# file: Image.aus

## class: Image

[9:7] **extends: FxObj** 

Represents a JavaFX Image, used for loading and displaying images from a file or URL.
The Image class extends FxObj and supports both local and web-based image sources.

#### Methods

- **Image** (`string FileName`)

	> Initializes a new Image instance with the specified file path or URL. If a URL is provided, the image is loaded directly from the web; otherwise, a local file is loaded.

	- **@p** `FileName` The path or URL of the image to be loaded.
	- **@r** `this` object




