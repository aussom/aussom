# file: ChoiceBox.aus

## class: ChoiceBox

[8:7] **extends: Node, Control** 

JavaFX ChoiceBox class.

#### Methods

- **ChoiceBox** (`Val`)

	> Constructor creates a new ChoiceBox object with the provided JavaFX object to set or a list with the available values to set.

	- **@p** `Val` is a list of strings with the ChoiceBox values.
	- **@r** `this` object


- **setItems** (`list lst`)

	> Sets the available items with the provided list.

	- **@p** `lst` is a list with the values.
	- **@r** `this` object


- **getValue** ()

	> Gets the selected value.

	- **@r** `The` selected value.


- **onChange** (`callback OnChange`)

	> Registeres the on change handler with the provided callback. The change handler should take three arguments similar to this function definition. onChoiceBoxChange(ObservableValue, OldValue, NewValue)

	- **@p** `OnChange` is a callback with the change handler.
	- **@r** `this` object




