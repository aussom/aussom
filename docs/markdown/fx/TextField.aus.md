# file: TextField.aus

## class: TextField

[11:7] **extends: Node, Control, TextInputControl** 

Represents a JavaFX TextField, a control for single-line text input.
The TextField class extends Node, Control, and TextInputControl, providing functionality for text entry and display.

#### Methods

- **TextField** (`Val = null`)

	> Initializes a new TextField instance with optional initial text. If no text is provided, the TextField is initialized with an empty string.

	- **@p** `Val` Val is a string to set as the initial text in the TextField, or null for empty text.
	- **@r** `this` object




