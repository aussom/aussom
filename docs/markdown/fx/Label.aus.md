# file: Label.aus

## class: Label

[12:7] **extends: Node, Control, Labeled** 

Represents a JavaFX Label, a non-editable text control for displaying text.
The Label class extends Node, Control, and Labeled, providing functionality
for setting label text and wrap options.

#### Methods

- **Label** (`Val`)

	> Initializes a new Label instance with the specified text. If no text is provided, the default text "Label" is used.

	- **@p** `Val` The label text to display, or null for the default.
	- **@r** `this` object


- **setWrap** (`bool Wrap`)

	> Sets whether the label text should wrap if it exceeds the label width.

	- **@p** `Wrap` A boolean specifying if the text should wrap.
	- **@r** `this` object for chaining




