# file: GridPane.aus

## class: GridPane

[8:7] **extends: Node, Pane** 

JavaFX GridPane class.

#### Methods

- **GridPane** ()

	> Constructor that creates the GridPane object.

	- **@r** `this` object




