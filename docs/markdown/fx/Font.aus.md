# file: Font.aus

## class: Font

[8:14] `static` **extends: object** 

Font related functions.

#### Methods

- **get** (`string FontName, double FontSize`)

	> Gets a font with the provided font name and size.

	- **@p** `FontName` is a string with the local font to get.
	- **@p** `FontSize` is a double with the font size to use.
	- **@r** `A` FxObj object with the font resource.


- **load** (`string FileStr, double FontSize`)

	> Gets a font with the provided URL string and size.

	- **@p** `FileStr` is a string with the file name where to get the font.
	- **@p** `FontSize` is a double with the font size to use.
	- **@r** `A` FxObj object with the font resource.


- **loadUrl** (`string UrlStr, double FontSize`)

	> Gets a font with the provided URL string and size.

	- **@p** `UrlStr` is a string with the URL where to get the font.
	- **@p** `FontSize` is a double with the font size to use.
	- **@r** `A` FxObj object with the font resource.




