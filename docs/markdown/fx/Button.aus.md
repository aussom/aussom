# file: Button.aus

## class: Button

[11:7] **extends: Node, Control, Labeled, ButtonBase** 

JavaFX Button class.

#### Methods

- **Button** (`Val`)

	> Constructor takes either an existing button object to set or a string with the button text.

	- **@p** `Val` is a string with the button text.
	- **@r** `this` object




