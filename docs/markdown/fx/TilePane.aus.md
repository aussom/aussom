# file: TilePane.aus

## class: TilePane

[9:7] **extends: Node, Pane** 

Represents a JavaFX TilePane, a layout pane that arranges its children in uniformly sized tiles.
The TilePane class is useful for creating grid-like structures where nodes are placed in a flow of equal-sized tiles.

#### Methods

- **TilePane** ()

	> Initializes a new TilePane instance.

	- **@r** `this` object




