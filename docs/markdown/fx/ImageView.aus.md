# file: ImageView.aus

## class: ImageView

[11:7] **extends: Node** 

Represents a JavaFX ImageView, a component for displaying images.
The ImageView class supports setting an image via an Image object, URL string, or null.

#### Methods

- **ImageView** (`Image = null`)

	> Creates a new ImageView object with either an Image object, a string URL, or null for no image.

	- **@p** `Image` An Image object, URL string, or null.
	- **@r** `this` object


- **setImage** (`object Image`)

	> Sets the image for the ImageView.

	- **@p** `Image` An Image object to display in the ImageView.
	- **@r** `this` object for chaining




