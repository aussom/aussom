# file: ToggleGroup.aus

## class: ToggleGroup

[9:7] **extends: FxObj, Control** 

Represents a JavaFX ToggleGroup, used to group ToggleButton instances so that only one can be selected at a time.
The ToggleGroup class provides methods for retrieving the selected button and handling selection change events.

#### Methods

- **ToggleGroup** ()

	> Initializes a new ToggleGroup instance.

	- **@r** `this` object


- **getSelected** ()

	> Retrieves the text of the currently selected ToggleButton in the group.

	- **@r** `string` The text of the selected ToggleButton, or an empty string if none is selected.


- **onChange** (`callback OnChange`)

	> Sets a callback function to execute when the selected ToggleButton changes.

	- **@p** `OnChange` The callback function for handling selection change events.
	- **@r** `this` object for chaining




