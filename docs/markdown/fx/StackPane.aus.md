# file: StackPane.aus

## class: StackPane

[9:7] **extends: Node, Pane** 

Represents a JavaFX StackPane, a layout component that stacks its children on top of each other.
The StackPane class allows for easy layering of nodes, with each new node placed on top of the previous ones.

#### Methods

- **StackPane** ()

	> Initializes a new StackPane instance.

	- **@r** `this` object




