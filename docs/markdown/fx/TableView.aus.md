# file: TableView.aus

## class: TableView

[9:7] **extends: Node, Control, Region** 

Represents a JavaFX TableView, a control for displaying tabular data with customizable columns and selectable rows.
The TableView class supports setting columns, populating rows, and handling selection change events.

#### Members
- **cols**

#### Methods

- **TableView** (`Val = null`)

	> Initializes a new TableView instance with optional columns. If a list of columns is provided, they are set as the initial columns.

	- **@p** `Val` Val is either a list of columns to initialize or null for an empty table.
	- **@r** `this` object


- **setColumns** (`list Cols`)

	> Sets the columns for the TableView. Each column in the list should include display and column name information.

	- **@p** `Cols` Cols is a list of column objects to add to the table.
	- **@r** `this` object for chaining


- **setItems** (`list Rows`)

	> Sets the rows for the TableView.

	- **@p** `Rows` Rows is a list of row data to populate the table.
	- **@r** `void` 


- **onChange** (`callback OnChange`)

	> Sets a callback function that triggers when the selected item changes.

	- **@p** `OnChange` The callback function for the selection change event.
	- **@r** `this` object for chaining


- **getSelected** ()

	> Retrieves the currently selected item in the TableView.

	- **@r** `The` selected item object, or null if no item is selected.


- **getSelectionModel** ()

	> Internal method to get the selection model for the TableView.

	- **@r** `The` selection model for managing item selection.


- **onCellValueFactoryCall** (`Param`)

	> Internal method for handling cell value factory calls for columns. This method provides cell data based on row values and column names.

	- **@p** `Param` Param is an object passed to retrieve column and row information.
	- **@r** `A` wrapper containing the value for the requested cell.




