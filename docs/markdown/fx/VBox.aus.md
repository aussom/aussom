# file: VBox.aus

## class: VBox

[9:7] **extends: Node, Pane** 

Represents a JavaFX VBox, a layout pane that arranges its children in a vertical column.
The VBox class is useful for stacking nodes vertically with customizable spacing and alignment.

#### Methods

- **VBox** ()

	> Initializes a new VBox instance.

	- **@r** `this` object




