# file: DialogPane.aus

## class: DialogPane

[8:7] **extends: Node, Pane** 

JavaFX DialogPane class.

#### Methods

- **DialogPane** ()

	> Constructor that creates the DialogPane object.

	- **@r** `this` object




