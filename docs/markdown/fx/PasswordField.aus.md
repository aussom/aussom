# file: PasswordField.aus

## class: PasswordField

[11:7] **extends: TextField** 

Represents a JavaFX PasswordField, a text input control designed to handle sensitive text input, such as passwords.
The PasswordField class extends TextField and masks entered characters for privacy.

#### Methods

- **PasswordField** (`Val = null`)

	> Initializes a new PasswordField instance with an optional initial value. If a value is provided, it sets the text within the password field.

	- **@p** `Val` The initial text for the PasswordField, or null for an empty field.
	- **@r** `this` object




