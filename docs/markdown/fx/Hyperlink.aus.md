# file: Hyperlink.aus

## class: Hyperlink

[13:7] **extends: Node, Control, Labeled, ButtonBase** 

Represents a JavaFX Hyperlink, a UI control that functions like a clickable hyperlink.
The Hyperlink class extends Node, Control, Labeled, and ButtonBase, allowing for
flexible styling and event handling.

#### Methods

- **Hyperlink** (`Val`)

	> Initializes a new Hyperlink instance with a specified label. If a value is provided, it is used as the display text; otherwise, an empty string is set.

	- **@p** `Val` The label text for the hyperlink, or null for an empty label.
	- **@r** `this` object




