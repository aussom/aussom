# file: RadioButton.aus

## class: RadioButton

[12:7] **extends: Node, Control, Labeled, ToggleButton** 

Represents a JavaFX RadioButton, a selectable button in a group where only one can be selected.
The RadioButton class extends Node, Control, Labeled, and ToggleButton.

#### Methods

- **RadioButton** (`Val`)

	> Initializes a new RadioButton instance with the specified label text. If no text is provided, the label defaults to an empty string.

	- **@p** `Val` The text label for the RadioButton, or null for a blank label.
	- **@r** `this` object




