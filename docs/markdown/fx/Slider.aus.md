# file: Slider.aus

## class: Slider

[9:7] **extends: Node, Control, Region** 

Represents a JavaFX Slider, a control for selecting a numeric value within a range by sliding a knob.
The Slider class supports optional tick marks, tick labels, and change event handling.

#### Methods

- **Slider** (`double Min, double Max, double Val`)

	> Initializes a new Slider instance with specified minimum, maximum, and initial values.

	- **@p** `Min` is a double value that sets the minimum value of the slider.
	- **@p** `Max` is a double value that sets the maximum value of the slider.
	- **@p** `Val` is a double value to set for the initial position of the slider.
	- **@r** `this` object


- **setShowTicks** (`bool ShowTicks`)

	> Sets whether tick marks should be displayed on the slider.

	- **@p** `ShowTicks` A boolean specifying whether to show tick marks.
	- **@r** `this` object for chaining


- **setShowLabels** (`bool ShowTicks`)

	> Sets whether tick labels should be displayed on the slider.

	- **@p** `ShowTicks` A boolean specifying whether to show tick labels.
	- **@r** `this` object for chaining


- **setValue** (`double Val`)

	> Sets the current value of the slider.

	- **@p** `Val` is a double value to set for the slider.
	- **@r** `this` object for chaining


- **onChange** (`callback OnChange`)

	> Sets a callback function to execute when the slider's value changes.

	- **@p** `OnChange` The callback function for the value change event.
	- **@r** `void` 




