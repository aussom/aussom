# file: HBox.aus

## class: HBox

[9:7] **extends: Node, Pane** 

Represents a JavaFX HBox, a layout component that arranges
its children in a single horizontal row.

#### Methods

- **HBox** ()

	> Initializes a new HBox instance. This constructor creates a JavaFX HBox object and assigns it to this.obj.

	- **@r** `this` object




