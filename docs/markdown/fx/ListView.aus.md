# file: ListView.aus

## class: ListView

[10:7] **extends: Node, Control, Region** 

Represents a JavaFX ListView, a control for displaying a list of items.
The ListView class allows for item selection, change listeners, and retrieval
of the selected item.

#### Methods

- **ListView** (`Items = null`)

	> Initializes a new ListView instance with optional items. If an item list is provided, it is set as the initial content of the ListView.

	- **@p** `Items` An array of items to populate the ListView, or null for an empty list.
	- **@r** `this` object


- **onChange** (`callback OnChange`)

	> Sets a callback function that triggers when the selected item changes.

	- **@p** `OnChange` The callback function to execute on item selection change.
	- **@r** `this` object for chaining


- **getSelected** ()

	> Retrieves the currently selected item in the ListView.

	- **@r** `The` selected item object, or null if no item is selected.


- **getSelectionModel** ()

	> Internal method to get the selection model for the ListView.

	- **@r** `The` selection model for managing item selection.




