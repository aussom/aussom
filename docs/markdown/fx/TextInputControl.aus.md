# file: TextInputControl.aus

## class: TextInputControl

[9:7] **extends: object** 

Represents a base class for text input controls, providing methods for setting and getting text,
as well as handling text change events.

#### Methods

- **setText** (`string Text`)

	> Sets the text content of the text input control.

	- **@p** `Text` is a string to set as the current content of the control.
	- **@r** `this` object for chaining


- **getText** ()

	> Retrieves the current text content of the text input control.

	- **@r** `A` string with the current text in the control.


- **onChange** (`callback OnChange`)

	> Sets a callback function that triggers when the text content changes.

	- **@p** `OnChange` The callback function for handling text change events.
	- **@r** `this` object for chaining




