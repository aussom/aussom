# file: ProgressIndicator.aus

## class: ProgressIndicator

[10:7] **extends: Node, Control, Region** 

Represents a JavaFX ProgressIndicator, a control for displaying progress in a circular form.
The ProgressIndicator class supports setting a specific progress level, defaulting to 0.0
to reduce resource usage.

#### Methods

- **ProgressIndicator** ()

	> Initializes a new ProgressIndicator instance. The progress is defaulted to 0.0 to avoid processor-intensive animations.

	- **@r** `this` object


- **setProgress** (`double Progress`)

	> Sets the current progress level of the ProgressIndicator.

	- **@p** `Progress` The progress value to display, where 0.0 is no progress and 1.0 is complete.
	- **@r** `this` object for chaining




