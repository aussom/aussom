# file: ComboBox.aus

## class: ComboBox

[8:7] **extends: Node, Control, Region** 

JavaFX ComboBox class.

#### Methods

- **ComboBox** (`Val`)

	> Constructor that takes a list of items to set.

	- **@p** `Val` is a list of items to set.
	- **@r** `this` object


- **setItems** (`list lst`)

	> Sets a list of items for the ComboBox.

	- **@p** `lst` is a list of items to set.
	- **@r** `this` object


- **getValue** ()

	> Gets the current selected object.

	- **@r** `The` value.


- **setVisibleRows** (`int Count`)

	> Sets the number of visible rows to show in the ComboBox.

	- **@p** `Count` is an int with the number of rows to show.
	- **@r** `this` object


- **onChange** (`callback OnChange`)

	> Sets the on change handler callback function. Callback example: onComboBoxChange(ObservableValue, OldValue, NewValue)

	- **@p** `OnChange` is a callback function to call on change.
	- **@r** `this` object




