# file: Menu.aus

## class: Menu

[11:7] **extends: MenuItem** 

Represents a JavaFX Menu, a UI component that can hold menu items or submenus.
The Menu class extends MenuItem, allowing for nested items within a menu structure.

#### Methods

- **Menu** (`string Name`)

	> Initializes a new Menu instance with the specified name.

	- **@p** `Name` The name displayed on the menu.
	- **@r** `this` object


- **add** (`Items`)

	> Adds one or more Menu or MenuItem objects to this Menu.

	- **@p** `Items` Either a single Menu/MenuItem or a list of them to add.
	- **@r** `this` object




