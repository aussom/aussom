# file: CheckBox.aus

## class: CheckBox

[11:7] **extends: Node, Control, Labeled, ButtonBase** 

JavaFX CheckBox class.

#### Methods

- **CheckBox** (`Val`)

	> Creates a new CheckBox object with the provided text or JavaFX CheckBox object to set.

	- **@p** `Val` is a string with the CheckBox text.
	- **@r** `this` object


- **setSelected** (`bool Selected`)

	> Sets the CheckBox selected flag.

	- **@p** `Selected` is a bool with the selected value.
	- **@r** `this` object


- **getSelected** ()

	> Gets the CheckBox selected value.

	- **@r** `A` bool with the selected value.




