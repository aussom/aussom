# file: Color.aus

## class: Color

[9:7] **extends: FxObj** 

Color object stores a JavaFX Color object and provides
functions to set it.

#### Methods

- **Color** (`string Val = null, double Alpha = 1.0`)

	> Creates a new Color object with the provied optional hex string and alpha double values.

	- **@p** `Val` is an optional string with the web hex value to set. (ex. #ffffff)
	- **@p** `Alpha` is an optional double between 0 and 1 with the alpha value.
	- **@r** `this` object


- **web** (`string Val, double Alpha = 1.0`)

	> Set the color value ith the provied hex string and optional alpha double values.

	- **@p** `Val` is an optional string with the web hex value to set. (ex. #ffffff)
	- **@p** `Alpha` is an optional double between 0 and 1 with the alpha value.
	- **@r** `this` object


- **rgb** (`double Red, double Blue, double Green`)

	> Set the color value ith the provied red blue green values.

	- **@p** `Red` is a double with the red part. (0.0-255.0)
	- **@p** `Blue` is a double with the blue part. (0.0-255.0)
	- **@p** `Green` is a double with the green part. (0.0-255.0)
	- **@r** `this` object




