# file: ScrollPane.aus

## class: SpPolicy

[11:6] `static` **extends: object** 

Enum representing scroll bar policies for a ScrollPane.
The SpPolicy options include: `ALWAYS`, which always shows the scrollbar;
`AS_NEEDED`, which displays the scrollbar only when necessary; and `NEVER`,
which hides the scrollbar at all times.

#### Members
- **ALWAYS**
- **AS\_NEEDED**
- **NEVER**



## class: ScrollPane

[21:7] **extends: Node, Control, Region** 

Represents a JavaFX ScrollPane, a control that provides a scrollable view for its content.
The ScrollPane class supports setting horizontal and vertical scrollbar policies.

#### Methods

- **ScrollPane** ()

	> Initializes a new ScrollPane instance.

	- **@r** `this` object


- **setContent** (`Val`)

	> Sets the content to be displayed within the ScrollPane.

	- **@p** `Val` The Node object to set as the content.
	- **@r** `void` 


- **setHBarPolicy** (`Policy`)

	> Sets the horizontal scroll bar policy for the ScrollPane.

	- **@p** `Policy` The scroll bar policy, using values from SpPolicy (e.g., SpPolicy.ALWAYS).
	- **@r** `this` object for chaining


- **setVBarPolicy** (`Policy`)

	> Sets the vertical scroll bar policy for the ScrollPane.

	- **@p** `Policy` The scroll bar policy, using values from SpPolicy (e.g., SpPolicy.AS_NEEDED).
	- **@r** `this` object for chaining




