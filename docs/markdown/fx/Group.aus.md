# file: Group.aus

## class: Group

[8:7] **extends: Node** 

JavaFX Group class.

#### Methods

- **Group** ()

	> Constructor creates the Group object.

	- **@r** `this` object


- **add** (`Items`)

	> Add a signgle item, or multiple items to the pane.

	- **@p** `Items` is either a single item or a list of items to add.
	- **@r** `this` object




