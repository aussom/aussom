# file: ColorPicker.aus

## class: ColorPicker

[8:7] **extends: Node, Control, Region** 

JavaFX ColorPicker class.

#### Methods

- **ColorPicker** (`object ColorVal = null`)

	> Constructor takes an optional default color value to apply to the color picker.

	- **@p** `ColorVal` is an optional Color object to set.
	- **@r** `this` object


- **setValue** (`object ColorVal`)

	> Sets the provided color value.

	- **@p** `ColorVal` is a Color object to set.
	- **@r** `this` object


- **getValue** ()

	> Gets the current selected color object.

	- **@r** `A` hex formatted string with the color value.


- **onChange** (`callback OnChange`)

	> Sets the on change handler with the provided callback function to call on change. The callback takes a single argument with the event.

	- **@p** `OnChange` is a callback to call on change.
	- **@r** `this` object




