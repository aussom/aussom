# file: MenuItem.aus

## class: MenuItem

[9:7] **extends: Node, Control** 

Represents a JavaFX MenuItem, a selectable item within a Menu.
The MenuItem class supports setting a display name, an optional image, and click events.

#### Methods

- **MenuItem** (`string Name, Img = null`)

	> Initializes a new MenuItem with the specified name and optional image.

	- **@p** `Name` The name displayed on the menu item.
	- **@p** `Img` An optional Image object to display with the menu item.
	- **@r** `this` object


- **onClick** (`callback OnClick`)

	> Sets a callback function to execute when the menu item is clicked.

	- **@p** `OnClick` The callback function for the click event.
	- **@r** `void` 




