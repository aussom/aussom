# file: ToggleButton.aus

## class: ToggleButton

[12:7] **extends: Node, Control, Labeled, ButtonBase** 

Represents a JavaFX ToggleButton, a button that can be toggled between selected and unselected states.
The ToggleButton class extends Node, Control, Labeled, and ButtonBase, and supports grouping with other toggle buttons.

#### Methods

- **ToggleButton** (`Val`)

	> Initializes a new ToggleButton instance with the specified label. If no label is provided, the button text defaults to an empty string.

	- **@p** `Val` is a string to set as the label for the ToggleButton.
	- **@r** `this` object


- **setToggleGroup** (`object TGroup`)

	> Sets the toggle group for this ToggleButton, linking it with other ToggleButton instances in the same group.

	- **@p** `TGroup` An object representing the toggle group to which this button belongs.
	- **@r** `this` object for chaining


- **setSelected** (`bool Selected`)

	> Sets the selected state of the ToggleButton.

	- **@p** `Selected` A boolean specifying the selected state of the button.
	- **@r** `this` object for chaining




