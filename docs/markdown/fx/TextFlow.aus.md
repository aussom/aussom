# file: TextFlow.aus

## class: TextFlow

[9:7] **extends: Node, Pane** 

Represents a JavaFX TextFlow, a layout pane that arranges text in a horizontal flow
while allowing individual text nodes to wrap as needed within the pane width.

#### Methods

- **TextFlow** ()

	> Initializes a new TextFlow instance.

	- **@r** `this` object




