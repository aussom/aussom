# file: yaml.aus

## class: yaml

[8:21] `static` (extern: com.lehman.aussom.stdlib.AussomYaml) **extends: object** 

Yaml class supports parsing and creating of yaml files.

#### Methods

- **parse** (`string YamlString`)

	> Parses the yaml file in the provided string.

	- **@p** `YamlString` is a string with the yaml file.
	- **@r** `A` map with the parsed result.


- **parseFile** (`string FileName`)

	> Parses the provided yaml file.

	- **@p** `FileName` is a string with the yaml file to parse.
	- **@r** `A` map with the parsed result.




