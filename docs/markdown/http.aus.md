# file: http.aus

## class: http

[8:14] (extern: com.lehman.aussom.stdlib.AussomHttp) **extends: object** 

Http class implements basic HTTP requests.

#### Methods

- **setTrustAllCerts** (`bool TrustAllCerts`)

	> Sets the trust all certs flag and resets the HTTP client. Setting to true isn't recommended for production environments.

	- **@p** `TrustAllCerts` is a bool with true to trust all certificates and false to not.
	- **@r** `this` object


- **get** (`string Url`)

	> Makes a HTTP GET request to the provided Url and returns a response object.

	- **@p** `Url` is a string with the URL to request.
	- **@r** `A` HTTP response object.


- **post** (`string Url, string Content, string MediaType = "application/json; charset=utf-8"`)

	> Makes a HTTP POST request with the provided arguments.

	- **@p** `Url` is a string with the URL to request.
	- **@p** `Content` is a string with the POST body content.
	- **@p** `MediaType` is a string with the media type. It defaults to application/json and charset utf-8.
	- **@r** `A` HTTP response object.


- **put** (`string Url, string Content, string MediaType = "application/json; charset=utf-8"`)

	> Makes a HTTP PUT request with the provided arguments.

	- **@p** `Url` is a string with the URL to request.
	- **@p** `Content` is a string with the PUT body content.
	- **@p** `MediaType` is a string with the media type. It defaults to application/json and charset utf-8.
	- **@r** `A` HTTP response object.


- **patch** (`string Url, string Content, string MediaType = "application/json; charset=utf-8"`)

	> Makes a HTTP PATCH request with the provided arguments.

	- **@p** `Url` is a string with the URL to request.
	- **@p** `Content` is a string with the PATCH body content.
	- **@p** `MediaType` is a string with the media type. It defaults to application/json and charset utf-8.
	- **@r** `A` HTTP response object.


- **delete** (`string Url`)

	> Makes a HTTP DELETE request to the provided Url and returns a response object.

	- **@p** `Url` is a string with the URL to request.
	- **@r** `A` HTTP response object.




