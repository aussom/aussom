# file: xml.aus

## class: xmltype

[8:6] `static` **extends: object** 

Defines the XML types in this enum.

#### Members
- **ELEMENT**
- **ATTRIBUTE**
- **TEXT**
- **CDATA\_SECTION**
- **ENTITY\_REFERENCE**
- **ENTITY**
- **PROCESSING\_INSTRUCTION**
- **COMMENT**
- **DOCUMENT**
- **DOCUMENT\_TYPE**
- **DOCUMENT\_FRAGMENT**
- **NOTATION**



## class: xml

[28:21] `static` (extern: com.lehman.aussom.stdlib.AussomXml) **extends: object** 

XML class implements various functions for working with XML.
It implements parse, toXml, and helper functions for
creating XML nodes.

#### Methods

- **parse** (`string XmlString`)

	> Parses the provided XML string and returns an Aussom structure with the result.



- **toXml** (`map XmlStructure, bool Pretty = true, bool Strict = false`)

	> Converts the provided Aussom XML structure of maps and lists and returns an XML encoded string. Note that this function will attempt to create the XMl but will throw an excpeiton if it gets to a spot where it can't continue.

	- **@p** `XmlStructure` is a map with the XML document to convert.
	- **@p** `Pretty` is a bool with true for pretty print and false for not.
	- **@p** `Strict` is a bool with true for strict mode and false for not. If not strict mode, the function will make it's very best effort to convert the XML without exception. This means that it may not convert parts that are invalid.
	- **@r** `A` string with the XML encoded content.


- **newNode** (`string Name, string Type = "ELEMENT"`)

	> Creates a new XML node structure. This is a help function and it just returns a map with the strucure built. You can build the structure and then provide this to the toXml function that builds XML from it.

	- **@p** `Name` is a string with the node name.
	- **@p** `Type` is a string with the XML node type. See xmltype enum for options.
	- **@r** `A` map with the new node structure.


- **newCdata** (`string Text`)

	> Creates a new CDATA section node and returns it.

	- **@r** `A` map with the node.


- **newDoc** ()

	> Creates a new document node and returns it.

	- **@r** `A` map with the node.




