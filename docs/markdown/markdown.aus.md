# file: markdown.aus

## class: markdown

[9:21] `static` (extern: com.lehman.aussom.stdlib.AussomMarkdown) **extends: object** 

Aussom markdown is a static class with functions for converting
markdown files to HTML.

#### Methods

- **toHtml** (`string markdownStr`)

	> Writes a text file with to the provided file name with the provided data.

	- **@p** `markdownStr` is a string with the markdown source to convert to HTML.
	- **@r** `A` string with the converted HTML text.




