# file: handlebars.aus

## class: Handlebars

[8:14] (extern: com.lehman.aussom.stdlib.AussomHandlebars) **extends: object** 

Handlebars support object.

#### Methods

- **compileString** (`string Template`)

	> Compiles the provided template string.

	- **@p** `Template` is a string with the HTML template to compile.
	- **@r** `This` object.


- **compile** (`string Template`)

	> Compiles the provided template with the provided template file name.

	- **@p** `Template` is a string with the file name of the template to compile.
	- **@r** `This` object.


- **apply** (`JsonArgs`)

	> Applies the provided JSON arguments to the template and returns it as a string.

	- **@p** `JsonArgs` is a JSON string or an Aussom Object with the values to apply to the template.
	- **@r** `A` string with the template and values.




