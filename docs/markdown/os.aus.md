# file: os.aus

## class: os

[11:21] `static` (extern: com.lehman.aussom.stdlib.AussomOs) **extends: object** 

Represents a static external class providing access to operating system functionalities.
The `os` class is used to run OS-level commands and retrieve their execution results,
including success status and output. It offers a simple interface for interacting
with the system's command line or shell.

#### Methods

- **exec** (`string OsCommand, bool LogToStdout = false`)

	> Executes the provided OS command and returns a Map with the result. The map will contain a key 'success' with a boolean of true for success and false for failure. It also returns a key of 'result' with the text result from the command.

	- **@p** `OsCommand` is a string with the command to run.
	- **@p** `LogToStdout` is a bool with true to log to output and false to not.
	- **@r** `A` map with the results.


- **execRaw** (`list OsCommand, bool LogToStdout = false`)

	> Executes the provided OS command and returns a Map with the result. The map will contain a key 'success' with a boolean of true for success and false for failure. It also returns a key of 'result' with the text result from the command. The difference between this one and regular exec is that the reguarl one will execute /bin/sh -c <cmd> or cmd.exe -c <cmd> depending for Linux/Unix or Windows. With execRaw, the string passed gets sent all on it's own.

	- **@p** `OsCommand` is a list of strings with the command to run.
	- **@p** `LogToStdout` is a bool with true to log to output and false to not.
	- **@r** `A` map with the results.




