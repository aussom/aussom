# file: jansi.aus

## class: jansi

[11:14] `static` **extends: object** 

Jansi is a small ASL 2.0 licensed Java library that allows you
to use ANSI escape sequences to format your console output.

#### Members
- **ansiConsoleCls**

#### Methods

- **systemInstall** ()

	> Installs the jansi system.

	- **@r** `this` object


- **systemUninstall** ()

	> Unistalls the jansi system.

	- **@r** `this` object


- **println** (`Val`)

	> Prints a line with the provided value.

	- **@p** `Val` is the value to print. @ this object


- **getTermWidth** ()

	> Gets the terminal width.

	- **@r** `An` int with the width.




## class: ansi

[60:7] **extends: object** 

The ansi class provides a fluent API for generating
ANSI escape sequences.

#### Members
- **obj**

#### Methods

- **ansi** ()

	> Constructor creates a new ansi object and stores the Java object as this.obj.



- **a** (`Val`)

	> Appends the provided value.

	- **@p** `Val` is the value to append.
	- **@r** `this` object


- **eraseScreen** ()

	> Erases the screen.

	- **@r** `this` object


- **eraseLine** ()

	> Erases the current line.

	- **@r** `this` object


- **cursor** (`int row, int col`)

	> Moves the cursor to the provided location.

	- **@p** `row` is an int with the row number from the top of the screen.
	- **@p** `col` is an int with the column number from the left side of the screen.
	- **@r** `this` object


- **bg** (`int red, int green, int blue`)

	> Sets the background color with the provided RGB value.

	- **@p** `red` is an int with the red part.
	- **@p** `green` is an int with the green part.
	- **@p** `blue` is an int with the blue part.
	- **@r** `this` object


- **bold** ()

	> Turns on bold.

	- **@r** `this` object


- **boldOff** ()

	> Turns off bold.

	- **@r** `this` object


- **fg** (`int red, int green, int blue`)

	> Sets the foreground color with the provided RGB value.

	- **@p** `red` is an int with the red part.
	- **@p** `green` is an int with the green part.
	- **@p** `blue` is an int with the blue part.
	- **@r** `this` object


- **scrollDown** (`int rows`)

	> Scrolls the screen down.

	- **@r** `this` object


- **scrollUp** (`int rows`)

	> Scrolls the screen up.

	- **@r** `this` object




