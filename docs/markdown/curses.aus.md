# file: curses.aus

## class: textbox

[622:14] (extern: com.lehman.aussom.stdlib.curses.CursesTextBox) **extends: component** 

The textbox component allows for editing of text fields and
text areas. They can be read only or read and write.

#### Methods

- **textbox** (`string text, bool multiLine = true, int cols = null, int rows = null`)

	> Constructor that creates a textbox with the provided arguments.

	- **@p** `text` is the textbox text value.
	- **@p** `multiLine` is a boolean with true for multiline support and false for not.
	- **@p** `cols` is an int with the size in columns.
	- **@p** `rows` is an int with the size in rows.
	- **@r** `null` 


- **newTextBox** (`bool multiLine = true`)

	> Helper that creates the new textbox.

	- **@p** `multiLine` is a boolean with true for multiline and false for not.
	- **@r** `null` 


- **setText** (`string text`)

	> Sets the text for the text box.

	- **@p** `text` is a string with the text to set.
	- **@r** `null` 


- **setValidation** (`string regexPattern`)

	> Sets the validation pattern for the textbox.

	- **@p** `regexPattern` is a string with the regex pattern to match.
	- **@r** `null` 


- **setReadOnly** (`bool readOnly`)

	> Sets the read only boolean.

	- **@p** `readOnly` is a boolean with true for read only and false for not.
	- **@r** `null` 


- **getText** ()

	> Gets the text of the textbox.

	- **@r** `A` string with the text.




## class: borderlayout

[525:14] (extern: com.lehman.aussom.stdlib.curses.CursesBorderLayout) **extends: object** 

Border layout provides a similar layout to the swing border
layout. The layoutpos enum can be used to specify the 5 different
layout areas.



## class: actionbox

[910:14] (extern: com.lehman.aussom.stdlib.curses.CursesActionBox) **extends: component** 

Is a component that provides a list of actions.

#### Methods

- **actionbox** (`list items`)

	> Constructor for the actionbox object.

	- **@p** `items` is a list of pairs of ['name', ::callback].
	- **@r** `null` 


- **setItems** (`list items`)

	> Sets a list of items for the actionbox.

	- **@p** `items` is a list of pairs of ['name', ::callback].
	- **@r** `null` 


- **addItem** (`list item`)

	> Adds an item pair to the actionbox.

	- **@p** `item` is a pair with ['name', ::callback].
	- **@r** `null` 




## class: filedialog

[1034:21] `static` (extern: com.lehman.aussom.stdlib.curses.CursesFileDialog) **extends: object** 

Dialog filedialog provides file chooser support.

#### Methods

- **show** (`object gui, string title = "", string desc = "", string actionLabel = ""`)

	> Opens a modal file dialog window with the provided arguments.

	- **@p** `gui` is the gui object.
	- **@p** `title` is a string with the title to use.
	- **@p** `desc` is a string with the description for the dialog.
	- **@p** `actionLabel` is a string with the action button text.
	- **@r** `A` string with the selected file.




## class: screen

[322:14] (extern: com.lehman.aussom.stdlib.curses.Screen) **extends: object** 

Implementation of the screen object.

#### Methods

- **start** ()

	> Starts the screen.

	- **@r** `null` 


- **stop** ()

	> Stops the screen.

	- **@r** `null` 


- **setColor** (`string Color`)

	> Sets the text color to the specified color.

	- **@p** `Color` is a string with the color. (enum color)
	- **@r** `null` 


- **setBgColor** (`string Color`)

	> Sets the background color to the specified color.

	- **@p** `Color` is a string with the color. (enum color)
	- **@r** `null` 


- **putString** (`int x, int y, string str`)

	> Writes a string at the provided coordinates.

	- **@p** `x` is an int with the x coordinate.
	- **@p** `y` is an int with the y coordinate.
	- **@p** `str` is a string to write.
	- **@r** `null` 


- **clear** ()

	> Clears the screen.

	- **@r** `null` 


- **refresh** ()

	> Writes content from the back buffer to the front buffer effectively updating the screen.

	- **@r** `null` 


- **getKey** ()

	> Blocking call that gets a key stroke and returns it as a keyStroke object.

	- **@return** `A` keyStroke object.


- **doResizeIfNecessary** ()

	> This method can be called at the beginning of your rendering code to handle screen resize events and properly resize the screen buffers.

	- **@r** `null` 


- **setPos** (`int col, int row`)

	> Sets the cursor position to the provided location.

	- **@p** `col` is an int with the column.
	- **@p** `row` is an int with the row.
	- **@r** `null` 




## class: keyStroke

[274:14] (extern: com.lehman.aussom.stdlib.curses.CursesKeyStroke) **extends: object** 

This object is returned from get key function calls
such as getKey() and pollKey().

#### Methods

- **getChar** ()

	> For key strokes of normal characters this gets the character value. Letters, digits, symbols ..

	- **@r** `A` character.


- **getEventTime** ()

	> Gets the event time the key was pressed.

	- **@r** `An` integer with epoch time.


- **getKeyType** ()

	> Gets the key type. Compare this to keyType values.

	- **@r** `A` string with the key type.


- **isAltDown** ()

	> Is the alt key down.

	- **@r** `A` boolean with true if the key is down.


- **isCtrlDown** ()

	> Is the control key down.

	- **@r** `A` boolean with true if the key is down.


- **isShiftDown** ()

	> Is the shift key down.

	- **@r** `A` boolean with true if the key is down.


- **toString** ()

	> Returns a string representation of the keyStroke object.

	- **@r** `A` sring representing the keyStroke.




## class: linearlayout

[479:14] (extern: com.lehman.aussom.stdlib.curses.CursesLinearLayout) **extends: object** 

Linear layout lays out components in a single direction, either
vertical or horizontal. Direction is specified in direction enum.

#### Methods

- **linearlayout** (`string direct = "vertical"`)

	> Layout constructor takes the layout direction as an argument.

	- **@p** `direct` is the direction for the layout. (enum direction)
	- **@r** `null` 


- **setDirection** (`string direct`)

	> Sets the layout direction.

	- **@p** `direct` is the direction for the layout. (enum direction)
	- **@r** `null` 




## class: inputdialog

[1017:21] `static` (extern: com.lehman.aussom.stdlib.curses.CursesInputDialog) **extends: object** 

Dialog inputdialog prompts for text input.

#### Methods

- **show** (`object gui, string title = "", string desc = "", string initText = "", int cols = null, int rows = null`)

	> Opens an input modal dialog window with the provided arguments.

	- **@p** `gui` is the gui object.
	- **@p** `title` is a string with the title to use.
	- **@p** `desc` is a string with the description.
	- **@initText** `is` a string with the input initial text.
	- **@p** `cols` is an int with the number of cols for the input.
	- **@p** `rows` is an int with the number of rows for the input.
	- **@r** `A` string with the input result.




## class: curses

[133:21] `static` (extern: com.lehman.aussom.stdlib.curses.Curses) **extends: object** 

The main curses class for primitive operations.

#### Methods

- **start** ()

	> Starts basic curses support.

	- **@r** `null` 


- **stop** ()

	> Stops basic curses support.

	- **@r** `null` 


- **enterPrivateMode** ()

	> In private mode the screen is cleared, scrolling is disabled and the previous content is stored. When you exit private mode, the previous content is restored and displayed on screen again.



- **exitPrivateMode** ()

	> Exits private mode.

	- **@r** `null` 


- **setPos** (`int col, int row`)

	> Sets the cursor position.

	- **@p** `col` is the column or x coordinate to use.
	- **@p** `row` is the row or y coordinate to use.
	- **@r** `null` 


- **putChar** (`string ch`)

	> Puts the provided character at the current position.

	- **@p** `ch` is a string with a character to place at the current position.
	- **@r** `null` 


- **getRows** ()

	> Gets the screen size in rows.

	- **@r** `An` int with number of rows in the screen.


- **getCols** ()

	> Gets the screen size in cols.

	- **@r** `An` int with the number of cols in a screen.


- **flush** ()

	> Flushes anything left in the buffer out to the device.

	- **@r** `null` 


- **putString** (`int x, int y, string str`)

	> Puts the provided string at the provided coordinates.

	- **@p** `x` is the x coordinate.
	- **@p** `y` is the y coordinate.
	- **@p** `str` is the string to write.
	- **@r** `null` 


- **putLine** (`int x1, int y1, int x2, int y2, string str`)

	> Draws a line of text with the provided start and end coordinates and the provided string.

	- **@p** `x1` is the start x coordinate.
	- **@p** `y1` is the start y coordinate.
	- **@p** `x2` is the end x coordinate.
	- **@p** `y2` is the end y coordinate.
	- **@p** `str` is the string to write.
	- **@r** `null` 


- **setColor** (`string Color`)

	> Sets the text color with the provided color. It's recommended to use the color enum.

	- **@p** `Color` is a string with the color value.
	- **@r** `null` 


- **setBgColor** (`string Color`)

	> Sets the background color with the provided color. It's recommended to use the color enum.

	- **@p** `Color` is a string with the color value.
	- **@r** `null` 


- **enableStyle** (`string Style`)

	> Sets the provided style from the style enum.

	- **@p** `Style` is a string with the style to set.
	- **@r** `null` 


- **disableStyle** (`string Style`)

	> Disables the provided style from the style enum.

	- **@p** `Style` is a string with the style to disable.
	- **@r** `null` 


- **resetStyle** ()

	> Resets all styles.

	- **@r** `null` 


- **clearScreen** ()

	> Clears the screen.

	- **@r** `null` 


- **getKey** ()

	> Blocking call that gets a key stroke from the keyboard and returns a keyStroke object.

	- **@r** `A` keyStroke object with the pressed key info.


- **pollKey** ()

	> Non blocking call which gets the key stroke if available and returns it as a keyStroke object.

	- **@r** `A` keyStroke object with the key info.




## class: style

[23:6] `static` **extends: object** 

Enum defines the curses styles.

#### Members
- **bold**
- **reverse**
- **underline**
- **blink**
- **bordered**
- **franktur**
- **crossed\_out**
- **circled**
- **italic**



## class: gui

[397:14] (extern: com.lehman.aussom.stdlib.curses.Gui) **extends: object** 

Implementation of the GUI object.

#### Methods

- **gui** (`Screen, Color`)

	> Constructor creates a new gui object with the provided screen and color.

	- **@p** `Screen` is the screen object.
	- **@p** `Color` is a string with the text color to set. (enum color)
	- **@r** `null` 


- **newGui** (`object Screen, string Color`)

	> Internal function for creating a new gui.

	- **@p** `Screen` is the screen object.
	- **@p** `Color` is a string with the text color to set. (enum color)
	- **@r** `null` 


- **start** ()

	> Starts the screen.

	- **@r** `null` 


- **stop** ()

	> Stops the screen.

	- **@r** `null` 


- **addWindowAndWait** (`object win`)

	> Adds a window to the text gui system and then blocks.

	- **@p** `win` is the window object to set for this gui.
	- **@r** `null` 




## class: actiondialog

[1064:21] `static` (extern: com.lehman.aussom.stdlib.curses.CursesActionDialog) **extends: object** 

Dialog actiondialog provides action dialog support.

#### Methods

- **show** (`object gui, string title = "", string desc = "", list actions`)

	> Opens a modal action dialog window with the provided arguments.

	- **@p** `gui` is the gui object.
	- **@p** `title` is a string with the title to use.
	- **@p** `desc` is a string with the description for the dialog.
	- **@p** `actions` is a list of pairs of ['name', ::callback].
	- **@r** `null` 




## class: absolutelayout

[473:14] (extern: com.lehman.aussom.stdlib.curses.CursesAbsoluteLayout) **extends: object** 

Absolute layout allows for exact positioning.



## class: color

[8:6] `static` **extends: object** 

Enum defines the curses colors.

#### Members
- **black**
- **red**
- **green**
- **yellow**
- **blue**
- **magenta**
- **cyan**
- **white**
- **def**



## class: directorydialog

[1049:21] `static` (extern: com.lehman.aussom.stdlib.curses.CursesDirectoryDialog) **extends: object** 

Dialog directorydialog provides directory chooser support.

#### Methods

- **show** (`object gui, string title = "", string desc = "", string actionLabel = ""`)

	> Opens a modal directory dialog window with the provided arguments.

	- **@p** `gui` is the gui object.
	- **@p** `title` is a string with the title to use.
	- **@p** `desc` is a string with the description for the dialog.
	- **@p** `actionLabel` is a string with the action button text.
	- **@r** `A` string with the selected directory.




## class: button

[677:14] (extern: com.lehman.aussom.stdlib.curses.CursesButton) **extends: component** 

The button component provides button functionality.

#### Methods

- **button** (`string text = "", callback cb`)

	> Constructor for button object.

	- **@p** `text` is a string with the button text.
	- **@p** `cb` is a callback with the on-click handler.
	- **@r** `null` 


- **newButton** (`string text = "", callback cb`)

	> Helper function that creates a new button.

	- **@p** `text` is a string with the button text.
	- **@p** `cb` is a callback with the on-click handler.
	- **@r** `null` 


- **setText** (`string text`)

	> Sets the button text.

	- **@p** `text` is a string with the button text.
	- **@r** `null` 


- **getText** ()

	> Gets the button text.

	- **@r** `A` string with the button text.




## class: dialog

[1002:21] `static` (extern: com.lehman.aussom.stdlib.curses.CursesDialog) **extends: object** 

Standard text dialog.

#### Methods

- **show** (`object gui, string title = "", string text = "", string dlgBtn = ""`)

	> Opens a modal dialog window with the provided arguments.

	- **@p** `gui` is the gui object.
	- **@p** `title` is a string with the title to use.
	- **@p** `text` is a string with the text for the dialog.
	- **@p** `dlgBtn` is a string with the dialog button from the dlgbtn enum.
	- **@r** `null` 




## class: radiobox

[838:14] (extern: com.lehman.aussom.stdlib.curses.CursesRadioBox) **extends: component** 

The radiobox component provides a list of radio buttons.

#### Methods

- **radiobox** (`list items`)

	> Constructor for the radiobox object.

	- **@p** `items` is a list of strings with the radiobox items.
	- **@r** `null` 


- **setItems** (`list items`)

	> Sets the radiobox items.

	- **@p** `items` is a list of strings with the radiobox items.
	- **@r** `null` 


- **addItem** (`string item = ""`)

	> Adds an item to the radiobox.

	- **@p** `item` is a string with the item to add.
	- **@r** `null` 


- **clear** ()

	> Clears/removes the items from the radiobox.

	- **@r** `null` 


- **clearSelected** ()

	> Unselects selected items.

	- **@r** `null` 


- **getSelected** ()

	> Gets the selected item.

	- **@r** `A` string with the selected item.


- **getSelectedIndex** ()

	> Gets the selected index.

	- **@r** `An` integer with the index of the selected item.


- **size** ()

	> Gets the size of the radiobox items.

	- **@r** `An` int with the number of items in the radiobox.


- **isChecked** (`int index`)

	> Checks if the item at the provided index is selected.

	- **@p** `index` is an int with the index to check.
	- **@r** `A` boolean with true if selected and false if not.


- **remove** (`int index`)

	> Removes the item at the provided index.

	- **@p** `index` is an int with the index to remove the item at.
	- **@r** `null` 




## class: checkbox

[778:14] (extern: com.lehman.aussom.stdlib.curses.CursesCheckBox) **extends: component** 

The checkbox component provides a checkbox list.

#### Methods

- **checkbox** (`list items`)

	> Constructor for the checkbox object.

	- **@p** `items` is a list of strings of each checkbox.
	- **@r** `null` 


- **setItems** (`list items`)

	> Sets the checkbox items.

	- **@p** `items` is a list of strings of each checkbox.
	- **@r** `null` 


- **addItem** (`string item = ""`)

	> Adds an item to the checkbox list.

	- **@p** `item` is a string with the item to add.
	- **@r** `null` 


- **clear** ()

	> Clears the checkbox list.

	- **@r** `null` 


- **getCheckedItems** ()

	> Gets a list of checked items.

	- **@r** `A` list of strings with checked items.


- **size** ()

	> Gets the checkbox list size.

	- **@r** `An` int with the checkbox size.


- **isChecked** (`int index`)

	> Checks to see if the checkbox is checked at the provided index.

	- **@p** `index` is an int with the index to check.
	- **@r** `A` boolean with true for checked and false for not.


- **remove** (`int index`)

	> Remove the checkbox item at the provided index.

	- **@p** `index` is an int with the index to remove.
	- **@r** `null` 




## class: dlgbtn

[107:6] `static` **extends: object** 

Enum that defines dialog button options.

#### Members
- **ok**
- **cancel**
- **yes**
- **no**
- **close**
- **abort**
- **ignore**
- **retry**
- **continue**



## class: gridlayout

[501:14] (extern: com.lehman.aussom.stdlib.curses.CursesGridLayout) **extends: object** 

Grid layout lays out components in a customizable grid structure.

#### Methods

- **gridlayout** (`int NumCols`)

	> Constructor for gridlayout takes the number of columns for the grid.

	- **@p** `NumCols` is an int with the number of columns of the grid.
	- **@r** `null` 


- **newGridLayout** (`int NumCols`)

	> Private helper function creates a new gridlayout.

	- **@p** `NumCols` is an int with the number of columns of the grid.
	- **@r** `null` 




## class: keyType

[38:6] `static` **extends: object** 

Enum defines the curses keyboard keys.

#### Members
- **arrowdown**
- **arrowleft**
- **arrowright**
- **arrowup**
- **backspace**
- **character**
- **cursorlocation**
- **delete**
- **end**
- **enter**
- **eof**
- **escape**
- **f1**
- **f2**
- **f3**
- **f4**
- **f5**
- **f6**
- **f7**
- **f8**
- **f9**
- **f10**
- **f11**
- **f12**
- **f13**
- **f14**
- **f15**
- **f16**
- **f17**
- **f18**
- **f19**
- **home**
- **indert**
- **mouseevent**
- **pagedown**
- **pageup**
- **reversetab**
- **tab**
- **unknown**



## class: panel

[575:14] (extern: com.lehman.aussom.stdlib.curses.CursesPanel) **extends: component** 

The panel component is a base element where other components
can be placed.

#### Methods

- **setLayout** (`object layout`)

	> Sets a layout for the panel.

	- **@p** `layout` is the layout object to set.
	- **@r** `null` 


- **add** (`object Component`)

	> Adds a component to the panel.

	- **@p** `Component` is a component object to add.
	- **@r** `null` 




## class: win

[440:14] (extern: com.lehman.aussom.stdlib.curses.Win) **extends: object** 

Implementation of the GUI object.

#### Methods

- **setHints** (`list Hints`)

	> Sets the window hints. Use the winHint enum to supply the desired attributes.

	- **@p** `Hints` is a list of strings with the window hints.
	- **@r** `null` 


- **set** (`object Component`)

	> Sets the component object for this window.

	- **@p** `Component` is a curses component to set.
	- **@r** `null` 


- **setMenuBar** (`object MenuBar`)

	> Sets the curses menubar object.

	- **@p** `MenuBar` is a menubar to set for the window.
	- **@r** `null` 




## class: table

[938:14] (extern: com.lehman.aussom.stdlib.curses.CursesTable) **extends: component** 

Is a component that provides table functionality.

#### Methods

- **table** (`list cols`)

	> Constructor for a table component.

	- **@p** `cols` is an array of columns for the tables.
	- **@r** `null` 


- **setCols** (`list cols`)

	> Sets the cols of the tables. This resets the table.

	- **@p** `cols` is an array of columns for the tables.
	- **@r** `null` 


- **addCol** (`string col`)

	> Adds a column to the table with the provided column.

	- **@p** `col` is a string with the new column to add.


- **addRow** (`list vals`)

	> Adds a row to the table with the provided list of values.

	- **@p** `vals` is a list of values to set for the row.
	- **@r** `null` 


- **setAction** (`callback action = null`)

	> Sets an action handler to do an action when a row is selected.

	- **@p** `action` is a callback to invoke when a row is selected. When called the argument is a list with the row values is provided.
	- **@r** `null` 


- **setVisibleCols** (`int cols`)

	> Sets the number of visible columns.

	- **@p** `cols` is an int with the number of visible cols.
	- **@r** `null` 


- **setVisibleRows** (`int rows`)

	> Sets the number of visible rows.

	- **@p** `rows` is an int with the number of visible rows.
	- **@r** `null` 


- **setViewTopRow** (`int row`)

	> Sets the view to row.

	- **@p** `row` is an int with the view top row.
	- **@r** `null` 




## class: direction

[99:6] `static` **extends: object** 

Enum that defines a curses layout direction.

#### Members
- **vertical**
- **horizontal**



## class: winHint

[83:6] `static` **extends: object** 

Enum that defines the curses window hint (styles).

#### Members
- **centered**
- **expanded**
- **fit\_terminal\_window**
- **fixed\_position**
- **fixed\_size**
- **full\_screen**
- **modal**
- **no\_decorations**
- **no\_focus**
- **no\_post\_rendering**



## class: label

[594:14] (extern: com.lehman.aussom.stdlib.curses.CursesLabel) **extends: component** 

The label component is used to set text in the UI.

#### Methods

- **label** (`string text`)

	> Constructor that creates a label component with the provided text.

	- **@p** `text` is a string with the label text.
	- **@r** `null` 


- **setText** (`string text`)

	> Sets the label text.

	- **@p** `text` is a string with the label text.
	- **@r** `null` 


- **getText** ()

	> Gets the label text.

	- **@r** `A` string with the label text.




## class: menu

[1092:14] (extern: com.lehman.aussom.stdlib.curses.CursesMenu) **extends: object** 

When added to a menubar provides a dropdown type menu and
sub menu items.

#### Methods

- **menu** (`string label`)

	> Constructor of a menu object with the provided label.

	- **@p** `label` is a string with the menu label.
	- **@r** `null` 


- **newMenu** (`string label`)

	> Helper function that resets the menu object with the provided label.

	- **@p** `label` is a string with the menu label.
	- **@r** `null` 


- **addItem** (`string label, callback action`)

	> Adds an item to the menu.

	- **@p** `label` is a string with the item label.
	- **@p** `action` is a callback with the method to call when the action is invoked.
	- **@r** `null` 




## class: menubar

[1079:14] (extern: com.lehman.aussom.stdlib.curses.CursesMenuBar) **extends: object** 

Provides support for a top menubar.

#### Methods

- **add** (`object menu`)

	> Adds a menu object to the menubar.

	- **@p** `menu` is a menu object to add to the menubar.
	- **@r** `null` 




## class: component

[537:7] **extends: object** 

Component is a class that all other components inherit from. It
provides common functionality for all components.

#### Methods

- **setPos** (`int col, int row`)

	> Sets the position of the cursor on the screen.

	- **@p** `col` is an int with the column.
	- **@p** `row` is an int with the row.
	- **@r** `null` 


- **setPrefSize** (`int cols, int rows`)

	> Sets the referred size. This is used for layouts like linear.

	- **@p** `cols` is an int with the number of columns.
	- **@p** `rows` is an int with the number of rows.
	- **@r** `null` 


- **setSize** (`int cols, int rows`)

	> Sets the size of the component. This is used for absolute layout.

	- **@p** `cols` is an int with the number of columns.
	- **@p** `rows` is an int with the number of rows.
	- **@r** `null` 


- **setLayoutData** (`string val`)

	> Sets the layout data. Use the layoutpos enum to provide position when using border layout.

	- **@p** `val` is a string with the value.
	- **@r** `null` 




## class: layoutpos

[122:6] `static` **extends: object** 

Enum that defines border layout positions.

#### Members
- **center**
- **top**
- **bottom**
- **left**
- **right**



## class: combobox

[713:14] (extern: com.lehman.aussom.stdlib.curses.CursesComboBox) **extends: component** 

The combobox component provides a dropdown type selector.

#### Methods

- **combobox** (`list items`)

	> Constructor for a combobox object.

	- **@p** `items` is a list of strings with the items of the combobox.
	- **@r** `null` 


- **setItems** (`list items`)

	> Sets the combobox items.

	- **@p** `items` is a list of strings with the items of the combobox.
	- **@r** `null` 


- **addItem** (`string item = ""`)

	> Adds an item to the combobox.

	- **@p** `item` is a string with the item to add.
	- **@r** `null` 


- **clear** ()

	> Clears the combobox.

	- **@r** `null` 


- **size** ()

	> Gets the size of the combobox.

	- **@r** `An` int with the size.


- **getItemAt** (`int index`)

	> Gets the item at the provided index.

	- **@p** `index` is an int with the index to get the item at.
	- **@r** `A` string with the item at the provided index.


- **getSelectedIndex** ()

	> Gets the selected index.

	- **@r** `An` int with the selected index.


- **getSelectedItem** ()

	> Gets the selected item.

	- **@r** `A` string with the selected item.


- **getText** ()

	> Gets the combobox text string.

	- **@r** `A` string with the combobox text.




