# file: jdbc.aus

## class: jdbc

[9:14] (extern: com.lehman.aussom.stdlib.AussomJdbc) **extends: object** 

Jdbc class implements functionality for communicating with
RDBMS systems using Java JDBC.

#### Methods

- **setDriver** (`string Driver`)

	> Sets the JDBC driver string.

	- **@p** `Driver` is a string with the JDBC driver.
	- **@r** `This` object.


- **setUrl** (`string Url`)

	> Sets the DB URL string.

	- **@p** `Url` is a string with the DB URL.
	- **@r** `This` object.


- **setUserName** (`string UserName`)

	> Sets the DB user name.

	- **@p** `UserName` is the DB user name.
	- **@r** `This` object.


- **setPassword** (`string Password`)

	> Sets the DB password.

	- **@p** `Password` is the DB password.
	- **@r** `This` object.


- **getDriver** ()

	> Gets the JDBC driver string.

	- **@r** `A` string with the JDBC driver.


- **getUrl** ()

	> Gets the DB URL.

	- **@r** `A` string with the DB URL.


- **getUserName** ()

	> Gets the DB user name.

	- **@r** `A` string with the DB user name.


- **getPassword** ()

	> Gets the DB password.

	- **@r** `A` string with the DB password.


- **setConnectionInfo** (`string Driver, string Url, string UserName, string Password`)

	> Sets all of the JDBC connection info in a single function call.

	- **@p** `Driver` is the JDBC driver string.
	- **@p** `Url` is the JDBC Url.
	- **@p** `UserName` is a string with the DB user name.
	- **@p** `Password` is a string with the DB password.


- **select** (`string Query, list Params`)

	> Performs a select query with the provided arguments and returns the results. It returns a map with 'cols' and 'rows'.

	- **@p** `Query` is a string with the JDBC query.
	- **@p** `Params` is an optional list with prepared statement arguments.
	- **@r** `A` map with the query result in 'cols' and 'rows'.


- **update** (`string Query, list Params`)

	> Performs an update query with the provided arguments.

	- **@p** `Query` is a string with the JDBC query.
	- **@p** `Params` is an optional list with prepared statement arguments.
	- **@r** `An` int with the number of rows affected.


- **connect** ()

	> Attempts to connect to the database.

	- **@r** `This` object.


- **disconnect** ()

	> Attempts to disconnect from the database.

	- **@r** `This` object.




