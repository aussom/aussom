# file: fx.aus

## class: Node

[254:7] **extends: FxObj** 

This class provides easy to use helpers for setting
JavaFX Node values. Node is pretty much the root object
of all JavaFX containers and controls.

#### Methods

- **setId** (`string Id`)

	> Sets the ID of the node. If you set 'myId' you can reference it from a stylesheet using #myId.

	- **@p** `Id` is a string with the ID to set.
	- **@r** `this` object


- **setStyle** (`string StyleStr`)

	> Sets the inline style CSS for the node. This is similar to HTML style tag.



- **setRotate** (`double Angle`)

	> Sets the rotation of the Node with the provided angle.

	- **@p** `Angle` is a double with the angle.
	- **@r** `this` object


- **setTranslateX** (`double Val`)

	> Sets the X translation with the provided value.

	- **@p** `Val` is a double with the translation.
	- **@r** `this` object


- **setTranslateY** (`double Val`)

	> Sets the Y translation with the provided value.

	- **@p** `Val` is a double with the translation.
	- **@r** `this` object




## class: fx

[13:21] `static` (extern: com.lehman.aussom.stdlib.AussomFx) **extends: object** 

This is the base JavaFX class. It provides support for
creating a new JavaFX application or dialog and provides
support functions.

#### Methods

- **fxApp** (`string Title = "Aussom Application", int Width = 800, int Height = 600`)

	> Creates a new JavaFX application and returns a fxApp object with the application.

	- **@p** `Title` is a string with the application window title.
	- **@p** `Width` is an int with the width of the app in pixels.
	- **@p** `Height` is an int with the height of the app in pixels.
	- **@r** `A` fxApp object.


- **fxDialog** (`string Title = "Aussom Dailog", int Width = null, int Height = null`)

	> Creates a new JavaFX dialog and returns a fxApp object with the dialog window.

	- **@p** `Title` is a string with the dialog window title.
	- **@p** `Width` is an int with the width of the dialog in pixels.
	- **@p** `Height` is an int with the height of the dialog in pixels.
	- **@r** `A` fxApp object.


- **shutdown** ()

	> Stops the JavaFX application and associated background processes for the current running app.



- **runLater** (`callback ToRun, ...`)

	> Runs a UI task async. Runs UI updates properly. Any args after ToRun will be passed to called function.

	- **@p** `ToRun` is a callback with the function to run.
	- **@p** `etc` is an optional list of arguments to pass to the called function.


- **isUiThread** ()

	> Checks if the current executing thread is the FX UI thread.

	- **@r** `A` bool with true if it's running on the UI thread and false if not.


- **actionEventHandler** (`callback OnEvent`)

	> Creates a JavaFX Action event handler and returns it with the event. The OnEvent function just takes a single string argument with the action.

	- **@p** `OnEvent` is a callback with the function to call on the event.
	- **@r** `A` AussomJavaObject with the EventHandler<ActionEvent> object.


- **toHexString** (`object ColorObj`)

	> Converts the provided AussomJavaObject with the Java Color object to be converted to a hex string.

	- **@p** `ColorObj` is a AussomJavaObject to convert.
	- **@r** `A` string with the hex value.


- **getClosure** (`string InterfaceClass, callback OnAction`)




## class: Control

[310:7] **extends: object** 

Represents a base class for JavaFX controls, providing functionality to set tooltips.

#### Methods

- **setTooltip** (`Val`)

	> Sets a tooltip for the control.

	- **@p** `Val` Val is a Tooltip object to display when hovering over the control.
	- **@r** `this` object for chaining




## class: Pane

[346:7] **extends: object** 

This class provides some of the common functionality
that the JavaFX Pane class provides and containers
such as AnchorPane and VBox subclass this.

#### Methods

- **add** (`Items`)

	> Add a signgle item, or multiple items to the pane.

	- **@p** `Items` is either a single item or a list of items to add.
	- **@r** `this` object




## class: FxObj

[209:7] **extends: object** 

Represents a base class for JavaFX objects, providing utility methods for copying,
creating observable lists, and managing style classes.

#### Members
- **obj**

#### Methods

- **copy** (`Val`)

	> Copy constructor to set the value. If an AussomJavaObject is passed, this.obj is set to it. Returns true if it sets the value successfully.

	- **@p** `Val` is any value to check and set if it is an AussomJavaObject.
	- **@r** `A` bool with true if set and false if not.


- **observableArrayList** (`Items = null`)

	> Creates an observable ArrayList, optionally with initial items.

	- **@p** `Items` is a list of initial items or null for an empty list.
	- **@r** `An` observable ArrayList object.


- **getStyleClass** ()

	> Gets a list of the JavaFX style classes for the node.

	- **@r** `A` list of strings with the style classes.




## class: Region

[326:7] **extends: object** 

Represents a base class for JavaFX regions, allowing for preferred size configuration.

#### Methods

- **setPrefSize** (`double Width, double Height`)

	> Sets the preferred width and height for the region.

	- **@p** `Width` Width is a double value to set as the preferred width.
	- **@p** `Height` Height is a double value to set as the preferred height.
	- **@r** `this` object for chaining




## class: fxApp

[85:14] (extern: com.lehman.aussom.stdlib.AussomFxApp) **extends: object** 

Represents the main application or dialog class for creating UI applications in Aussom.
The `fxApp` class serves as the base object for all other UI elements, acting as the container
and primary controller of the application interface. Although it is a native Aussom object,
all JavaFX objects can be created and manipulated within it using AJI (Aussom-Java Interface).

#### Methods

- **getAjo** ()

	> Retrieves the underlying AussomJavaObject associated with this application instance.

	- **@r** `The` AussomJavaObject object.


- **show** (`bool BlockThread = true`)

	> Displays the application or dialog. Optionally blocks the thread.

	- **@p** `BlockThread` A bool specifying if the thread should be blocked. Defaults to true.


- **\_show** (`bool BlockThread = true`)


- **close** ()

	> Closes the application or dialog.

	- **@r** `A` result indicating the closure status.


- **\_close** ()


- **add** (`object FxObj`)

	> Adds a JavaFX object (e.g., a UI component) to the application.

	- **@p** `FxObj` The object to be added to the application's UI.


- **addStyleSheet** (`string StyleSheetFile`)

	> Adds a CSS stylesheet to the application's scene.

	- **@p** `StyleSheetFile` The path to the stylesheet file.


- **openInBrowser** (`string HostUri`)

	> Opens the specified URI in the default web browser.

	- **@p** `HostUri` The URI to be opened.


- **setOnShow** (`callback OnShown = null`)

	> Sets a callback function that is executed when the application is shown.

	- **@p** `OnShown` The callback function, or null for no action.


- **setOnClosing** (`callback OnClosing = null`)

	> Sets a callback function that is executed when the application is closing.

	- **@p** `OnClosing` The callback function, or null for no action.


- **setLayout** (`object FxObj`)

	> Sets the layout node for the application's scene.

	- **@p** `FxObj` The layout node to set.


- **setMenuBar** (`object MenuBar`)

	> Sets a menu bar for the application window.

	- **@p** `MenuBar` The menu bar object to set.


- **setMaximized** (`bool Maximized`)

	> Sets the maximized state of the application window.

	- **@p** `Maximized` A bool indicating if the window should be maximized.


- **setFullScreen** (`bool FullScreen`)

	> Sets the application window to full-screen mode.

	- **@p** `FullScreen` A bool indicating if full-screen mode should be enabled.


- **getMaximized** ()

	> Retrieves whether the application window is maximized.

	- **@r** `A` bool indicating if the window is maximized.


- **getFullScreen** ()

	> Retrieves whether the application window is in full-screen mode.

	- **@r** `A` bool indicating if full-screen mode is enabled.


- **getStyle** ()

	> Retrieves the current style of the application.

	- **@r** `The` style string.


- **getId** ()

	> Retrieves the ID of the application.

	- **@r** `The` ID string.


- **setStyle** (`string Style`)

	> Sets the style for the application.

	- **@p** `Style` A string specifying the style to set.


- **setId** (`string Id`)

	> Sets the ID for the application.

	- **@p** `Id` A string specifying the ID to set.


- **setToolTip** (`object ToolTip = null`)

	> Sets a tooltip for the application window.

	- **@p** `ToolTip` The tooltip object to attach, or null for no tooltip.




